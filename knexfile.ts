import dotenv from 'dotenv'
dotenv.config()

// Update with your config settings.

module.exports = {
  development: {
    client: 'postgresql',
    connection: {
      database: process.env.DB_NAME,
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      time_zone:'Asia/Hong_Kong'
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },

  testing: {
    client: 'postgresql',
    connection: {
      database: process.env.TEST_DB_NAME,
      user: process.env.TEST_DB_USER,
      password: process.env.TEST_DB_PASS,
      time_zone:'Asia/Hong_Kong'

    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },

  // ci: {
  //   client: 'postgresql',
  //   connection: {
  //     database: process.env.POSTGRES_DB,
  //     user: process.env.POSTGRES_USER,
  //     password: process.env.POSTGRES_PASSWORD,
  //     host: process.env.POSTGRES_HOST,
  //     time_zone:'Asia/Hong_Kong'
  //   },
  //   pool: {
  //     min: 2,
  //     max: 10,
  //   },
  //   migrations: {
  //     tableName: 'knex_migrations',
  //   },
  // },

  // staging: {
  //   client: 'postgresql',
  //   connection: {
  //     database: process.env.DB_NAME,
  //     user: process.env.DB_USER,
  //     password: process.env.DB_PASS,
  //     time_zone:'Asia/Hong_Kong'
  //   },
  //   pool: {
  //     min: 2,
  //     max: 10,
  //   },
  //   migrations: {
  //     tableName: 'knex_migrations',
  //   },
  // },

  production: {
    client: 'postgresql',
    connection: {
      database: process.env.DB_NAME,
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      time_zone:'Asia/Hong_Kong'
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },
}
