import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('live_posts')) {
        return
      }
      await knex.schema.createTable('live_posts', table => {
        table.increments()
        table.integer('user_id').references('id').inTable('users').notNullable().onDelete('cascade');
        table.text('caption')
        table.text('url').notNullable()
        table.timestamp('created_at').defaultTo(knex.fn.now())
      })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('live_posts')
}