import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('admins')) {
    return
  }
  await knex.schema.createTable('admins', table => {
    table.increments()
    table.string('admin_name').notNullable()
    table.string('email').notNullable().unique()
    table.string('password').notNullable()
    table.string('role').notNullable()
    table.string('theme').notNullable()
    table.timestamps(false, true)
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('admins')
}

