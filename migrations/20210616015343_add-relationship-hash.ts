import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasColumn('relationship', 'room_id')) {
    return
  }
  await knex.schema.alterTable('relationship', table => {
    table.text('room_id');
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.table('relationship', table => {
    table.dropColumn('room_id');
  })
}

