import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('coin_records')) {
        return
      }
      await knex.schema.createTable('coin_records', table => {
        table.increments()
        table.integer('user_id').references('id').inTable('users').notNullable().onDelete('cascade');
        table.integer('transaction').notNullable()
        // table.string('status', 10).notNullable()
        table.string('description',255)
        table.string('created_by',30)
        table.timestamp('created_at').defaultTo(knex.fn.now())
      })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('coin_records')
}

