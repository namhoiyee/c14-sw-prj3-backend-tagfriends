import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('users')) {
    return
  }
  if (await knex.schema.hasTable('genders')) {
    return
  }
  await knex.schema.createTable('genders', table => {
    table.increments()
    table.string('gender',20).notNullable()
    table.unique(['gender'])
  })
  await knex.schema.createTable('users', table => {
    table.increments()
    table.string('username', 50).notNullable()
    table.string('email', 255).notNullable()
    table.unique(['email'])
    table.string('phone_num', 8)
    table.integer('registration_status').defaultTo(0)
    table.string('google_access_token', 255)
    table.string('password', 255)
    table.date('birthday')
    table.integer('gender_id').references('id').inTable('genders').onDelete('cascade');
    table.timestamps(false, true)
    table.integer('coins').defaultTo(100)
    table.timestamp('last_fetch_time').defaultTo(knex.fn.now())
    table.string('lang')
    table.text('text_description')
    table.text('voice_description')
    table.boolean('isBlocked').defaultTo(false)
    table.boolean('isFake').defaultTo(false)
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('users')
  await knex.schema.dropTableIfExists('genders')
}