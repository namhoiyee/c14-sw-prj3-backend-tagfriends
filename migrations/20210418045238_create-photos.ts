import { Knex } from "knex";
import { deleteFromS3 } from "../helpers/deleteFromS3";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('user_photos')) {
    return
  }
  await knex.schema.createTable('user_photos', table => {
    table.increments()
    table.integer('user_id').references('id').inTable('users').notNullable().onDelete('cascade');
    table.text('url')
    table.string('description', 255)
    // table.text('image_key')
    table.timestamp('created_at').defaultTo(knex.fn.now())
  })
}

export async function down(knex: Knex): Promise<void> {
  let urlArr = await knex.select('url').from('user_photos')
  let promise = []
  for (let url of urlArr){
    promise.push(deleteFromS3(url.url))

  }
  Promise.all(promise)
  await knex.schema.dropTableIfExists('user_photos')
}