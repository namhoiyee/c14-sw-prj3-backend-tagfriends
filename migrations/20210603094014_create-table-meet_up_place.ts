import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('meet_up_places')) {
        return
      }
      await knex.schema.createTable('meet_up_places', table => {
        table.increments()
        table.integer('event_category_id').references('id').inTable('event_categories').onDelete('cascade');
        table.jsonb('description')
        table.specificType('address','POINT')
      })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('meet_up_places')
}