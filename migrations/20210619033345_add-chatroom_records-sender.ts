import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasColumn('chatroom_records', 'sender_id')) {
    return
  }
  await knex.schema.alterTable('chatroom_records', table => {
    table.integer('sender_id');
  })

  const existingRecords = await knex.select('user_id', 'id')
    .from('relationship').whereNotNull('room_id')

  for (let rec of existingRecords) {
    await knex("chatroom_records")
    .update({
      sender_id: rec.user_id,
    })
    .where("relationship_id", rec.id)
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.table('chatroom_records', table => {
    table.dropColumn('sender_id');
  })
}

