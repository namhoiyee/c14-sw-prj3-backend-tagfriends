import { Knex } from "knex";


exports.up = async function (knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('user_locations')) {
    return
  }
  await knex.schema.createTable("user_locations", (table) => {
    table.increments();
    table.string("grid", 6).notNullable();
    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.timestamp('matched_at')
    table.integer('user_id').references('id').inTable('users').notNullable().onDelete('cascade');
    table.jsonb('result')
    table.timestamp('updated_at')
    table.jsonb('updated_result')
    table.specificType('location','point')
  });
};

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('user_locations')
}

