import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('reports')) {
        return
      }
      await knex.schema.createTable('reports', table => {
        table.increments()
        table.integer('user_id').references('id').inTable('users').notNullable().onDelete('cascade');
        table.integer('target_id').references('id').inTable('users').onDelete('cascade');
        table.string('category', 255)
        table.text('content')
        table.boolean("read")
        table.boolean("replied")
        table.string("replied_by", 255)
        table.timestamp('created_at').defaultTo(knex.fn.now())
      })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('reports')
}

