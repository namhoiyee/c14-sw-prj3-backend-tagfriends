import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('relationship')) {
    return
  }
  await knex.schema.createTable('relationship', table => {
    table.increments()
    table.timestamp('blocked_time');
    table.integer('friend_id').references('id').inTable('users').notNullable().onDelete('cascade');
    table.integer('user_id').references('id').inTable('users').notNullable().onDelete('cascade');
    table.timestamps(true,true)
    table.string('status',10).notNullable()
    table.boolean('trained').defaultTo(false)
    table.boolean('isScheduled').defaultTo(false)
    table.specificType('match_coordinates', 'point').notNullable()
    table.text('room_id');
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('relationship')
}