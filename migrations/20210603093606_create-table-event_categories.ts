import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('event_categories')) {
        return
      }
      await knex.schema.createTable('event_categories', table => {
        table.increments()
        table.string('category_name',255)
      })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('event_categories')
}