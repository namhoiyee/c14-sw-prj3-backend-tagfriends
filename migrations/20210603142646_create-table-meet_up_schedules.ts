import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('meet_up_schedules')) {
        return
      }
      await knex.schema.createTable('meet_up_schedules', table => {
        table.increments()
        table.integer('inviter_id').references('id').inTable('users').onDelete('cascade');
        table.integer('invitee_id').references('id').inTable('users').onDelete('cascade');
        table.integer('place_id').references('id').inTable('meet_up_places').onDelete('cascade');
        table.boolean('isAccepted')
        table.timestamp('scheduled_at')
        table.timestamp('created_at').defaultTo(knex.fn.now())
      })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('meet_up_schedules')
}