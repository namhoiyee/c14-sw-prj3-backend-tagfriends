import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasColumn('user_wish_tags', 'position')) {
    return
  }
  if (await knex.schema.hasColumn('user_tags', 'position')) {
    return
  }
  await knex.schema.alterTable('user_wish_tags', table => {
    table.integer('position');
  })
  await knex.schema.alterTable('user_tags', table => {
    table.integer('position');
  })

  const user_tags = await knex.select('user_id', 'tag_id').from('user_tags').orderBy('user_id', 'asc')
  const user_wish_tags = await knex.select('user_id', 'tag_id').from('user_wish_tags').orderBy('user_id', 'asc')

  // const users_for_user_tags = user_tags.filter((userId, index, self) => {
  //   return self.indexOf(userId) === index;
  // });
  // const users_for_user_wish_tags = user_wish_tags.filter((userId, index, self) => {
  //   return self.indexOf(userId) === index;
  // });

  let index = 0
  for (let i = 0; i < user_tags.length; i++) {
    if (user_tags[i].user_id !== user_tags[i > 0 ? i - 1 : i].user_id) {
      index = 0
    }
    await knex('user_tags').update({
      position: index
    }).where('user_id', user_tags[i].user_id)
      .where('tag_id', user_tags[i].tag_id)
    index++
  }

  index = 0
  for (let i = 0; i < user_wish_tags.length; i++) {
    if (user_wish_tags[i].user_id !== user_wish_tags[i > 0 ? i - 1 : i].user_id) {
      index = 0
    }
    await knex('user_wish_tags').update({
      position: index
    }).where('user_id', user_wish_tags[i].user_id)
      .where('tag_id', user_wish_tags[i].tag_id)
    index++
  }

  await knex.schema.alterTable('user_wish_tags', table => {
    table.integer('position').notNullable().alter();
  })
  await knex.schema.alterTable('user_tags', table => {
    table.integer('position').notNullable().alter();;
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.table('user_wish_tags', table => {
    table.dropColumn('position');
  })
  await knex.schema.table('user_tags', table => {
    table.dropColumn('position');
  })
}