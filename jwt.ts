import {config} from 'dotenv'
config()

if (!process.env.JWT_SECRET) console.log("missing JWT_SECRET in dotenv")
export default {
    jwtSecret: process.env.JWT_SECRET!,
    jwtSession: {
        session: false
    }
}