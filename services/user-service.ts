// import { format } from 'fecha';
import { Knex } from 'knex';
import { checkPassword, hashPassword } from '../hash'
import { JWTPayload } from '../helpers/types';

export class UserService {

    constructor(private knex: Knex) { }

    async checkEmail(email: string) {
        return (await this.knex.select('email').from('users')
            .where('email', email).first())
    }

    async updateFetchTime(id: number) {
        try {
            let update = await this.knex('users').update({
                last_fetch_time: 'now()'
            }).where('id', id)
            .where('registration_status', 3)
            .returning('id')
            if (!update) throw new Error('unable to update fetch time or registration status not 3')
            let today = new Date(new Date().toDateString())
            let coinsAddedToday = await this.knex.select('id')
                .from('coin_records')
                .where('created_at', '>=', today)
                .where('description','daily')
                .where('user_id',id)

            if (coinsAddedToday) return { isSuccess: true, id, added: 0 }

            let coinAdded = await this.fetchAddCoin(id, 1,'daily')
            if (coinAdded == 1) return { isSuccess: true, id, added: coinAdded }
            else return { isSuccess: false, msg: "unable to add coin" }
        } catch (e) {
            console.log(e)
            return { isSuccess: false, msg: e }
        }
    }

    async registrationService(body: { username: string, email: string, password: string, loginLang: string }) {
        try {
            let passwordHash = await hashPassword(body.password)
            let newUser = await this.knex
                .insert({
                    username: body.username,
                    email: body.email,
                    password: passwordHash,
                    last_fetch_time: 'now()',
                    lang: body.loginLang
                }).into("users").returning("id")
            let userPayload = await this.knex.select('id', 'registration_status')
                .from('users')
                .where('id', newUser[0])
                .first()

            return { isSuccess: true, data: userPayload }
        } catch (e) {
            console.log(e)
            return { isSuccess: false, msg: "cannot add user. " + e }
        }
    }

    async loginService(body: { email: string, password: string, lang: string }) {
        // console.log("service: ", body)
        try {
            let user = await this.knex.select('*').from('users')
                .where('email', body.email).first()
            console.log(user)
            let userProfilePic
            let coinAdded = 0

            let passwordCorrect = await checkPassword(body.password, user.password)
            if (passwordCorrect) {
                if (user.registration_status > 1) {
                    userProfilePic = await this.knex.select('url')
                        .from('user_photos')
                        .where({ user_id: user.id })
                        .where("description", "profile")
                        .first()
                }
                await this.knex('users').update({
                    lang:body.lang
                }).where({id:user.id})
                let today = new Date(new Date().toDateString())
                let coinsAddedToday = await this.knex.select('id')
                    .from('coin_records')
                    .where('created_at', '>=', today)
                    .where('description', 'daily')
                    .where('user_id', user.id)
                console.log("coin added", coinsAddedToday)
                if (coinsAddedToday.length == 0) {
                    coinAdded = await this.fetchAddCoin(user.id, 1,'daily')
                    if (coinAdded != 1) throw new Error('unable to add coin')
                }
                return {
                    isSuccess: true,
                    data: {
                        id: user.id,
                        username: user.username,
                        lang: user.lang,
                        profilePic: userProfilePic?.url,
                        coinsAdded: coinAdded,
                        registration_status: user.registration_status
                    }
                }
            }
            else return { isSuccess: false, msg: 'incorrect pw' }
        } catch (e) {
            return { isSuccess: false, msg: 'unable to login: ' + e }
        }
    }

    async updateToken(body: JWTPayload) {
        try {

            let { username, id, lang, registration_status } = body
            let userId = await this.knex('users').update({
                username,
                lang,
                id,
                registration_status
            }).where('id', id)
                .returning('*')
            return userId[0]

        } catch (e) {
            throw new Error('unable to generate new token ' + e)
        }
    }
    async checkToken(Token: number) {
        console.log('checking token...Token: ', Token)
        return (await this.knex.select('*').from('users')
            .where('google_access_token', Token))
    }

    async registrationServiceForGoogle(result: { id: number, email: string, name: string, picture: string }) {
        // console.log('registering account...')
        let inserted = await this.knex
            .insert({
                username: result.name,
                email: result.email,
                google_access_token: result.id,
                last_fetch_time: 'now()'
            }).into("users").returning("*")
        // console.log('inserting photo from Google...user id: ', inserted);
        // await this.knex.insert({
        //     user_id: inserted[0].id,
        //     url: result.picture,
        //     description: 'profile'
        // }).into("user_photos").returning("*")
        return inserted[0]
    }

    async getCoinNum(id: number) {
        return (await this.knex.select('coins', 'last_fetch_time').from('users').where('id', id))
    }

    async fetchAddCoin(id: number, coin: number, description:string) {
        try {
            let originalCoins = await this.knex.select('coins').from('users').where('id', id).first()
            await this.knex.transaction(async (txn) => {
                try {
                    await txn('users').update({
                        coins: originalCoins.coins + coin,
                    }).where('id', id)
                    await txn('coin_records').insert({
                        'transaction': coin,
                        'user_id': id,
                        description,
                        'created_by':'system'
                    })
                } catch (e) {
                    throw new Error(e)
                }
            })
            return coin

        } catch (e) {
            console.log("failed to add coin", e)
            return 0
        }
    }

    async updateLang(lang: string, id: number) {
        try {
            await this.knex('users').update({
                lang: lang
            }).where('id', id)
            return ('language updated')
        } catch (e) {
            throw new Error('unsuccessful update language')
        }
    }

    async getLang(id: number) {
        try {
            return (await this.knex('users').select('lang').where('id', id))
        } catch (e) {
            throw e
        }

    }

    // async canUseCoins(id: number, coinsConsumed: number) {
    //     try {
    //         const coinBalance = await this.knex('users').select('coins').where('id', id)

    //         if (coinBalance[0].coins - coinsConsumed >= 0) {
    //             return true
    //         } else {
    //             return false
    //         }
    //     } catch (e) {
    //         throw e
    //     }


    // }

    async getRegistrationStatus(id: number) {
        return (await this.knex('users').select('registration_status').where('id', id).first()).registration_status
    }

    async getUserById(id: number) {
        let user = await this.knex('users').select('username', 'id').where('id', id).first()
        console.log("service:", user)
        return user
    }

    async getAllInfoForToken(id: number) {
        try {
            const user = await this.knex.from('users').select('username', 'users.id', 'lang', 'registration_status', 'url')
                .join('user_photos', 'user_photos.user_id', 'users.id')
                .where('users.id', id)
                .where('user_photos.description', 'profile')
            return user
        } catch (e) {
            throw e
        }
    }
}