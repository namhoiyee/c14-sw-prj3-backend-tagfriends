import { Knex } from "knex";

export class FriendService {
  constructor(private knex: Knex) {}

  async friendList(userID: number) {
    try {
      // console.log('getting friend list from database, id: ', userID)
      return await this.knex
        .select("id", "friend_id", "status")
        .from("relationship")
        .where("user_id", userID);
      // .orderBy('id', 'desc')
    } catch (e) {
      throw e;
    }
  }

  async userSuggestions(selfRelId: number) {
    try {
      let suggestions = new Map();
      suggestions.set("運動", "parks");
      suggestions.set("文學", "books");
      suggestions.set("菜式", "restaurants");
      suggestions.set("棋類", "board games");
      suggestions.set("電影類別", "cinemas");

      let relationship = await this.knex
        .select("friend_id", "user_id")
        .from("relationship")
        .where("id", selfRelId)
        .where("status", "like")
        .first();
      let friendId = relationship.friend_id;
      let userId = relationship.user_id;
      if (!relationship) throw new Error("relationship does not exist");
      let ownClasses = await this.knex
        .select("classes.class")
        .from("classes")
        .join("tags", "tags.class_id", "classes.id")
        .join("user_tags", "tag_id", "tags.id")
        .where("user_tags.user_id", userId);

      let frdClasses = await this.knex
        .select("classes.class")
        .from("classes")
        .join("tags", "tags.class_id", "classes.id")
        .join("user_tags", "tag_id", "tags.id")
        .where("user_tags.user_id", friendId);

      let combinedClasses = ownClasses.concat(frdClasses);
      combinedClasses = combinedClasses.map((tag) => tag.class);
      combinedClasses = combinedClasses.reduce(
        (acc, e) => acc.set(e, (acc.get(e) || 0) + 1),
        new Map()
      );

      let topClasses = Array.from(combinedClasses.entries()).sort(
        (a, e) => e[1] - a[1]
      );
      let sorted: string[] = topClasses.map((item) => item[0].toString());
      let result = [];
      for (let i = 0; i < sorted.length; i++) {
        if (suggestions.has(sorted[i])) {
          result.push(suggestions.get(sorted[i]));
          if (result.length == 3) return result;
        }
      }
      if (result.length < 3) result.push("cafe");
      return result;
    } catch (e) {
      throw e;
    }
  }
  //people user has liked
  async friendLikeList(userID: number) {
    if (!userID) return;
    try {
      // console.log('getting friend list from database, id: ', userID)
      return await this.knex
        .select("*")
        .from("relationship")
        .where("user_id", userID)
        .andWhere("status", "like");
      // .orderBy('id', 'desc')
    } catch (e) {
      throw e;
    }
  }

  async friendLikeListByFdRelationshipID(FdID: number) {
    try {
      // console.log('getting friend list from database, id: ', userID)
      return await this.knex
        .select("*")
        .from("relationship")
        .where("id", FdID)
        .andWhere("status", "like");
      // .orderBy('id', 'desc')
    } catch (e) {
      throw e;
    }
  }

  //check whether other user liked user
  async checkFriendRelationship(
    userID: number,
    friend: { id: number; friend_id: number }
  ) {
    try {
      return await this.knex
        .select("*")
        .from("relationship")
        .where("user_id", friend.friend_id)
        .andWhere("friend_id", userID)
        .andWhereNot("status", "unlike")
        .orderBy("updated_at", "desc")
        .limit(1);
    } catch (e) {
      throw e;
    }
  }

  async checkRelationship(userId: number) {
    try {
      const relationship = await this.knex
        .select("friend_id", "room_id")
        .from("relationship")
        .where("user_id", userId)
        .whereNotNull("room_id")
        .orderBy("id");
      return relationship;
    } catch (e) {
      throw e;
    }
  }

  async getFriendNameAndPhoto(friend_ids: number[]) {
    try {
      // console.log('received: ', friend_id)
      return await this.knex("users")
        .join("user_photos", "users.id", "=", "user_photos.user_id")
        .select(
          "users.id as true_friend_id",
          "users.username",
          "user_photos.url"
        )
        .whereIn("users.id", friend_ids)
        .andWhere("user_photos.description", "profile")
        .orderBy("users.id");
    } catch (e) {
      throw e;
    }
  }

  async checkFriends(userId: number) {
    try {
      let friendList = [];
      const relationship = await this.knex
        .select("friend_id", "room_id")
        .from("relationship")
        .where("user_id", userId)
        .whereNotNull("room_id")
        .orderBy("id");
      if (relationship.length) {
        for (let item of relationship) {
          let likeBack = await this.knex
            .select("friend_id", "room_id")
            .from("relationship")
            .where("user_id", item.friend_id)
            .where("room_id", item.room_id)
            .whereNotNull("room_id");
          if (likeBack.length == 1) friendList.push(item);
        }
      }
      return friendList;
    } catch (error) {
      throw error
    }
  }

  async getFriendInfo(userId: number, friendId: number) {
    try {
      //   let otherUserIdsBeforeRemove = otherUserNameAndIds.map(otherUserNameAndId => otherUserNameAndId.user_id)
      //   // console.log('ids',otherUserIds)
      //   let otherUserIds = await this.removeSwipedPeople(otherUserIdsBeforeRemove, userId)
      let gender = (
        await this.knex("users").select("gender_id").where("id", friendId)
      )[0].gender_id;

      const userWishTags = await this.knex
        .select("tag_id")
        .from("user_wish_tags")
        .where("user_id", userId);
      // console.log({userWishTags})
      const userWishTagsMap = userWishTags.map(
        (userWishTag) => userWishTag["tag_id"]
      );
      // , : otherUserTags, : otherUserGallery, : otherUserNameAndIds[i].username, })

      const otherUserTags = await this.knex("user_tags")
        .join("tags", "user_tags.tag_id", "=", "tags.id")
        .select("*")
        .orderBy("created_at", "asc")
        .where("user_id", friendId);

      let [{ birthday: otherUserBdayTimeStamp, username }] = await this.knex
        .select("username", "birthday")
        .from("users")
        .where("id", friendId);

      const otherUserBday = new Date(otherUserBdayTimeStamp);
      let today = Date.now();
      let friendAge = Math.floor(
        (today - otherUserBday.getTime()) / 1000 / 60 / 60 / 24 / 365
      );
      let matchedTags = otherUserTags.filter((userTag) => {
        // console.log("userTagMap",userTag['tag_id'])
        // console.log("userWishTagsMap",userWishTags.map(userWishTag => userWishTag['tag_id']))
        return userWishTagsMap.includes(userTag["tag_id"]);
      });
      // console.log({matchedTags})

      //   const horoscopeMatched = await this.checkUserHoroscopePreference(userWishTagsMap, otherUserBday, matchedTags)

      // console.log(otherUserIds[i], 'passed horoscope test', horoscopeMatched, userHoroscopePreference)

      const otherUserGallery = await this.knex
        .select("url")
        .from("user_photos")
        .orderBy(["user_id", "id"])
        .where("user_id", friendId)
        .whereNot("description", "profile")
        .whereNot("description", "test");

      let userDetails = {
        id: friendId,
        matchedTags,
        user_tags: otherUserTags,
        gallery: otherUserGallery,
        username,
        userAge: friendAge,
        gender,
      };
      // console.log({otherUserDetail})
      // console.log({userMatch})

      return userDetails;
    } catch (e) {
      throw e;
    }
  }

  async getLastMessage(roomIds: string[], userId: number/* , blockTime: Date */) {
    try {
      // console.log('received: ', relationship_id, ' and ', friend_relationship_id, ' and ', blockTime)
      const lastMessages = await this.knex.select('friend_id', 'user_id', 'message', 'created_at', 'distinct_id.room_id', 'unread')
        .from(
          this.knex.select('friend_id', 'user_id', 'message', 'chatroom_records.created_at', 'room_id')
            .distinctOn('room_id')
            .from('chatroom_records')
            .join('relationship', 'chatroom_records.relationship_id', 'relationship.id')
            .whereIn('room_id', roomIds)
            .orderBy(['room_id', { column: 'chatroom_records.created_at', order: 'desc' }])
            .as('distinct_id')
        )
        .leftJoin(this.knex.select('room_id').count('chatroom_records.status as unread').from('chatroom_records')
          .join('relationship', 'relationship_id', 'relationship.id')
          .where('chatroom_records.status', 'unread')
          .where('user_id', '<>', userId)
          .whereIn('room_id', roomIds)
          .groupBy('room_id')
          .as('user_unread'),
          'distinct_id.room_id', 'user_unread.room_id')
        .orderBy('created_at', 'desc')
          
      // const lastMessages = await this.knex.raw(/* sql */
      //   `select friend_id, user_id, message, created_at, X.room_id, unread from (select distinct on (room_id) 
      //   friend_id, user_id, message, chatroom_records.created_at, room_id from chatroom_records
      //   inner join relationship on chatroom_records.relationship_id = relationship.id
      //   where room_id in (${roomIds.map(_ => '?').join(',')}) 
      //   order by room_id asc, chatroom_records.created_at desc)
      //   as X inner join (select room_id, count(chatroom_records.status) as unread from chatroom_records inner join relationship
      //   on relationship_id = relationship.id  where chatroom_records.status = 'unread' and user_id != ? and room_id in (${roomIds.map(_ => '?').join(',')}) 
      //   group by room_id) as Y on X.room_id = Y.room_id
      //   order by created_at desc`
      // , [...roomIds, userId, ...roomIds]);
      // console.log(...roomIds)
      // console.log("Last message", lastMessages.rows)
        // .select('*')
        // .from(function() {
        //   this.select(
        //     "chatroom_records.created_at",
        //     "relationship.room_id",
        // user_id,
        // friend_id,
        // message,
        // chatroom_records.status,
        //   )
        //   .from("chatroom_records")
        //   .join(
        //       "relationship",
        //       "chatroom_records.relationship_id",
        //       "relationship.id"
        //   )
        //   .whereIn("relationship.room_id", roomIds)
        //   .orderBy([
        //     { column: "room_id", order: "asc" },
        //   ])
        //   .distinctOn('room_id')
        //   .as('room_id')
        // })
        // .orderBy('created_at', 'desc')
        // .as('ignored_alias')
          
        // { column: "chatroom_records.", order: "desc" },

      // const distinctLastMessage = [];
      // for (let id of roomIds) {
      //   console.log("room", id);
      //   const lastMessage = await this.knex
      //     .select(
      //       "user_id",
      //       "friend_id",
      //       "message",
      //       "chatroom_records.created_at",
      //       "chatroom_records.status"
      //     )
      //     .from("chatroom_records")
      //     .join(
      //       "relationship",
      //       "chatroom_records.relationship_id",
      //       "relationship.id"
      //     )
      //     .where("relationship.room_id", id)
      //     .orderBy([
      //       { column: "chatroom_records.created_at", order: "desc" },
      //       { column: "friend_id", order: "asc" },
      //     ])
      //     .limit(1);
      //   if (lastMessage.length) distinctLastMessage.push(lastMessage[0]);
      // }

      return lastMessages;
    } catch (e) {
      throw e;
    }
  }

  async messageList(userId: number, roomId: string) {
    try {
      // console.log('received: ', body)
      let relationshipIds = await this.getRelIdsByRoom(userId, roomId);
      let selfRelId = relationshipIds.selfRelId;
      let frdRelId = relationshipIds.frdRelId;
      // let friendId = await this.knex("relationship")
      //   .select("friend_id")
      //   .where("id", selfRelId);
      let allChats = await this.knex
        .select("*")
        .from("chatroom_records")
        .where("relationship_id", selfRelId)
        .orWhere("relationship_id", frdRelId)
        .orderBy("created_at", "desc")
        .limit(500);
      // allChats.map((item) => {
      //   if (item.relationship_id == frdRelId) item["sender"] = friendId;
      //   else item["sender"] = userId;
      // });
      return allChats;
    } catch (e) {
      throw e;
    }
  }

  async getRelIdsByRoom(userId: number, roomId: string) {
    try {
      // console.log("room",roomId)
      let relIds = await this.knex
        .select("id", "user_id")
        .from("relationship")
        .where("room_id", roomId);
      console.log("rel ids", relIds);
      if (relIds.length != 2) throw new Error("more than 2 users in this room");
      let selfRelId, frdRelId;
      relIds.forEach((id) => {
        if (id.user_id == userId) selfRelId = id.id;
        else frdRelId = id.id;
      });
      return { selfRelId, frdRelId };
    } catch (error) {
      throw new Error("error in getting id by room" + error);
    }
  }

  async insertMessage(message: string, roomId: string, senderId: number) {
    try {
      // console.log('received: ', message, ' and ', selfChatroom)
      let selfRelId = (await this.getRelIdsByRoom(senderId, roomId)).selfRelId;
      return await this.knex
        .insert({
          message: message,
          relationship_id: selfRelId,
          sender_id: senderId,
          status: "unread",
        })
        .into("chatroom_records")
        .returning("*");
    } catch (e) {
      throw e;
    }
  }

  //for matching model
  // async getLikeList(userId: number) {
  //     let friendIds = await this.knex.select('friend_id').from('relationship')
  //         .where('user_id', userId).where('trained', 0).orderBy('updated_at', 'desc')
  //     if (friendIds.length < 50) {
  //         return []
  //     }
  //     let frdId: number[] = friendIds.map(friend => friend.friend_id)
  //     // console.log(frdId)
  //     frdId = frdId.slice(0, 50)
  //     let result = []
  //     let ageId = (await this.knex.select('id').from('classes')
  //         .where('class', '年齡'))[0].id
  //     let ageTag = await this.knex.select('id').from('tags')
  //         .where('class_id', ageId)
  //     let agetagId = ageTag.map(tag => tag.id)
  //     for (let friendId of frdId) {
  //         let tagsArr = await this.knex.select('tag_id').from('user_tags')
  //             .where('user_id', friendId)
  //         let tags = tagsArr.map(tag => tag.tag_id)
  //         // let gender = (await this.knex.select('genders.gender')
  //         //                 .join('users','genders.id','=','users.gender_id')
  //         //                 .where('users.id',frdId))[0].gender
  //         let gender = (await this.knex('users')
  //             .join('genders', 'genders.id', '=', 'users.gender_id')
  //             .select('genders.gender'))[0].gender
  //         let genderTag
  //         if (gender == 'female') {
  //             genderTag = (await this.knex.select('id').from('tags')
  //                 .where('tag', '女'))[0].id
  //         } else if (gender == 'male') {
  //             genderTag = (await this.knex.select('id').from('tags')
  //                 .where('tag', '男'))[0].id
  //         } else {
  //             genderTag = (await this.knex.select('id').from('tags')
  //                 .where('tag', '其他'))[0].id
  //         }
  //         tags.push(genderTag)
  //         let birthday = (await this.knex.select('birthday').from('users')
  //             .where('id', friendId))[0].birthday
  //         let age = Date.now() - new Date(birthday).getTime()
  //         switch (true) {
  //             case (age < 22):
  //                 tags.push(agetagId[0])
  //                 break
  //             case (age < 26):
  //                 tags.push(agetagId[1])
  //                 break
  //             case (age < 31):
  //                 tags.push(agetagId[2])
  //                 break
  //             case (age < 36):
  //                 tags.push(agetagId[3])
  //                 break
  //             case (age < 41):
  //                 tags.push(agetagId[4])
  //                 break
  //             case (age < 51):
  //                 tags.push(agetagId[5])
  //                 break
  //             case (age < 61):
  //                 tags.push(agetagId[6])
  //                 break
  //             case (age > 60):
  //                 tags.push(agetagId[7])
  //                 break
  //         }
  //         let status = (await this.knex.select('status').from('relationship')
  //             .where('user_id', userId)
  //             .where('friend_id', friendId))[0].status
  //         let url = (await this.knex.select('url').from('user_photos')
  //             .where('user_id', friendId)
  //             .where('description', 'profile'))[0].url

  //         result.push({ friendId, tags, url, status })
  //     }
  //     return result
  // }

  async setMsgToRead(userId: number, roomId: string) {
    try {
      // let relationshipIds = await this.getRelIdsByRoom(userId, roomId);
      // const result =
        await this.knex("chatroom_records")
        .update({
          status: "read",
        })
        .where('relationship_id', function f(this: Knex) {
          this.select('id').from('relationship')
            .where('user_id', '<>', userId).where('room_id', roomId)
        })
        .andWhere("status", "unread")
        // .toSQL()
        // .returning('*');
      // console.log('test',result)
      return "updated to read";
    } catch (e) {
      throw e;
    }
  }

  async getCountOfUnread(id: number, blockTime: any) {
    try {
      return await this.knex("chatroom_records")
        .count("status")
        .where("relationship_id", id)
        .andWhere("status", "unread")
        .andWhere("created_at", "<", blockTime)
        .returning("*");
    } catch (e) {
      throw e;
    }
  }

  async getTotalUnreadMsg(userId: number) {
    try {
      // let relationships = await 

      // let relIds = relationships.map((item) => item.id);

      let count = await this.knex("chatroom_records")
        .count("status")
        .whereIn("relationship_id", this.knex("relationship")
          .select("id")
          .where("friend_id", userId)
          .where("status", "like"))
        .andWhere("status", "unread")
      console.log(count)
      return count;
    } catch (e) {
      throw e;
    }
  }

  async deleteMessagesFromLarger(selfID: number, fdID: number) {
    try {
      await this.knex("chatroom_records")
        .update({
          delete_by_larger: selfID,
        })
        .where(function () {
          this.where("relationship_id", selfID).orWhere(
            "relationship_id",
            fdID
          );
        })
        .andWhere(function () {
          this.where("status", "read").orWhere("status", "unread");
        });
      return "deleted chat records";
    } catch (e) {
      throw e;
    }
  }

  async deleteMessagesFromSmaller(selfID: number, fdID: number) {
    try {
      await this.knex("chatroom_records")
        .update({
          delete_by_smaller: selfID,
        })
        .where(function () {
          this.where("relationship_id", selfID).orWhere(
            "relationship_id",
            fdID
          );
        })
        .andWhere(function () {
          this.where("status", "read").orWhere("status", "unread");
        });
      return "deleted chat records";
    } catch (e) {
      throw e;
    }
  }

  async blockFd(relation_id: number) {
    try {
      // console.log('adding now() to block time')
      // let result =
      await this.knex("relationship")
        .update({
          blocked_time: "now()",
        })
        .where("id", relation_id)
        .returning("*");
      // console.log(result)
      return "added block time";
    } catch (e) {
      throw e;
    }
  }

  async checkBlockTime(relation_id: number) {
    try {
      // console.log('checking block time')
      return await this.knex("relationship")
        .select("blocked_time")
        .where("id", relation_id);
    } catch (e) {
      throw e;
    }
  }

  async unblockFd(relation_id: number) {
    try {
      // console.log('remove blocked time to unblock time')
      // let result =
      await this.knex("relationship")
        .update({
          blocked_time: null,
        })
        .where("id", relation_id)
        .returning("*");
      // console.log(result)
      return "removed block time";
    } catch (e) {
      throw e;
    }
  }
}
