import { Knex } from 'knex';
const zodiac = require('zodiac-signs')('en');
import { deleteFromS3 } from '../helpers/deleteFromS3';
import { Tag } from '../helpers/types';



export class ProfileService {

  constructor(private knex: Knex) { }

  async getProfile(id: number) {
    try {
      let profile = await this.knex.select('*').from('users')
        .where('id', id).first()
      delete profile['password']
      let pictures = await this.knex.select('*').from('user_photos')
        .where('user_id', profile.id)
      return { profile, pictures }
    } catch (e) {
      throw new Error(e)
    }
  }

  async getProfilePic(id: number) {
    try {
      let pictures = await this.knex.select('url','description').from('user_photos')
        .where('user_id', id).whereIn('description', ['original profile', 'profile'])
      return { pictures }
    } catch (e) {
      throw new Error(e)
    }
  }

  async getEmailById(id: number) {
    let email = (await this.knex.select('email').from('users')
      .where('id', id))[0].email
    return email

  }

  async uploadProfilePic(userId: number, profilePath: string, originalProfilePath: string) {
    try {
      let oldProfilePic = await this.knex.select('url').from('user_photos')
        .where('user_id', userId)
        .whereIn('description', ['profile', 'original profile'])
      console.log('oldProfilePic', oldProfilePic)
      let userCurrentStatus = (await this.knex('users').select('registration_status').where('id', userId))[0].registration_status
      let original, profile
      if (oldProfilePic.length > 0) {
        await this.knex.transaction(async (txn) => {
          try {
            profile = await txn('user_photos')
              .where('user_id', userId)
              .where('description', 'profile')
              .update({
                url: profilePath,
                created_at: this.knex.fn.now()
              })
              .returning('url')
            console.log('profile', profile)
            if (oldProfilePic.length > 1) {
              original = await txn('user_photos')
                .where('user_id', userId)
                .where('description', 'original profile')
                .update({
                  url: originalProfilePath,
                  created_at: this.knex.fn.now()
                })
                .returning('url')
                console.log('original', original)
            } else {
              original = await txn('user_photos')
                .insert({
                  user_id: userId,
                  url: originalProfilePath,
                  description: 'original profile'
                })
                .returning('url')
                // console.log('original insert', original)
            }
          } catch (e) {
            throw new Error('unable to update/add profile pics.')
          }
        })
        let deleteArr = []
        for (let url of oldProfilePic) {
          deleteArr.push(deleteFromS3(url.url))
        }
        Promise.all(deleteArr)

      } else {
        profile = await this.knex('user_photos')
          .insert({
            user_id: userId,
            url: profilePath,
            description: 'profile',
          }).returning('url')
          // console.log('profile else', profile)
        original = await this.knex('user_photos')
          .insert({
            user_id: userId,
            url: profilePath,
            description: 'original profile'
          }).returning('url')
          // console.log('original insert', original)
      }
      if (userCurrentStatus < 2) {
        userCurrentStatus = (await this.knex('users').update({
          registration_status: 2
        }).where('id', userId).returning('registration_status'))[0].registration_status
        // console.log('userCurrentStatus', userCurrentStatus)
      }
      return { profile, original, userCurrentStatus }
    } catch (e) {
      throw new Error('unable to update/add profile pics.')
    }
  }



  async updateProfile(newInfo: { gender: string, birthday: Date, username: string, text: string }, userId: number) {
    try {
      let profile = await this.getProfile(userId)
      if (profile.profile) {
        let genderArr = await this.knex.select('id').from('genders')
        const genderId = await this.knex.select('id').from('genders').where('gender', newInfo.gender)
        // if (newInfo.gender == 1) {
        //   gender = genderArr[0].id
        // } else if (newInfo.gender == 2) {
        //   gender = genderArr[1].id
        // }
        let gender = genderId[0].id
        let updatedProfile = await this.knex('users')
          .where('id', userId)
          .update({
            gender_id: gender,
            birthday: newInfo.birthday,
            username: newInfo.username,
            text_description: newInfo.text
          })
          .returning(['id', 'username', 'gender_id as gender', 'birthday', 'text_description'])
  
        let today = new Date(Date.now()).toDateString()
        let todayNum = Date.parse(today)
        let birthday = updatedProfile[0].birthday
        let age = (todayNum - birthday) / 1000 / 60 / 60 / 24 / 365.25
        let userAge: number = 0, userHoro: number = 0, userGender: number = 0
        let ageTag = await this.knex.select('tags.id as id')
          .from('tags')
          .join('classes', 'tags.class_id', 'classes.id')
          .where('classes.class', '年齡')
        let horoTag = await this.knex.select('tags.id as id')
          .from('tags')
          .join('classes', 'tags.class_id', 'classes.id')
          .where('classes.class', '星座')
        let genderTag = await this.knex.select('tags.id as id')
          .from('tags')
          .join('classes', 'tags.class_id', 'classes.id')
          .where('classes.class', '性別')
  
        switch (gender) {
          case (genderArr[0].id):
            userGender = genderTag[0].id
            break
          case (genderArr[1].id):
            userGender = genderTag[1].id
            break
          default:
            userGender = genderTag[2].id
            break
        }
  
        switch (true) {
          case (age < 22):
            userAge = ageTag[0].id
            break
          case (age < 26):
            userAge = ageTag[1].id
            break
          case (age < 31):
            userAge = ageTag[2].id
            break
          case (age < 36):
            userAge = ageTag[3].id
            break
          case (age < 41):
            userAge = ageTag[4].id
            break
          case (age < 51):
            userAge = ageTag[5].id
            break
          case (age < 61):
            userAge = ageTag[6].id
            break
          case (age > 60):
            userAge = ageTag[7].id
            break
          default:
            break
        }
  
        let date = birthday.getDate()
        let month = birthday.getMonth() + 1
        let horoscope = zodiac.getSignByDate({ day: date, month: month }).name
        switch (horoscope) {
          case ('Aries'):
            userHoro = horoTag[0].id
            break
          case ('Taurus'):
            userHoro = horoTag[1].id
            break
          case ('Cancer'):
            userHoro = horoTag[2].id
            break
          case ("Leo"):
            userHoro = horoTag[3].id
            break
          case ("Virgo"):
            userHoro = horoTag[4].id
            break
          case ("Gemini"):
            userHoro = horoTag[5].id
            break
          case ("Libra"):
            userHoro = horoTag[6].id
            break
          case ("Scorpio"):
            userHoro = horoTag[7].id
            break
          case ("Sagittarius"):
            userHoro = horoTag[8].id
            break
          case ("Capricorn"):
            userHoro = horoTag[9].id
            break
          case ("Aquarius"):
            userHoro = horoTag[10].id
            break
          case ("Pisces"):
            userHoro = horoTag[11].id
            break
          default:
            break
        }
        // console.log(userAge, userHoro, userGender, 'user id', userId)
        let combinedTags = ageTag.concat(horoTag.concat(genderTag))
        combinedTags = combinedTags.map(item => item.id)
        // console.log("combined", combinedTags)
        if (!userHoro || !userAge || !userGender) throw new Error('cannot get tags from gender, horoscope, or age')
        await this.knex.transaction(async (txn) => {
          try {
            let deletedTags = await txn('user_tags')
              .whereIn('tag_id', combinedTags)
              .where('user_id', userId).del()
            console.log("deleted: ", deletedTags)
            if (deletedTags > 3) throw new Error('should only delete max. of 3 tags')
            let insertTags = await txn('user_tags')
              .insert([{
                tag_id: userHoro,
                user_id: userId,
                position: 0,
              }, {
                tag_id: userAge,
                user_id: userId,
                position: 0,
              }, {
                tag_id: userGender,
                user_id: userId,
                position: 0,
              }]).returning('*')
            // console.log('insertTags', insertTags)
            if (insertTags.length != 3) throw new Error('unable to add tags')
          } catch (e) {
            console.log('unable to edit tags.')
            throw e 
          }
        })
        return updatedProfile
        // console.log("service: ",profile)
      } else {
        throw new Error('unable to find user')
      }
    } catch (e) {
      throw e
    }

  }

  async uploadVoice(url: string, userId: number) {
    try {
      let existingVoice = await this.knex.select('voice_description')
        .from('users')
        .where('id', userId).first()

      let updatedVoice = await this.knex('users')
        .where('id', userId)
        .update({
          voice_description: url,
        })
        .returning('voice_description')

      if (updatedVoice.length && existingVoice != null) deleteFromS3(existingVoice.voice_description)

      return updatedVoice
      // console.log("service: ",profile)
    }
    catch (e) {
      throw new Error('not uploaded')
    }
  }




  async getOwnUserTags(id: number) {
    try {
      let ageTag = await this.knex.select('tags.id as id')
        .from('tags')
        .join('classes', 'tags.class_id', 'classes.id')
        .where('classes.class', '年齡')
      let horoTag = await this.knex.select('tags.id as id')
        .from('tags')
        .join('classes', 'tags.class_id', 'classes.id')
        .where('classes.class', '星座')
      let genderTag = await this.knex.select('tags.id as id')
        .from('tags')
        .join('classes', 'tags.class_id', 'classes.id')
        .where('classes.class', '性別')

      let combinedTags = ageTag.concat(horoTag.concat(genderTag))
      combinedTags = combinedTags.map(item => item.id)

      return (await this.knex('user_tags')
        .join('tags', 'user_tags.tag_id', '=', 'tags.id')
        .join('classes', 'tags.class_id', '=', 'classes.id')
        .select('tags.id', 'tags.tag', 'classes.class', 'classes.id as classId', 'user_tags.id as dbPrimaryId')
        .whereNotIn('tags.id', combinedTags)
        .orderBy(['user_id', 'position'])
        .where('user_id', id))
    } catch (e) {
      throw e
    }
  }

  async checkEditUnique(id: number, nonDuplicableClasses: string[], isWishTag: boolean) {
    // console.log('getting tags from database: ', id)
    try {
      const result = await this.knex(isWishTag ? 'user_wish_tags' : 'user_tags')
        .select('class')
        .where('user_id', id)
        .join('tags', isWishTag ? 'user_wish_tags.tag_id' : 'user_tags.tag_id', '=', 'tags.id')
        .join('classes', 'tags.class_id', '=', 'classes.id')
        .whereIn('class', nonDuplicableClasses)
      return result
    } catch (e) {
      throw e
    }
  }

  async getTagsForUsers(ids: number[]) {
    // console.log('getting tags from database: ', id)
    try {
      return (await this.knex('user_tags')
        .join('tags', 'user_tags.tag_id', '=', 'tags.id')
        .select('tags.tag')
        .orderBy(['user_id', 'position'])
        .whereIn('user_id', ids))
    } catch (e) {
      throw e
    }
  }

  async getTagCount(id: number, isWishTag: boolean) {
    try {
      return (await this.knex(isWishTag ? 'user_wish_tags' : 'user_tags')
        .count('tag_id')
        .where('user_id', id))[0].count
    } catch (e) {
      throw e
    }
  }

  async getWishTags(id: number) {
    try {
      return (await this.knex('user_wish_tags')
        .join('tags', 'user_wish_tags.tag_id', '=', 'tags.id')
        .join('classes', 'tags.class_id', '=', 'classes.id')
        .select('tags.id', 'tags.tag', 'classes.class', 'classes.id as classId', 'user_wish_tags.id as dbPrimaryId')
        .orderBy(['user_id', 'position'])
        .where('user_id', id))
    } catch (e) {
      throw e
    }
    // console.log('getting wish tags from database: ', id)

  }

  async getWishTagCount(id: number) {
    try {
      return (await this.knex('user_wish_tags')
        .count('tag_id')
        .where('user_id', id))[0].count
    } catch (e) {
      throw e
    }
  }

  async getClass() {
    // console.log('getting class from database')
    try {
      return (await this.knex.select('*').from('classes'))
    } catch (e) {
      throw e
    }
  }

  async getAllTags() {
    // console.log('getting all tags from database')
    try {
      const allTags: Tag[] = await this.knex('tags')
        .join('classes', 'classes.id', '=', 'tags.class_id')
        .select('tags.id', 'tags.tag', 'classes.id as classId', 'classes.class')

      return allTags

      // return allTags.reduce((prev, acc, index, arr) => {
      //   const accumulated = (prev.class !== acc.class ? [...accumulated]) 

      // })
    } catch (e) {
      throw e
    }

  }

  async addUserTag(tagId: number, id: number) {
    // console.log('receive a add user tag request...: ', tag)
    try {
      let count = await this.getTagCount(id, false)
      console.log('count', count)
      if (count >= 16) {
        throw new Error('maximum quota of adding tags is met')
      }
      let checkDuplicate = await this.checkDuplicate(tagId, id, false, false)
      if (checkDuplicate === 'success') {
        const lastPositionQuery = await this.knex.max('position').from('user_tags').where('user_id', id);
        // console.log('tag id: ', tagID)
        const lastPosition = lastPositionQuery[0].max
        // console.log(lastPosition)
        const result = await this.knex
          .insert({
            tag_id: tagId,
            user_id: id,
            position: lastPosition + 1
          })
          .into('user_tags')
          .returning('id')
        
        const restructureQuery = await this.knex('user_tags')
          .join('tags', 'user_tags.tag_id', '=', 'tags.id')
          .join('classes', 'tags.class_id', '=', 'classes.id')
          .select('tags.id', 'tags.tag', 'classes.class', 'classes.id as classId', 'user_tags.id as dbPrimaryId')
          .where('user_id', id)
          .where('user_tags.id', result[0])
        
        console.log('addusertag', result, restructureQuery)

        return restructureQuery[0]
      } else throw new Error(checkDuplicate)

    } catch (e) {
      throw e
    }
  }

  async checkDuplicate(tagId: number, userId: number, isWishTag: boolean, isEditing: boolean) {
    try {
      let thisTag = await this.knex.select('tags.class_id as class_id', 'classes.class as class', 'tags.id as id')
        .from('tags')
        .join('classes', 'tags.class_id', 'classes.id')
        .where('tags.id', tagId).first()
      console.log("tagClass", thisTag)

      let classId = thisTag.class_id
      let uneditableClass = isWishTag ? [] : ["性別", "年齡", "星座"]
      if (uneditableClass.indexOf(thisTag.class) != -1) {
        return 'editing uneditable class'
      }
      let noDuplicateClasses = isWishTag ? ['習慣1', '習慣2'] : ["性別", "年齡", "星座", "習慣1", "習慣2"]
      //enter wishTagNoDuplicate in Array


      if (noDuplicateClasses.indexOf(thisTag.class) != -1) {
        if (!isEditing) {
          //check duplicate classes
          let previousTags =  await this.knex.select('tag_id').from(isWishTag ? 'user_wish_tags' : 'user_tags')
          .join('tags', isWishTag ? 'user_wish_tags.tag_id' : 'user_tags.tag_id', 'tags.id')
          .where('tags.class_id', classId)
          .where('user_id', userId);
          console.log("previousTags", previousTags)
          if (previousTags.length > 0) {
            return 'adding non-duplicatable class'
          }
        }
      } else {
        //check duplicate tags
        let previousTags = await this.knex.select('tag_id').from(isWishTag ? 'user_wish_tags' : 'user_tags')
          .where('tag_id', thisTag.id)
          .where('user_id', userId)
        if (previousTags.length > 0) return 'adding duplicating tag' + previousTags[0].tag_id
      }
      return 'success'
    } catch (e) {
      console.log('duplicate', e)
      throw e.message
    }
  }

  async updateUserTag(oldTagId: number, newTagTagId: number, id: number) {
    try {
      let checkDuplicate = await this.checkDuplicate(newTagTagId, id, false, true)
      // console.log('received oldTag: ', oldTag, ' and newTag: ', newTag, 'and id: ', id)
      if (checkDuplicate === 'success') {
        let updateTag = await this.knex('user_tags').update({
          tag_id: newTagTagId
        }).where('id', oldTagId).andWhere('user_id', id).returning('tag_id')
        // console.log('updateTag',updateTag)
        return updateTag
      } else {
        throw checkDuplicate
      }
    } catch (error) {
        throw error
    }
  }


  async addWishTag(tagId: number, id: number) {
    // console.log('receive a add wish tag request...: ', tag)
    try {
      let count = await this.getTagCount(id, true)
      console.log('count', count)
      if (count >= 16) {
        throw new Error('maximum quota of adding tags is met')
      }
      let checkDuplicate = await this.checkDuplicate(tagId, id, true, false)
      if (checkDuplicate === 'success') {
        const lastPositionQuery = await this.knex.max('position').from('user_wish_tags').where('user_id', id);
        // console.log('tag id: ', tagID)
        const lastPosition = lastPositionQuery[0].max
        // console.log(lastPosition)
        const result = await this.knex
          .insert({
            tag_id: tagId,
            user_id: id,
            position: lastPosition + 1
          })
          .into('user_wish_tags')
          .returning('id')
        
        const restructureQuery = await this.knex('user_wish_tags')
          .join('tags', 'user_wish_tags.tag_id', '=', 'tags.id')
          .join('classes', 'tags.class_id', '=', 'classes.id')
          .select('tags.id', 'tags.tag', 'classes.class', 'classes.id as classId', 'user_wish_tags.id as dbPrimaryId')
          .where('user_id', id)
          .where('user_wish_tags.id', result[0])
        
        console.log('addwishtag', result, restructureQuery)

        return restructureQuery[0]
      } else throw new Error(checkDuplicate)
      
    } catch (e) {
      throw e
    }

  }

  async deleteUserTag(tagId: number, id: number) {
    // console.log('receive a delete user tag request...: ', tag)
    try {
      // console.log('tag id: ', tagID)
      let thisTag = await this.knex.select('classes.class as class')
        .from('tags')
        .join('classes', 'tags.class_id', 'classes.id')
        .where('tags.id', tagId).first()
      let uneditableClass = ["性別", "年齡", "星座"]

      if (uneditableClass.indexOf(thisTag.class) != -1) {
        throw new Error('unable to delete this tag due special class')
      }
      await this.knex('user_tags').where('user_id', id).andWhere('tag_id', tagId).del()
      return ('deleted tag')
    } catch (e) {
      throw e
    }

  }

  async deleteWishTag(tagId: number, id: number) {
    try {
      // console.log('receive a delete user wish tag request...: ', tag)
      // console.log('tag id: ', tagID)
      const result = await this.knex('user_wish_tags').where('user_id', id).andWhere('tag_id', tagId).del()
      console.log(result)
      return ('deleted wish tag')
    } catch (e) {
      console.log(e)
      throw e
    }

  }

  async updateUsername(username: string, id: number) {
    try {
      // console.log('update username to database: ', username, ' and id: ', id)

      return (await this.knex('users')
        .update({
          username: username
        })
        .where('id', id).returning('username'))
    } catch (e) {
      throw e
    }

  }

  async updateGender(gender: number, id: number) {
    try {
      // console.log('add gender of new user to database: ', gender, ' and id: ', id)
      let userGender
      let genderArr = await this.knex.select('id').from('genders')
      if (gender == 1) userGender = genderArr[0].id
      else if (gender == 2) userGender = genderArr[1].id
      else userGender = genderArr[2].id
      await this.knex('users').update({
        gender_id: userGender
      }).where('id', id)
      return ('gender updated')
    } catch (e) {
      throw e
    }
  }

  async insertNewRegistrationInfo(birthday: string, habitOne: number, habitTwo: number, id: number) {
    // console.log("insert info")
    // let txn = this.knex
    let habit_1
    let habit_2
    await this.knex.transaction(async (txn) => {
      try {
        await txn('users').update({
          birthday: birthday,
          registration_status: 1
        }).where('id', id).returning('birthday')
        habit_1 = await txn.insert({
          user_id: id,
          tag_id: habitOne
        }).into('user_tags').returning('tag_id')
        habit_2 = await txn.insert({
          user_id: id,
          tag_id: habitTwo
        }).into('user_tags').returning('tag_id')
        console.log(id, habit_1, habit_2)
      } catch (e) {
        throw new Error('registration insert info failed')
      }
    })
    return ({ id: id, habit1: habit_1, habit2: habit_2 })
  }


  async getTagID(tagName: string) {
    try {
      // console.log('received tagName: ', tagName)
      const result = await this.knex.select('id').from('tags').where('tag', tagName)
      return result[0].id
    } catch (e) {
      throw e
    }

  }

  async updateUserWishTag(oldTagId: number, newTagTagId: number, id: number) {
    try {
      let checkDuplicate = await this.checkDuplicate(newTagTagId, id, true, true)
      // console.log('received oldTag: ', oldTag, ' and newTag: ', newTag, 'and id: ', id)
      if (checkDuplicate === 'success') {
        let updateTag = await this.knex('user_wish_tags').update({
          tag_id: newTagTagId
        }).where('id', oldTagId).andWhere('user_id', id).returning('tag_id')
        // console.log('updateTag',updateTag)
        return updateTag
      } else {
        throw checkDuplicate
      }
    } catch (e) {
      // console.log('update',e)
      throw e
    }

  }

  async getGallery(id: number) {
    try {
      // console.log('getting gallery from database where user id = ', id)
      const userPhotos = await this.knex.select('id', 'url', 'description').from('user_photos').orderBy('id').where('user_id', id).whereNot('description', 'profile')
      const updatedUserPhotos = userPhotos.sort(photo => photo.description === 'original profile' ? -1 : 1)
      return updatedUserPhotos
    } catch (e) {
      throw e
    }

  }

  async addGallery(urlArr: string[], id: number) {
    try {
      let [count] = await this.knex('user_photos')
        .count('id')
        .where('user_id', id)

      if (+count.count + urlArr.length <= 9) {
        let galleryArr = urlArr.map(url => {
          return { user_id: id, url: url, description: 'gallery' }
        })
        let photos = await this.knex.insert(galleryArr).into('user_photos').returning(['id', 'url'])
        return photos
      } else {
        let deleteAll = []
        for (let url of urlArr) {
          deleteAll.push(deleteFromS3(url))
        }
        Promise.all(deleteAll)
        return []
      }
    } catch (e) {
      throw new Error("unable to add photos " + e)
    }
  }

  // async editGallery(num: string, newUrl: URL, id: number) {
  //   try {
  //     console.log('handling edition of gallery for user')
  //     await this.knex('user_photos').update({
  //       url: newUrl
  //     }).where('user_id', id)
  //       // .andWhere('description', `photo-${num}`)
  //       .andWhere('url', `${num}`)
  //     return ('updated user photo')
  //   } catch (e) {
  //     throw e
  //   }

  // }

  async deleteGallery(urls: string[], id: number) {
    try {
      let deleteAll = []
      let unableDel = []
      for (let url of urls) {
        let result = await this.knex('user_photos')
          .where('user_id', id)
          .where('url', url)
          .whereNotIn('description', ['profile', 'original profile'])
          .del()
        if (result) deleteAll.push(deleteFromS3(url))
        else {
          unableDel.push(url)
        }
      }
      if (deleteAll.length > 0) {
        await Promise.all(deleteAll)
      }
      return { current: 'ok', unableDel }
    } catch (e) {
      throw e
    }

  }

  async loadUserProfile(userId: number) {
    try {
      const userDetails = await this.knex.from('users').select('username', 'email', 'coins', 'text_description', 'birthday', 'gender').where('users.id', userId)
        .join('genders', 'users.gender_id', 'genders.id')
      return userDetails
    } catch (e) {
      throw e
    }
  }

  async updateStageThree(id: number) {
    try {
      await this.knex('users').update({
        registration_status: 3
      }).where('id', id);
      return ('updated stage 3')
    } catch (e) {
      throw e
    }
  }


}