import { Knex } from 'knex';

export class ReportService {

  constructor(private knex: Knex) { }
  
  async submitReport(userID: number, targetID: number, title: string, content: string) {
    try {
      await this.knex.insert({
        user_id: userID,
        target_id: targetID,
        category: title,
        content: content,
        read: false,
        replied: false,
      }).into('reports')
      return {isSuccess: true}
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

}