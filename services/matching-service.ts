import { Knex } from "knex";
import { getUserGrid, calculateBuddy } from "../geohash";
const getSign = require("horoscope").getSign;
import {v4 as uuidv4} from 'uuid'

export class MatchingService {
  constructor(private knex: Knex) {}

  async insertGeolocation(info: {
    latitude: number;
    longitude: number;
    userId: number;
  }) {
    let { latitude, longitude, userId } = info;
    let userGrid = getUserGrid({ x: longitude, y: latitude });
    if (!userGrid) {
      return "no userGrid calculated";
    }
    // let buddyGridArray = calculateBuddy(userGrid)
    try {
      let grid = await this.knex
        .insert({
          grid: userGrid,
          user_id: userId,
          location: this.knex.raw(`point(${longitude}, ${latitude})`),
        })
        .into("user_locations")
        .returning(["grid", "location", "created_at"]);
      return grid[0];
    } catch (e) {
      throw e;
    }
  }

  async getUserLastGrid(userId: number) {
    try {
      let userLastLocation = await this.knex
        .select("grid")
        .from("user_locations")
        .where("user_id", userId)
        .orderBy("id", "desc")
        .limit(1);

      if (!userLastLocation[0]?.["grid"]) throw new Error("unknown grid");
      return userLastLocation[0]["grid"];
    } catch (e) {
      throw e;
    }
  }

  async getUserLastCoordinates(userId: number) {
    try {
      let userLastLocation = await this.knex
        .select("location")
        .from("user_locations")
        .where("user_id", userId)
        .orderBy("id", "desc")
        .limit(1);
      if (!userLastLocation[0]?.["location"])
        throw new Error("unknown coordinates");
      return userLastLocation[0]["location"];
    } catch (e) {
      throw e;
    }
  }

  async getUserLastGridWithLocation(userId: number) {
    try {
      let userLastLocation = await this.knex
        .select("grid", "location")
        .from("user_locations")
        .where("user_id", userId)
        .orderBy("id", "desc")
        .limit(1);

      if (!userLastLocation[0]?.["location"]) {
        throw new Error("unknown coordinates");
      }
      return userLastLocation;
    } catch (e) {
      throw e;
    }
  }

  async findBuddies(userGrid: string, userId: number) {
    let buddyGridArray = calculateBuddy(userGrid);
    // let buddies: any[] = []
    try {
      // for (let buddyGrid of buddyGridArray) {
      let buddiesNearby = await this.knex
        .select("user_locations.user_id", "users.username")
        .from("user_locations")
        .join("users", "user_locations.user_id", "=", "users.id")
        .whereIn("grid", buddyGridArray)
        .whereNot("user_id", userId)
        .distinct("user_id");
      // if (buddiesNearby.length > 0) {
      //   buddiesNearby.forEach(buddyNearby => buddies.push(buddyNearby))
      // }
      // }
      // console.log("buddies:", buddies)
      // return Array.from(new Set(buddies))
      return buddiesNearby;
    } catch (e) {
      throw e;
    }
  }

  async removeSwipedPeople(buddiesArr: any[], userId: number) {
    let buddyMap = new Map();
    buddiesArr.forEach((item) => {
      buddyMap.set(item.user_id, item);
    });
    // let buddiesIdTotal = buddies.map((bud) => bud.user_id)
    let timeNow = Date.now();
    let targetTime = new Date(timeNow - 1000 * 60 * 24);
    // console.log(date)

    let swipedList = await this.knex
      .select("friend_id")
      .from("relationship")
      .where("user_id", userId)
      .where("updated_at", ">", targetTime);
    swipedList.forEach((item) => {
      // console.log('delete', item.friend_id)
      buddyMap.delete(item.friend_id);
    });

    let likedList = await this.knex
      .select("friend_id")
      .from("relationship")
      .where("user_id", userId)
      .where("status", "like")
      .where("updated_at", "<=", targetTime);
    // console.log('likeList', likedList)
    likedList.forEach((item) => {
      // console.log('delete', item.friend_id)
      buddyMap.delete(item.friend_id);
    });

    let combinedList = swipedList.concat(likedList);
    let combined = combinedList.map((item) => item.friend_id as number);
    // swipedList = swipedList.concat(likedList)
    // for (let item of likedList) {
    //   swipedList.push(item)
    // }
    // for (let item of skipUserIds){
    //   if (buddiesIdTotal.includes(item)){
    //     buddies.splice(buddiesIdTotal.indexOf(item),1)
    //     buddiesIdTotal.splice(buddiesIdTotal.indexOf(item),1)
    //     if (item == 540){
    //       console.log('left:',buddiesIdTotal.indexOf(item))
    //       console.log(buddies.indexOf(item)==buddiesIdTotal.indexOf(item) )
    //     }
    //   }
    // }
    // console.log('after:',buddiesIdTotal.indexOf(540))
    // console.log('buddiesRemained',buddies.length)
    return {
      buddiesRemained: Array.from(buddyMap.values()),
      removedList: combined,
    };
  }
  async matchUserWishTagsWithOtherUserTags(
    userId: number,
    otherUserNameAndIds: any[]
  ) {
    try {
      let otherUserIds = otherUserNameAndIds.map(
        (otherUserNameAndId) => otherUserNameAndId.user_id
      );
      // let otherUserIds = await this.removeSwipedPeople(otherUserIdsBeforeRemove, userId)

      const userWishTags = await this.knex
        .select("tag_id")
        .from("user_wish_tags")
        .where("user_id", userId);
      // console.log({userWishTags})
      const userWishTagsMap = userWishTags.map(
        (userWishTag) => userWishTag["tag_id"]
      );
      // , : otherUserTags, : otherUserGallery, : otherUserNameAndIds[i].username, })
      let otherUserDetail = [];
      try {
        for (let i = 0; i < otherUserIds.length; i++) {
          const otherUserTags = await this.knex("user_tags")
            .join("tags", "user_tags.tag_id", "=", "tags.id")
            .select("*")
            .orderBy("created_at", "asc")
            .where("user_id", otherUserIds[i]);
          // console.log({otherUserTags})
          // const otherUserTagsMap = otherUserTags.map(otherUserTag => otherUserTag['tag_id'])

          const continueNextBuddyAfterGenderTest =
            await this.checkUserGenderPreference(
              userWishTagsMap,
              otherUserIds,
              i
            );
          if (continueNextBuddyAfterGenderTest === false) {
            continue;
          }

          // console.log(otherUserIds[i], 'passed gender test')

          let [{ birthday: otherUserBdayTimeStamp }] = await this.knex
            .select("birthday")
            .from("users")
            .where("id", otherUserIds[i]);

          const otherUserBday = new Date(otherUserBdayTimeStamp);

          const { agePreferencePoint, otherUserAge } =
            await this.checkUserAgePreference(userWishTagsMap, otherUserBday);

          // console.log(otherUserIds[i], 'passed age test', agePreferencePoint)
          let gender = (
            await this.knex("users")
              .select("gender_id")
              .where("id", otherUserIds[i])
          )[0].gender_id;

          let matchedTags = otherUserTags
            .filter((userTag) => {
              // console.log("userTagMap",userTag['tag_id'])
              // console.log("userWishTagsMap",userWishTags.map(userWishTag => userWishTag['tag_id']))
              return userWishTagsMap.includes(userTag["tag_id"]);
            })
            .map((tag) => tag.tag);
          // console.log({matchedTags})

          const horoscopeMatched = await this.checkUserHoroscopePreference(
            userWishTagsMap,
            otherUserBday,
            matchedTags
          );

          // console.log(otherUserIds[i], 'passed horoscope test', horoscopeMatched, userHoroscopePreference)

          const matchedPercent =
            (matchedTags.length + agePreferencePoint) / userWishTagsMap.length;
          const randomPart = Math.random() / userWishTagsMap.length;

          // console.log(otherUserIds[i], 'passed tag test')
          const otherUserGallery = await this.knex
            .select("url", "description")
            .from("user_photos")
            .orderBy(["user_id", "id"])
            .where("user_id", otherUserIds[i])
            .whereNot("description", "profile")
            .whereNot("description", "test");

          // console.log('otherUserGallery', otherUserGallery)
          const profilePic = otherUserGallery.splice(
            otherUserGallery.findIndex(
              (item) => item.description === "original profile"
            ),
            1
          );

          // console.log('profilePic', profilePic[0])

          otherUserDetail.push({
            id: otherUserIds[i],
            matchedTags,
            gender,
            user_tags: otherUserTags.map((tag) => tag.tag),
            gallery: otherUserGallery,
            profilePic: profilePic[0],
            username: otherUserNameAndIds[i].username,
            matchedPercent,
            userAge: otherUserAge,
            horoscopeMatched,
            agePreferencePoint,
            randomPart,
          });
          // console.log({otherUserDetail})
          // console.log({userMatch})
        }
      } catch (e) {
        throw e;
      }

      if (otherUserDetail.length !== 0) {
        otherUserDetail.sort((a, b) => {
          return (
            b.matchedPercent + b.randomPart - a.matchedPercent - a.randomPart
          );
        });
        otherUserDetail = otherUserDetail.slice(0, 10);
        // console.log(otherUserDetail)
        // otherUserDetail = otherUserDetail.map(oneUserDetail => {
        //   return {id: oneUserDetail.id, matchedTags: oneUserDetail.matchedTags, user_tags: oneUserDetail.user_tags, gallery: oneUserDetail.gallery, username: oneUserDetail.username}
        // })
        // console.log(otherUserDetail)

        const latestMatchedResult = await this.getLatestInsertedLocationResult(
          userId
        );

        let matchedAtResult = await this.knex("user_locations")
          .where("id", latestMatchedResult[0].id)
          .update({
            matched_at: this.knex.fn.now(),
            result: JSON.stringify(otherUserDetail),
          })
          .returning("matched_at");

        const buddiesRes = { otherUserDetail, matchedTime: matchedAtResult[0] };
        // console.log(buddiesRes)

        return buddiesRes;
      } else {
        throw new Error("no matchUserDetail");
      }

      // const matchedAtResult = await this.knex('user_locations')
      //   .where({ id: nearestInsertTimeId[0]['id'] })
    } catch (e) {
      throw e;
    }
  }

  async getLatestInsertedLocationResult(userId: number) {
    try {
      const latestMatchedResult = await this.knex
        .select("id")
        .from("user_locations")
        .where("user_id", userId)
        .orderBy("id", "desc")
        .limit(1);
      return latestMatchedResult;
    } catch (e) {
      throw e;
    }
  }

  async getLatestMatchedResultNotNull(userId: number) {
    try {
      const latestMatchedResult = await this.knex
        .select("id")
        .from("user_locations")
        .where("user_id", userId)
        .whereNotNull("result")
        .orderBy("id", "desc")
        .limit(1);
      return latestMatchedResult;
    } catch (e) {
      throw e;
    }
  }

  async checkUserGenderPreference(
    userWishTagsMap: any[],
    otherUserIds: number[],
    i: number
  ) {
    // female, male, other gender
    try {
      const userGenderPreference = userWishTagsMap.filter((userWishTag) => {
        return userWishTag >= 56 && userWishTag <= 58;
      });
      let continueNextBuddy = true;

      if (userGenderPreference.length > 0) {
        const [{ gender: otherUserGender }] = await this.knex
          .select("gender")
          .from("genders")
          .where(
            "id",
            "=",
            this.knex
              .select("gender_id")
              .from("users")
              .where("id", otherUserIds[i])
          );
        // console.log({otherUserGender})

        // console.log(userGenderPreference[0])
        switch (userGenderPreference[0]) {
          case 56:
            if (otherUserGender !== "female") {
              // console.log(otherUserIds[i], 'not match female')
              // continue
              continueNextBuddy = false;
            }
            break;
          case 57:
            if (otherUserGender !== "male") {
              // console.log(otherUserIds[i], 'not match male')
              // continue
              continueNextBuddy = false;
            }
            break;
          case 58:
            if (otherUserGender !== "其他") {
              // console.log(otherUserIds[i], 'not match 其他')
              // continue
              continueNextBuddy = false;
            }
            break;
          default:
            break;
        }
      }
      return continueNextBuddy;
    } catch (e) {
      throw e;
    }
  }

  async checkUserAgePreference(userWishTagsMap: any[], otherUserBday: Date) {
    try {
      const userAgePreference = userWishTagsMap.filter((userWishTag) => {
        return userWishTag >= 59 && userWishTag <= 66;
      });
      let agePreferencePoint = 0;

      const ageWeightingMinus = -0.15;

      if (!otherUserBday) {
        otherUserBday = new Date("2000-01-01");
      }

      const otherUserAge = Math.round(
        (Date.now() - otherUserBday.getTime()) / 1000 / 60 / 60 / 24 / 365
      );

      if (userAgePreference.length > 0) {
        let ageMatched = 0;
        for (let i = 0; i < userAgePreference.length; i++) {
          switch (userAgePreference[i]) {
            case 59:
              if (otherUserAge >= 22 || otherUserAge <= 17) {
                // console.log(otherUserIds[i], 'not match 18-21')
                // continue
                agePreferencePoint =
                  Math.abs(otherUserAge - 22) * ageWeightingMinus;
              } else {
                ageMatched = 1;
              }
              break;
            case 60:
              if (otherUserAge >= 26 || otherUserAge <= 21) {
                // console.log(otherUserIds[i], 'not match 22-25')
                // continue
                agePreferencePoint =
                  Math.abs(otherUserAge - (26 + 21) / 2) * ageWeightingMinus;
              } else {
                ageMatched = 1;
              }
              break;
            case 61:
              if (otherUserAge >= 31 || otherUserAge <= 25) {
                // console.log(otherUserIds[i], 'not match 26-30')
                // continue
                agePreferencePoint =
                  Math.abs(otherUserAge - (31 + 25) / 2) * ageWeightingMinus;
              } else {
                ageMatched = 1;
              }
              break;
            case 62:
              if (otherUserAge >= 36 || otherUserAge <= 30) {
                // console.log(otherUserIds[i], 'not match 31-35')
                // continue
                agePreferencePoint =
                  Math.abs(otherUserAge - (36 + 30) / 2) * ageWeightingMinus;
              } else {
                ageMatched = 1;
              }
              break;
            case 63:
              if (otherUserAge >= 41 || otherUserAge <= 35) {
                // console.log(otherUserIds[i], 'not match 36-40')
                // continue
                agePreferencePoint =
                  Math.abs(otherUserAge - (41 + 35) / 2) * ageWeightingMinus;
              } else {
                ageMatched = 1;
              }
              break;
            case 64:
              if (otherUserAge >= 51 || otherUserAge <= 40) {
                // console.log(otherUserIds[i], 'not match 41-50')
                // continue
                agePreferencePoint =
                  Math.abs(otherUserAge - (51 + 40) / 2) * ageWeightingMinus;
              } else {
                ageMatched = 1;
              }
              break;
            case 65:
              if (otherUserAge >= 61 || otherUserAge <= 50) {
                // console.log(otherUserIds[i], 'not match 51-60')
                // continue
                agePreferencePoint =
                  Math.abs(otherUserAge - (61 + 50) / 2) * ageWeightingMinus;
              } else {
                ageMatched = 1;
              }
              break;
            case 66:
              if (otherUserAge <= 60) {
                // console.log(otherUserIds[i], 'not match 60+')
                // continue
                agePreferencePoint =
                  Math.abs(otherUserAge - 60) * ageWeightingMinus;
              } else {
                ageMatched = 1;
              }
              break;
            default:
              break;
          }
        }
        if (ageMatched === 1) {
          agePreferencePoint = 1;
        }
      }
      return { agePreferencePoint, otherUserAge };
    } catch (e) {
      throw e;
    }
  }

  async checkUserHoroscopePreference(
    userWishTagsMap: any[],
    otherUserBday: Date,
    matchedTags: any[]
  ) {
    try {
      let horoscopeMatched = 0;

      const userHoroscopePreference = userWishTagsMap.filter((userWishTag) => {
        return userWishTag >= 67 && userWishTag <= 78;
      });
      if (userHoroscopePreference.length > 0 && otherUserBday) {
        const newDate = new Date(otherUserBday);
        const month = newDate.getMonth() + 1;
        const day = newDate.getDate();
        const horoscope = getSign({ month: month, day: day });

        for (
          let userHoroscopePreferenceLength = 0;
          userHoroscopePreferenceLength < userHoroscopePreference.length;
          userHoroscopePreferenceLength++
        ) {
          switch (userHoroscopePreference[userHoroscopePreferenceLength]) {
            case 67:
              if (horoscope === "Aries") {
                horoscopeMatched += 1;
                matchedTags.push("Aries");
              }
              break;
            case 68:
              if (horoscope === "Taurus") {
                horoscopeMatched += 1;
                matchedTags.push("Taurus");
              }
              break;
            case 69:
              if (horoscope === "Cancer") {
                horoscopeMatched += 1;
                matchedTags.push("Cancer");
              }
              break;
            case 70:
              if (horoscope === "Leo") {
                horoscopeMatched += 1;
                matchedTags.push("Leo");
              }
              break;
            case 71:
              if (horoscope === "Virgo") {
                horoscopeMatched += 1;
                matchedTags.push("Virgo");
              }
              break;
            case 72:
              if (horoscope === "Gemini") {
                horoscopeMatched += 1;
                matchedTags.push("Gemini");
              }
              break;
            case 73:
              if (horoscope === "Libra") {
                horoscopeMatched += 1;
                matchedTags.push("Libra");
              }
              break;
            case 74:
              if (horoscope === "Scorpio") {
                horoscopeMatched += 1;
                matchedTags.push("Scorpio");
              }
              break;
            case 75:
              if (horoscope === "Sagittarius") {
                horoscopeMatched += 1;
                matchedTags.push("Sagittarius");
              }
              break;
            case 76:
              if (horoscope === "Capricorn") {
                horoscopeMatched += 1;
                matchedTags.push("Capricorn");
              }
              break;
            case 77:
              if (horoscope === "Aquarius") {
                horoscopeMatched += 1;
                matchedTags.push("Aquarius");
              }
              break;
            case 78:
              if (horoscope === "Pisces") {
                horoscopeMatched += 1;
                matchedTags.push("Pisces");
              }
              break;
            default:
              break;
          }
        }
      }
      return horoscopeMatched;
    } catch (e) {
      throw e;
    }
  }

  async getUserLastTimeMatchResult(userId: number) {
    try {
      const userLastTimeMatchResult = await this.knex
        .select("result", "updated_result")
        .from("user_locations")
        .where("user_id", userId)
        .whereNotNull("result")
        .orderBy("id", "desc")
        .limit(1);

      // console.log("userLastTimeMatchResult",userLastTimeMatchResult[0])

      return userLastTimeMatchResult[0];
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  async randomGenUserIdsForMatch(length: number, ids?: number[]) {
    try {
      let res;
      if (ids) {
        res = await this.knex
          .select("id as user_id", "username")
          .from("users")
          .whereNotIn("id", ids)
          .orderByRaw("RANDOM()")
          .limit(length);
      } else {
        res = await this.knex
          .select("id as user_id", "username")
          .from("users")
          .orderByRaw("RANDOM()")
          .limit(length);
      }
      // const res: any[] = (await this.knex.raw(/* sql */`SELECT id as user_id,username from users where not id in ($1) ORDER BY  FROM users)) LIMIT ?`, [length])).rows
      // console.log('random:',res)
      // const resMap = res.map(resp => resp?.id)
      return res;
    } catch (e) {
      throw e;
    }
  }

  async unlikeOtherUser(userId: number, likedUserId: number) {
    try {
      let checkRelationshipDuplicate = await this.knex
        .select("*")
        .from("relationship")
        .where("user_id", userId)
        .where("friend_id", likedUserId);

      if (checkRelationshipDuplicate.length > 0) {
        let userLastLocation = await this.getUserLastCoordinates(userId);
        const updateRelationshipResult = await this.knex("relationship")
          .where("user_id", userId)
          .where("friend_id", likedUserId)
          .update({
            updated_at: this.knex.fn.now(),
            friend_id: likedUserId,
            user_id: userId,
            status: "unlike",
            trained: 0,
            match_coordinates: userLastLocation,
          })
          .returning("*");
        return updateRelationshipResult[0];
      } else {
        let insertRelationship = await this.knex
          .insert({
            friend_id: likedUserId,
            user_id: userId,
            status: "unlike",
          })
          .into("relationship")
          .returning("*");
        return insertRelationship[0];
      }
    } catch (e) {
      throw e;
    }
  }

  async likeOtherUser(userId: number, likedUserId: number) {
    try {
      let checkRelationshipDuplicate = await this.knex
        .select("*")
        .from("relationship")
        .where("user_id", userId)
        .where("friend_id", likedUserId);

      let userLastLocation = await this.getUserLastCoordinates(userId);
      let crushCheckResult = await this.checkCrushHappen(userId, likedUserId);
      let room_id = crushCheckResult? crushCheckResult[0].room_id : uuidv4() 
      if (checkRelationshipDuplicate.length > 0) {
        const updateRelationshipResult = await this.knex("relationship")
          .where("user_id", userId)
          .where("friend_id", likedUserId)
          .update({
            friend_id: likedUserId,
            user_id: userId,
            status: "like",
            trained: 0,
            room_id:room_id,
            match_coordinates: this.knex.raw(
              `point(${userLastLocation.y}, ${userLastLocation.x})`,
            ),
          })
          .returning("*");
        return {likedRelationship:updateRelationshipResult[0],crush:crushCheckResult!=0};
      } else {
        let insertRelationship = await this.knex
          .insert({
            friend_id: likedUserId,
            user_id: userId,
            status: "like",
            trained: 0,
            room_id:room_id,
            match_coordinates: this.knex.raw(
              `point(${userLastLocation.y}, ${userLastLocation.x})`
            ),
          })
          .into("relationship")
          .returning("*");
        return {likedRelationship:insertRelationship[0],crush:crushCheckResult!=0};
      }
    } catch (e) {
      throw e;
    }
  }

  async checkCrushHappen(userId: number, likedUserId: number) {
    try {
      const crushHappenResult = await this.knex
        .select("id", "friend_id", "user_id", "room_id")
        .from("relationship")
        .where("user_id", likedUserId)
        .where("friend_id", userId)
        .where("status", "like")
        .whereNotNull("room_id")
        .orderBy("id", "desc");

      if (crushHappenResult.length == 1) return crushHappenResult;
      else if (crushHappenResult.length == 0) return 0;
      else
        throw new Error("more than 1 crush result from this user/friend pair");
    } catch (e) {
      throw e;
    }
  }

  async updateMatchedResultForUI(userId: number, likedUserId: number) {
    try {
      const userLastTimeMatchResult = await this.getUserLastTimeMatchResult(
        userId
      );

      if (!userLastTimeMatchResult) return;

      let updatedResult: Array<Object>;
      if (userLastTimeMatchResult?.updated_result) {
        // console.log({ userLastTimeMatchResult: userLastTimeMatchResult.updated_result })
        updatedResult = userLastTimeMatchResult.updated_result;
      } else {
        // console.log({ userLastTimeMatchResult: userLastTimeMatchResult.result })
        updatedResult = userLastTimeMatchResult.result;
      }

      const updatedResultFiltered = updatedResult.filter((result) => {
        return result["id"] !== likedUserId;
      });

      const latestMatchedResult = await this.getLatestMatchedResultNotNull(
        userId
      );
      // console.log('latestMatchedResult',latestMatchedResult[0])
      // console.log('updatedResultFiltered',updatedResultFiltered)

      const updateMatchedResult = await this.knex("user_locations")
        .where("id", latestMatchedResult[0].id)
        .update({
          updated_at: this.knex.fn.now(),
          updated_result: JSON.stringify(updatedResultFiltered),
        })
        .returning("updated_result");

      return { updateMatchedResult, updatedResultFiltered };
    } catch (e) {
      throw e;
    }
  }
  async setFriendsTrained(trainedRelationship: number[], userId: number) {
    try {
      for (let id of trainedRelationship) {
        await this.knex("relationship")
          .where("user_id", userId)
          .where("friend_id", id)
          .update({
            trained: 1,
          });
      }
    } catch (e) {
      console.log(e);
    }
  }
  async getLikeList(userId: number) {
    let friendIds = await this.knex
      .select("friend_id")
      .from("relationship")
      .where("user_id", userId)
      .where("trained", 0)
      .orderBy("updated_at", "desc");
    if (friendIds.length < 50) {
      return [];
    }
    let frdId: number[] = friendIds.map((friend) => friend.friend_id);
    // console.log(frdId)
    frdId = frdId.slice(0, 50);
    let result = [];
    let ageId = (
      await this.knex.select("id").from("classes").where("class", "年齡")
    )[0].id;
    let ageTag = await this.knex
      .select("id")
      .from("tags")
      .where("class_id", ageId);
    let agetagId = ageTag.map((tag) => tag.id);
    for (let friendId of frdId) {
      let tagsArr = await this.knex
        .select("tag_id")
        .from("user_tags")
        .where("user_id", friendId);
      let tags = tagsArr.map((tag) => tag.tag_id);
      // let gender = (await this.knex.select('genders.gender')
      //                 .join('users','genders.id','=','users.gender_id')
      //                 .where('users.id',frdId))[0].gender
      let gender = (
        await this.knex("users")
          .join("genders", "genders.id", "=", "users.gender_id")
          .select("genders.gender")
      )[0].gender;
      let genderTag;
      if (gender == "female") {
        genderTag = (
          await this.knex.select("id").from("tags").where("tag", "女")
        )[0].id;
      } else if (gender == "male") {
        genderTag = (
          await this.knex.select("id").from("tags").where("tag", "男")
        )[0].id;
      } else {
        genderTag = (
          await this.knex.select("id").from("tags").where("tag", "其他")
        )[0].id;
      }
      tags.push(genderTag);
      let birthday = (
        await this.knex.select("birthday").from("users").where("id", friendId)
      )[0].birthday;
      let age = Date.now() - new Date(birthday).getTime();
      switch (true) {
        case age < 22:
          tags.push(agetagId[0]);
          break;
        case age < 26:
          tags.push(agetagId[1]);
          break;
        case age < 31:
          tags.push(agetagId[2]);
          break;
        case age < 36:
          tags.push(agetagId[3]);
          break;
        case age < 41:
          tags.push(agetagId[4]);
          break;
        case age < 51:
          tags.push(agetagId[5]);
          break;
        case age < 61:
          tags.push(agetagId[6]);
          break;
        case age > 60:
          tags.push(agetagId[7]);
          break;
      }
      let status = (
        await this.knex
          .select("status")
          .from("relationship")
          .where("user_id", userId)
          .where("friend_id", friendId)
      )[0].status;
      let url = (
        await this.knex
          .select("url")
          .from("user_photos")
          .where("user_id", friendId)
          .where("description", "profile")
      )[0].url;

      result.push({ friendId, tags, url, status });
    }
    return result;
  }

  async payToRematch(
    userId: number,
    prioritizeResult: (
      userId: number,
      buddies: any[]
    ) => Promise<{
      matchedTime: any;
      otherUserDetail: any[];
    }>
  ) {
    try {
      const matchedResult = await this.knex.transaction(async (txn) => {
        try {
          const coinBalance = await txn("users")
            .select("coins")
            .where("id", userId);
          const coinBalanceUpdated = coinBalance[0].coins - 1;
          let coinBalanceAfterUsed;

          if (coinBalanceUpdated >= 0) {
            coinBalanceAfterUsed = await txn("users")
              .update("coins", coinBalanceUpdated)
              .where("id", userId)
              .returning("coins");
            await txn("coin_records").insert({
              transaction: -1,
              user_id: userId,
              description: "rematch",
              created_by: "system",
            });
          } else {
            throw new Error("not enough balance");
          }
          const userLastLocation = await this.getUserLastGrid(userId);
          let buddies = await this.findBuddies(userLastLocation, userId);
          // console.log("buddies:", buddies)
          if (buddies.length < 10) {
            const remainingBuddyNumber = 10 - buddies.length;
            const randomMatches: number[] = await this.randomGenUserIdsForMatch(
              remainingBuddyNumber
            );
            randomMatches.forEach((randomMatch) => buddies.push(randomMatch));
          }
          const finalResult = await prioritizeResult(userId, buddies);
          // console.log('matchservicefinalresult', finalResult)
          if (!finalResult) {
            throw new Error("no result");
          }

          // throw new Error(finalResult.toString())
          return { finalResult, coinBalanceAfterUsed };
        } catch (e) {
          throw e;
        }
      });

      // let userLastLocation = await this.knex.select('grid')
      // .from('user_locations')
      // .where('user_id', userId)
      // .orderBy('id', 'desc')
      // .limit(1)
      return matchedResult;
    } catch (e) {
      throw e;
    }
  }

  async checkAfter15Mins(userId: number) {
    try {
      let lastTimeInsert = await this.knex
        .select("matched_at")
        .from("user_locations")
        .where("user_id", userId)
        .whereNotNull("matched_at")
        .orderBy("id", "desc")
        .limit(1);
      // console.log({ lastTimeInsert })
      return lastTimeInsert;
    } catch (e) {
      throw e;
    }
  }

  async getBuddiesTags(buddiesId: number[]) {
    let result = [];
    buddiesId = buddiesId.sort(() => 0.5 - Math.random());
    if (buddiesId.length > 100) buddiesId = buddiesId.slice(0, 100);
    for (let buddyId of buddiesId) {
      let buddyTags = await this.knex("user_tags")
        .select("tag_id")
        .where("user_id", buddyId);
      let buddyTagsId = buddyTags.map((tag) => tag.tag_id);
      let genderTag = await this.getGenderTag(buddyId);
      buddyTagsId.push(genderTag);
      let ageTag = await this.getAgeTag(buddyId);
      buddyTagsId.push(ageTag);
      buddyTagsId.push(await this.getHoroscopeTag(buddyId));
      let url = (
        await this.knex
          .select("url")
          .from("user_photos")
          .where("user_id", buddyId)
          .where("description", "profile")
      )[0].url;
      result.push({ buddyId, buddyTagsId, url });
    }
    return result;
  }

  async getGenderTag(userId: number) {
    let gender = (
      await this.knex("users")
        .join("genders", "genders.id", "=", "users.gender_id")
        .select("genders.gender")
        .where("users.id", userId)
    )[0].gender;
    let genderTag;
    if (gender == "female") {
      genderTag = (
        await this.knex.select("id").from("tags").where("tag", "女")
      )[0].id;
    } else if (gender == "male") {
      genderTag = (
        await this.knex.select("id").from("tags").where("tag", "男")
      )[0].id;
    } else {
      genderTag = (
        await this.knex.select("id").from("tags").where("tag", "其他")
      )[0].id;
    }
    return genderTag;
  }

  async getAgeTag(userId: number) {
    let ageId = (
      await this.knex.select("id").from("classes").where("class", "年齡")
    )[0].id;
    let ageTag = await this.knex
      .select("id")
      .from("tags")
      .where("class_id", ageId);
    let agetagId = ageTag.map((tag) => tag.id);
    let birthday = (
      await this.knex.select("birthday").from("users").where("id", userId)
    )[0].birthday;
    let age = Math.floor(
      (Date.now() - new Date(birthday).getTime()) / 31536000000
    );
    switch (true) {
      case age < 22:
        return agetagId[0];
      case age < 26:
        return agetagId[1];
      case age < 31:
        return agetagId[2];
      case age < 36:
        return agetagId[3];
      case age < 41:
        return agetagId[4];
      case age < 51:
        return agetagId[5];
      case age < 61:
        return agetagId[6];
      case age > 60:
        return agetagId[7];
    }
  }
  async getHoroscopeTag(userId: number) {
    let birthday = (
      await this.knex.select("birthday").from("users").where("id", userId)
    )[0].birthday;
    let newDate = new Date(birthday);
    let month = newDate.getMonth() + 1;
    let day = newDate.getDate();
    let horoscope = getSign({ month: month, day: day }) || null;

    switch (horoscope) {
      case "Aries":
        return 67;
      case "Taurus":
        return 68;
      case "Cancer":
        return 69;
      case "Leo":
        return 70;
      case "Virgo":
        return 71;
      case "Gemini":
        return 72;
      case "Libra":
        return 73;
      case "Scorpio":
        return 74;
      case "Sagittarius":
        return 75;
      case "Capricorn":
        return 76;
      case "Aquarius":
        return 77;
      case "Pisces":
        return 78;
      default:
        return 0;
    }
  }

  async AIUsersInfo(otherUserIds: number[], userId: number) {
    try {
      const userWishTags = await this.knex
        .select("tag_id")
        .from("user_wish_tags")
        .where("user_id", userId);
      // console.log({userWishTags})
      const userWishTagsMap = userWishTags.map(
        (userWishTag) => userWishTag["tag_id"]
      );
      // , : otherUserTags, : otherUserGallery, : otherUserNameAndIds[i].username, })
      let otherUserDetail = [];
      for (let i = 0; i < otherUserIds.length; i++) {
        let otherUserTags = await this.knex("user_tags")
          .join("tags", "user_tags.tag_id", "=", "tags.id")
          .select("*")
          .orderBy("created_at", "asc")
          .where("user_id", otherUserIds[i]);
        // console.log({otherUserTags})
        // const otherUserTagsMap = otherUserTags.map(otherUserTag => otherUserTag['tag_id'])
        let userTagsName = otherUserTags.map((tag) => tag.tag);
        let gender = (
          await this.knex("genders")
            .join("users", "genders.id", "=", "users.gender_id")
            .select("genders.gender")
        )[0].gender;

        let [{ birthday: otherUserBdayTimeStamp }] = await this.knex
          .select("birthday")
          .from("users")
          .where("id", otherUserIds[i]);
        const otherUserBday = new Date(otherUserBdayTimeStamp);
        let age = Math.floor(
          (Date.now() - otherUserBday.getTime()) / 31536000000
        );

        let [{ username: otherUserUsername }] = await this.knex
          .select("username")
          .from("users")
          .where("id", otherUserIds[i]);
        let matchedTags = otherUserTags.filter((userTag) => {
          // console.log("userTagMap",userTag['tag_id'])
          // console.log("userWishTagsMap",userWishTags.map(userWishTag => userWishTag['tag_id']))
          return userWishTagsMap.includes(userTag["tag_id"]);
        });
        // console.log({matchedTags})

        // console.log(otherUserIds[i], 'passed tag test')
        const otherUserGallery = await this.knex
          .select("url")
          .from("user_photos")
          .orderBy(["user_id", "id"])
          .where("user_id", otherUserIds[i])
          .whereNot("description", "profile")
          .whereNot("description", "test");

        otherUserDetail.push({
          id: otherUserIds[i],
          gender,
          matchedTags,
          user_tags: userTagsName,
          gallery: otherUserGallery,
          username: otherUserUsername,
          userAge: age,
        });
        // console.log({ otherUserDetail })
        // console.log({userMatch})
      }

      return otherUserDetail;
    } catch (e) {
      throw e;
    }
  }

  async getWhoLikeU(id: number) {
    try {
      // let whoLikeU = await this.knex('relationship')
      //   .innerJoin('user_photos', 'relationship.user_id', '=', 'user_photos.user_id')
      //   .innerJoin('users','users.id','relationship.user_id')
      //   .select('relationship.id', 'relationship.match_coordinates', 'relationship.user_id','users.username', 'user_photos.url')
      //   .where('relationship.friend_id', id)
      //   .where('relationship.status', 'like')
      //   .where('user_photos.description', 'profile')
      let whoLikeU = await this.knex("relationship")
        .select(
          "relationship.id",
          "relationship.match_coordinates",
          "relationship.user_id as userId",
          "users.username"
        )
        .join("users", "users.id", "=", "relationship.user_id")
        .where("relationship.friend_id", id)
        .where("relationship.status", "like");

      for (let user of whoLikeU) {
        user["profile"] = (
          await this.knex
            .select("url")
            .from("user_photos")
            .where("user_id", user.userId)
            .where("description", "profile")
            .first()
        ).url;
        user["oProfile"] = (
          await this.knex
            .select("url")
            .from("user_photos")
            .where("user_id", user.userId)
            .where("description", "original profile")
            .first()
        ).url;
        if (!user.profile || !user.oProfile)
          throw new Error("either profile/original profile missing");
      }

      return whoLikeU;
    } catch (e) {
      throw new Error("get who like u" + e);
    }
  }

  async checkULikeWho(selfID: number, fdID: number[]) {
    try {
      return await this.knex("relationship")
        .select("*")
        .where("user_id", selfID)
        .andWhere("friend_id", fdID)
        .andWhere("status", "like");
    } catch (e) {
      throw e;
    }
  }
}
