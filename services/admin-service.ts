import { Knex } from 'knex';
import { checkPassword, hashPassword } from '../hash';
// import { checkPassword, hashPassword } from '../hash'
// import { JWTPayload } from '../helpers/types';

export class AdminService {

  constructor(private knex: Knex) { }

  async getUserData() {
    try {
      return (await this.knex.select('id', 'username', 'email', 'coins', 'last_fetch_time').from('users').orderBy('id', 'asc'))
    } catch (e) {
      console.log(e)
      return
    }
  }

  async getUserDetail(id: number) {
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          let detail = await txn.select('id', 'phone_num', 'registration_status', 'google_access_token', 'birthday', 'gender_id', 'created_at', 'text_description', 'voice_description', 'isBlocked', 'isFake').from('users').where('id', id)
          let photo = await txn.select('*').from('user_photos').where('user_id', id)
          let transaction = await txn.select('*').from('coin_records').where('user_id', id).orderBy('created_at', 'asc')
          let result = { ...detail[0], user_photos: photo, user_transactions: transaction }
          return result
        } catch (e) {
          console.log(e)
          return
        }
      })
      return result
    } catch (e) {
      console.log(e)
      return
    }
  }

  async getAllAdminData() {
    try {
      return (await this.knex.select('id', 'admin_name', 'email', 'role').from('admins').orderBy('id', 'asc'))
    } catch (e) {
      console.log(e)
      return
    }
  }

  async getAdminDataById(id: number) {
    try {
      console.log('getAdminDataById, id: ', id)
      return (await this.knex.select('*').from('admins').where('id', id).first())
    } catch (e) {
      throw e
    }
  }
  async getReportData() {
    try {
      return (await this.knex
        .select('reports.id', 'user_id', 'username', 'email', 'category', 'target_id', 'content', 'read', 'replied', 'replied_by','reports.created_at')
        .from('reports')
        .join('users', 'user_id', '=', 'users.id')
        .orderBy('id', 'asc').returning('*'))
    } catch (e) {
      console.log(e)
      return
    }
  }

  // async getData(){
  //   try{
      
  //   } catch(e){

  //   }
  // }

  async getDailyUsers() {
    try {
      let initialDate = await this.knex.select('created_at').from('user_locations').orderBy('created_at', 'asc').first()
      let firstDate: any = new Date((new Date(initialDate.created_at)).toDateString())
      let currentDate: any = new Date(new Date().toDateString())
      let dateNum = (currentDate - firstDate)/86400000
      // console.log("date no.: ", dateNum)
      let data: any = await this.knex('user_locations').countDistinct('user_id').where('created_at', '>=', currentDate).first()
      // console.log("get data", data)
      let result = []
      result.push(parseInt(data.count))
      for (let i = dateNum; i > 0; i--){
        let oldData = await this.knex('user_locations').countDistinct('user_id').where('created_at', '>=', (new Date(currentDate - 86400000 * (dateNum - i + 1))))
        let adjustedData: number = parseInt(oldData[0].count as any) - result.reduce((total, num)=>{return (total + num)})
        result.push(adjustedData)
      }
      // console.log("get result", result)
      return result
    } catch (e) {
      console.log(e)
      return
    }
  }

  async getDailyRegistration() {
    try {
      let initialDate = await this.knex.select('created_at').from('users').orderBy('created_at', 'asc').first()
      let firstDate: any = new Date((new Date(initialDate.created_at)).toDateString())
      let currentDate: any = new Date(new Date().toDateString())
      let dateNum = (currentDate - firstDate) / 86400000
      console.log("getDailyRegistration initialDate: ", initialDate)
      console.log("date no.: ", dateNum)
      let data: any = await this.knex('users').count().where('created_at', '>=', currentDate).first()
      // console.log("get data", data)
      let result: number[] = []
      result.push(parseInt(data.count))
      for (let i = dateNum; i > 0; i--) {
        let oldData = await this.knex('users').count().where('created_at', '>=', (new Date(currentDate - 86400000 * (dateNum - i + 1))))
        // console.log("old data", oldData)
        // console.log("result", result)
        let adjustedData: number = parseInt(oldData[0].count as any) - result.reduce((total, num)=>{return (total + num)})
        // console.log("adjusted data", adjustedData)
        result.push(adjustedData)
      }
      console.log("get Daily Registration result", result)
      return result
    } catch (e) {
      console.log(e)
      return
    }
  }

  async dailyMatch() {
    try {
      let initialDate = await this.knex.select('created_at').from('relationship').orderBy('created_at', 'asc').first()
      let firstDate: any = new Date((new Date(initialDate.created_at)).toDateString())
      let currentDate: any = new Date(new Date().toDateString())
      let dateNum = (currentDate - firstDate) / 86400000
      // console.log("date no.: ", dateNum)
      let data: any = await this.knex
        .count('*')
        .from({ra: 'relationship'})
        .join({rb: 'relationship'}, function(){
          this.on('ra.user_id', '=', 'rb.friend_id')
          .on('ra.friend_id', '=', 'rb.user_id')
        })
        .where('ra.created_at', '>=', currentDate)
        .andWhere('ra.status', 'like')
        .andWhere('rb.status', 'like')
        .first()
      // console.log("get data", data)
      let result: number[] = []
      result.push((parseInt(data.count)))
      for (let i = dateNum; i > 0; i--) {
        let oldData = await this.knex
          .count('*')
          .from({ra: 'relationship'})
          .join({rb: 'relationship'}, function(){
            this.on('ra.user_id', '=', 'rb.friend_id')
            .on('ra.friend_id', '=', 'rb.user_id')
          })
          .where('ra.created_at', '>=', (new Date(currentDate - 86400000 * (dateNum - i + 1))))
        // console.log("old data", oldData)
        // console.log("result", result)
        let adjustedData: number = (parseInt(oldData[0].count as any)) - result.reduce((total, num)=>{return (total + num)})
        // console.log("adjusted data", adjustedData)
        result.push(adjustedData)
      }
      // console.log("get result", result)
      return result
    } catch (e) {
      console.log(e)
      return
    }
  }

  async getDailyConsumption() {
    try {
      let initialDate = await this.knex.select('created_at').from('coin_records').where('description', 'rematch').andWhere('created_by', 'system').orderBy('created_at', 'asc').first()
      // console.log("initial date: ", initialDate)
      let firstDate: any = new Date((new Date(initialDate.created_at)).toDateString())
      let currentDate: any = new Date(new Date().toDateString())
      let dateNum = (currentDate - firstDate) / 86400000
      // console.log("current date: ", currentDate)
      // console.log("date no.: ", dateNum)
      let data: any = await this.knex('coin_records').count().where('created_at', '>=', currentDate).where('description', 'rematch').andWhere('created_by', 'system').first()
      // console.log("get data", data)
      let result: number[] = []
      result.push(parseInt(data.count))
      for (let i = dateNum; i > 0; i--) {
        let oldData = await this.knex('coin_records').count().where('created_at', '>=', (new Date(currentDate - 86400000 * (dateNum - i + 1)))).where('description', 'rematch').andWhere('created_by', 'system')
        // console.log("old data", oldData)
        // console.log("result", result)
        let adjustedData: number = parseInt(oldData[0].count as any) - result.reduce((total, num)=>{return (total + num)})
        // console.log("adjusted data", adjustedData)
        result.push(adjustedData)
      }
      // console.log("get result", result)
      return result
    } catch (e) {
      console.log(e)
      return
    }
  }

  async updateReplyStatus(reply: string, id: number, adminID: number, role: string) {
    try {
      await this.knex('reports').where('id', id)
        .update({
          replied: true,
          replied_by: `${role} Admin ID: ${adminID}`
        })
      return {isSuccess: true}
    } catch (e) {

      return {isSuccess: false}
    }
  }

  async restoreReplyStatus(reply: string, id: number, adminID: number) {
    try {
      await this.knex('reports').where('id', id)
        .update({
          replied: false,
          replied_by: null
        })
      return {isSuccess: true} 
    } catch (e) {

      return {isSuccess: false}
    }
  }

  async updateMultiReplyStatus(array: number[], adminID: number, role: string) {
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          for (let i = 0; i < array.length; i++) {
            await txn('reports').where('id', array[i])
              .update({
                replied: true,
                replied_by: `${role} Admin ID: ${adminID}`
              })
          }
          return {isSuccess: true}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async restoreMultiReplyStatus(array: number[]) {
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          for (let i = 0; i < array.length; i++) {
            await txn('reports').where('id', array[i])
              .update({
                replied: false,
                replied_by: 'restored by super admin'
              })
          }
          return {isSuccess: true}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async updateReadStatus(id: number) {
    try {
      await this.knex('reports').where('id', id)
        .update({
          read: true
        })
      return
    } catch (e) {

    }
  }

  async editMultiUsername(userIDs: number[], newName: string) {
    try {
      await this.knex.transaction(async (txn) => {
        try {
          for (let i = 0; i < userIDs.length; i++) {
            await this.knex('users').where('id', userIDs[i])
              .update({
                username: newName
              })
          }
        } catch (e) {

        }
      })
      return
    } catch (e) {

    }
  }

  async updateMultiCoin(userIDs: number[], newCoin: number, description: string, adminID: number) {
    try {
      if (description === ""){
        description = 'adjusted by admin'
      }
      let result = await this.knex.transaction(async (txn) => {
        try {
          for (let i = 0; i < userIDs.length; i++) {
            await txn('users').where('id', userIDs[i])
              .update({
                'coins': txn.raw(`coins + ${newCoin}`)
              })
            await txn('coin_records').insert({
              'transaction': newCoin,
              'user_id': userIDs[i],
              'description': description,
              'created_by': `admin id: ${adminID}`
              })
          }
          return {isSuccess: true}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async deleteMultiUser(userIDs: number[]) {
    try {
      const result = await this.knex.transaction(async (txn) => {
        try {
          for (let i = 0; i < userIDs.length; i++) {
            await txn('users').where('id', userIDs[i]).del()
          }
          return {isSuccess: true}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async deleteMultiAdmin(adminIDs: number[]) {
    try {
      const result = await this.knex.transaction(async (txn) => {
        try {
          for (let i = 0; i < adminIDs.length; i++) {
            let role = await txn('admins').where('id', adminIDs[i]).select('role')
            if (role[0].role !== "Super") {
              await txn('admins').where('id', adminIDs[i]).del()
            }
          }
          return {isSuccess: true}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async adminLogin(email: string, password: string) {
    try {
      console.log('email: ', email)
      let admin = await this.knex.select('*').from('admins').where('email', email).first();
      if (admin.length == 0) {
        return {
          isSuccess: false,
          msg: "email does not exist"
        }
      }
      let passwordCorrect = await checkPassword(password, admin.password)
      if (passwordCorrect) {
        return {
          isSuccess: true,
          data: {
            id: admin.id,
            adminName: admin.admin_name,
            email: admin.email,
            role: admin.role,
            theme: admin.theme,
          }
        }
      } else {
        return {
          isSuccess: false,
          msg: "wrong password"
        }
      }
    } catch (e) {
      throw e
    }
  }

  async updatePassword(adminID: number, newPassword: string, oldPassword: string){
    try {
      const result = await this.knex.transaction(async (txn) => {
        try{
          // console.log("oldPassword: ", oldPassword)
          let adminCheck = await txn('admins').where('id', adminID).select('password').first()
          // console.log("adminCheck: ", adminCheck)
          let passwordCorrect = await checkPassword(oldPassword, adminCheck.password)
          if (passwordCorrect === false){
            return {isSuccess: false}
          }
          // console.log("newPassword: ", newPassword)

          let passwordHash = await hashPassword(newPassword)
          // console.log("passwordHash: ", passwordHash)
          let admin = await txn('admins').where('id', adminID).update({
            password: passwordHash,
          }).returning('id')
          if (admin[0] === adminID){
            return {isSuccess: true}
          } else {
            return {isSuccess: false}
          }
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
    return result
    } catch (e) {
      console.error(e)
      return { isSuccess: false }
    }
  }

  async updateTheme(theme: string, adminID: number){
    try {
      const result = await this.knex('admins').where('id', adminID).update({
        theme: theme
      }).returning('*')
      if (result[0].id === adminID){
        return {isSuccess: true, data: result[0]}
      }
      return {isSuccess: false}
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async getCurrentAdmin(adminID: number){
    try {
      const admin = await this.knex('admins').where('id', adminID).select('*').first()
      return {
        isSuccess: true,
        data: {
          id: admin.id,
          adminName: admin.admin_name,
          email: admin.email,
          role: admin.role,
          theme: admin.theme,
        }
      }
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async checkAdmin(email: string){
    try {
      const check = await this.knex('admins').where('email', email).select('id').first()
      if (check === undefined){
        console.log("return true")
        return {isSuccess: true}
      } 
      return {isSuccess: false}
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async addAdmin(email: string, adminName: string, password: string, role: "Super" | "Ordinary" | "Intern"){
    try {
      let passwordHash = await hashPassword(password)
      await this.knex('admins').insert({
        admin_name: adminName,
        email: email,
        password: passwordHash,
        role: role,
        theme: "lightBlue",
        created_at: "now()",
        updated_at: "now()",
      })
      return {isSuccess: true}
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async checkSuper(id: number){
    try {
      let result = await this.knex('admins').where('id', id).select('role').first()
      console.log('check role: ', result)
      if (result.role === "Super"){
        return {isSuccess: true}
      }
      return {isSuccess: false}
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async checkNotIntern(id: number){
    try {
      let result = await this.knex('admins').where('id', id).select('role').first()
      console.log('check role: ', result)
      if (result.role === "Super" || result.role === "Ordinary"){
        return {isSuccess: true}
      }
      return {isSuccess: false}
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async updateAdminName(adminName: string, id: number){
    await this.knex('admins').where('id', id).update({
      admin_name: adminName
    })
  }
  async updateAdminEmail(email: string, id: number){
    await this.knex('admins').where('id', id).update({
      email: email
    })
  }
  async updateAdminRole(role: string, id: number){
    await this.knex('admins').where('id', id).update({
      role: role
    })
  }

  async updateAdmin(adminName: string, email: string, role: string, id: number){
    try {
      console.log("adminName: ", adminName)
      console.log("email: ", email)
      console.log("role: ", role)
      const result = await this.knex.transaction(async (txn) => {
        try{
          if (adminName !== undefined){
            await txn('admins').where('id', id).update({
              admin_name: adminName
            })
          }
          if (email !== undefined){
            await txn('admins').where('id', id).update({
              email: email
            })
          }
          if (role !== undefined){
            await txn('admins').where('id', id).update({
              role: role
            })
          }
          return {isSuccess: true}
        }catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async updateMultiAdmin (content: {id: number, email: string | null, adminName: string | null, role: "Super" | "Ordinary" | "Intern" | null}[]){
    try {
      const result = await this.knex.transaction(async (txn) => {
        try{
          for (let i = 0; i < content.length; i++){
            if (content[i].adminName !== null){
              await txn('admins').where('id', content[i].id).update({
                admin_name: content[i].adminName
              })
            }
            if (content[i].email !== null){
              await txn('admins').where('id', content[i].id).update({
                email: content[i].email
              })
            }
            if (content[i].role !== null){
              await txn('admins').where('id', content[i].id).update({
                role: content[i].role
              })
            }
          }
          return {isSuccess: true}
        }catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async blockUser (id: number){
    try {
      await this.knex('users').where('id', id).update({
        isBlocked: true
      })
      return {isSuccess: true}
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async unblockUser (id: number){
    try {
      await this.knex('users').where('id', id).update({
        isBlocked: false
      })
      return {isSuccess: true}
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async removeProfilePic(id: number, url: string){
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          await txn('user_photos').where('user_id', id).andWhere(url, url).andWhere('description', 'profile').del()
          await txn('user_photos').where('user_id', id).andWhere('description', 'original profile').del()
          await txn('users').where('id', id).update({
            registration_status: 2
          })
          return {isSuccess: true}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async editUserDetail(content: {
    phone_num: string | null, 
    registration_status: number | null, 
    birthday: string | null, 
    gender_id: number | null,
    text_description: string | null,
    voice_description: string | null
  }, userID: number){
    try {
      const result = await this.knex.transaction(async (txn) => {
        try{
          if (content.phone_num !== null){
            await txn('users').where('id', userID).update({
              phone_num: content.phone_num
            })
          }
          if (content.registration_status !== null){
            await txn('users').where('id', userID).update({
              registration_status: content.registration_status
            })
          }
          if (content.birthday !== null){
            await txn('users').where('id', userID).update({
              birthday: (new Date(content.birthday)).toISOString()
            })
          }
          if (content.gender_id !== null){
            await txn('users').where('id', userID).update({
              gender_id: content.gender_id
            })
          }
          if (content.text_description !== null){
            await txn('users').where('id', userID).update({
              text_description: content.text_description
            })
          }
          if (content.text_description !== null){
            await txn('users').where('id', userID).update({
              voice_description: null
            })
          }
          return {isSuccess: true}
        }catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async deletePhoto(url: string, userID: number){
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          await txn('user_photos').where('user_id', userID).andWhere('url', url).del()
          return {isSuccess: true}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async getUserTagAnalystData(){
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          let select = await txn('classes').select('class')
          let result = []
          for (let i = 0; i < select.length; i++){
            let num = await txn('user_tags')
            .join('tags', 'tag_id', 'tags.id')
            .join('classes', 'class_id', 'classes.id')
            .count('*')
            .where('classes.class', select[i].class)
            .first()
            result.push({
              name: select[i].class,
              value: num?.count
            })
          }
          return {isSuccess: true, data: result}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async getUserWishTagAnalystData(){
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          let select = await txn('classes').select('class')
          let result = []
          for (let i = 0; i < select.length; i++){
            let num = await txn('user_wish_tags')
            .join('tags', 'tag_id', 'tags.id')
            .join('classes', 'class_id', 'classes.id')
            .count('*')
            .where('classes.class', select[i].class)
            .first()
            result.push({
              name: select[i].class,
              value: num?.count
            })
          }
          return {isSuccess: true, data: result}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async getSubData(type: string){
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          let select = await txn('tags').select('tag').join('classes', 'class_id', 'classes.id').where('class', type)
          let result = []
          for (let i = 0; i < select.length; i++){
            let num = await txn('user_tags')
            .join('tags', 'tag_id', 'tags.id')
            .join('classes', 'class_id', 'classes.id')
            .count('*')
            .where('tags.tag', select[i].tag)
            .first()
            result.push({
              name: select[i].tag,
              value: num?.count
            })
          }
          return {isSuccess: true, data: result}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async getWishTagSubData(type: string){
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          let select = await txn('tags').select('tag').join('classes', 'class_id', 'classes.id').where('class', type)
          let result = []
          for (let i = 0; i < select.length; i++){
            let num = await txn('user_wish_tags')
            .join('tags', 'tag_id', 'tags.id')
            .join('classes', 'class_id', 'classes.id')
            .count('*')
            .where('tags.tag', select[i].tag)
            .first()
            result.push({
              name: select[i].tag,
              value: num?.count
            })
          }
          return {isSuccess: true, data: result}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async getGenderSubData(type: string){
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          let select = await txn.raw(`select distinct extract(year from birthday) from users join genders on gender_id = genders.id where genders.gender = '${type}'`)
          let result = []
          for (let i = 0; i < select.rows.length; i++){
            let num = await txn.raw(`select count('*') from users join genders on gender_id = genders.id where genders.gender = '${type}' and extract (year from birthday) = ${select.rows[i].date_part}`)
            result.push({
              name: select.rows[i].date_part,
              value: num.rows[0]?.count
            })
          }
          return {isSuccess: true, data: result}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async getUserGenderAnalystData(){
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          let select = await txn('genders').select('gender')
          let result = []
          for (let i = 0; i < select.length; i++){
            let num = await txn('users')
            .join('genders', 'gender_id', 'genders.id')
            .count('*')
            .where('genders.gender', select[i].gender)
            .first()
            result.push({
              name: select[i].gender,
              value: num?.count
            })
          }
          return {isSuccess: true, data: result}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async getAgeSubData(type: string){
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          let select = await txn('genders').select('gender')
          let result = []
          for (let i = 0; i < select.length; i++){
            let num
            if (type !== "60+"){
              num = await txn.raw(`select count('*') from users join genders on gender_id = genders.id where extract (year from birthday) >= '${(new Date()).getFullYear() - parseInt(type.slice(-2))}' and extract (year from birthday) <= '${(new Date()).getFullYear() - parseInt(type.slice(0, 2))}' and genders.gender = '${select[i].gender}'`)
            } else {
              num = await txn.raw(`select count('*') from users join genders on gender_id = genders.id where extract (year from birthday) <= '${(new Date()).getFullYear() - 60}' and genders.gender = '${select[i].gender}'`)
            }
            result.push({
              name: select[i].gender,
              value: num.rows[0]?.count
            })
          }
          return {isSuccess: true, data: result}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }

  async getUserAgeAnalystData(){
    try {
      let result = await this.knex.transaction(async (txn) => {
        try {
          let select = await txn.raw(`select distinct extract(year from birthday) from users join genders on gender_id = genders.id`)
          let result = []
          for (let i = 0; i < select.rows.length; i++){
            let num = await txn.raw(`select count('*') from users join genders on gender_id = genders.id and extract (year from birthday) = ${select.rows[i].date_part}`)
            result.push({
              name: select.rows[i].date_part,
              value: num.rows[0]?.count
            })
          }
          return {isSuccess: true, data: result}
        } catch (e) {
          console.error(e)
          return {isSuccess: false}
        }
      })
      return result
    } catch (e) {
      console.error(e)
      return {isSuccess: false}
    }
  }
}