import { MatchingController } from './controllers/matching-controller'
import express from 'express'
import { Multer } from 'multer'
import { UserController } from './controllers/user-controller'
import { FriendController } from './controllers/friend-controller'
import { ProfileController } from './controllers/profile-controller'
import { AdminController } from './controllers/admin-controller'
import { isAdmin } from './guards'
import { ReportController } from './controllers/report-controller'

export function createRouter(options: {
  matchingController: MatchingController
  // upload: Multer
  uploadS3: Multer
  isLoggedIn: express.RequestHandler
  isAdmin:express.RequestHandler
  userController: UserController
  friendController: FriendController
  profileController: ProfileController
  adminController: AdminController
  reportController: ReportController
}) {
  const { uploadS3, isLoggedIn, matchingController, userController, friendController, profileController, adminController, reportController } = options
  // const { uploadS3, upload, isLoggedIn, matchingController, userController, friendController, profileController } = options

  let router = express.Router()

  // matching routes
  // router.get('/google-map', (req, res) => res.redirect("/main/google-map.html"))
  router.post('/new-location', isLoggedIn, matchingController.insertGeolocation)
  router.get('/buddies', isLoggedIn, matchingController.findBuddies)//TODO
  router.get('/check-last-match-time', isLoggedIn, matchingController.checkAfter15Mins)//TODO
  router.post('/like-match', isLoggedIn, matchingController.likeOrUnlikeOtherUser)
  router.post('/pay-to-rematch', isLoggedIn, matchingController.payToRematch)
  router.get('/whoLikeU', isLoggedIn, matchingController.getWhoLikeU)

  // user routes
  router.post('/signUp', userController.registration)
  router.post('/login', userController.login)
  // router.get('/login', userController.loginRedirect)
  // router.post('/logout', userController.logout)
  router.get('/login/google', userController.loginGoogle) //TODO
  // router.get('/main', (req, res) => res.redirect("/main/main.html"))
  router.get('/check-user-reg-status', isLoggedIn, userController.checkUserRegStatus)
  router.get('/update-token', isLoggedIn, userController.updateToken)
  // router.get('/language', userController.getLanguage)
  // router.post('/language', userController.updateLanguage)
  // router.post('/coins', isLoggedIn, userController.useCoins)
  router.get('/updateFetchTime', isLoggedIn, userController.updateFetchTime)

  // friend routes
  // router.get('/fd-last-message', isLoggedIn, friendController.getFriendsLastMessage) //blocked
  router.get('/totalUnread', isLoggedIn, friendController.  getTotalUnreadMessageCount)
  router.post('/chatroom', isLoggedIn, friendController.getChatroomMessageInsideRoom) //blocked
  router.post('/sendMessage', isLoggedIn, friendController.sendMessage) //blocked
  router.get('/fd-list', isLoggedIn, friendController.getFriendList) //blocked
  router.post('/readMessage', isLoggedIn, friendController.setMsgToRead) //blocked
  // router.get('/deleteChatRecord', isLoggedIn, friendController.deleteChatRecord) //blocked
  // router.get('/block', isLoggedIn, friendController.blockFd) //blocked
  // router.get('/unblock', isLoggedIn, friendController.unblockFd) //blocked
  router.get('/relations', isLoggedIn, friendController.getFriendNameAndPhotoAndLastMessage)

  // profile routes
  router.get('/getProfile', isLoggedIn, profileController.getProfile)
  // router.post('/updateProfilePic', isLoggedIn, uploadS3.single('originalProfile'), profileController.updateProfilePic) //TODO
  router.post('/uploadProfilePic', isLoggedIn, uploadS3.fields([{name:'profile'},{name:'originalProfile'}]), profileController.uploadProfilePic) //TODO
  router.get('/getUserTags', isLoggedIn, profileController.getOwnUserTags)
  router.get('/getWishTags', isLoggedIn, profileController.getWishTags)
  router.post('/getMatchUserTags', isLoggedIn, profileController.getMatchUserTags)
  router.get('/getClassAndTags', profileController.getClassAndTags)
  router.post('/updateProfile', isLoggedIn, profileController.updateProfile)
  router.post('/uploadVoice',isLoggedIn,uploadS3.single('voice'),profileController.uploadVoice)
  router.post('/addUserTag', isLoggedIn, profileController.addUserTag)
  router.post('/addWishTag', isLoggedIn, profileController.addWishTag)
  router.delete('/deleteUserTag', isLoggedIn, profileController.deleteUserTag)
  router.delete('/deleteWishTag', isLoggedIn, profileController.deleteWishTag)
  router.post('/questionnaire', isLoggedIn,profileController.newRegistration) 
  router.patch('/editWishTag', isLoggedIn, profileController.editUserWishTag)
  router.patch('/editUserTag', isLoggedIn, profileController.editUserTag)
    // router.post('/addProfilePic', isLoggedIn, uploadS3.single('profile'),uploadS3.single('originalProfile'), profileController.updateProfilePic)
  router.get('/getGallery', isLoggedIn, profileController.getGallery)
  router.post('/addPhotos', isLoggedIn, uploadS3.array('photo'), profileController.addPhotos)
  router.delete('/delPhotos', isLoggedIn, profileController.delPhotos)
  router.get('/stageThree', profileController.updateStageThree)//TODO
  router.get('/getUserProfile', isLoggedIn, profileController.loadUserProfile)
  router.get('/getUserProfilePic', isLoggedIn, profileController.loadUserProfilePic)
  
  // dashboard routes
  router.get('/getUserData', isAdmin, adminController.getUserData)
  router.post('/getUserDetail', isAdmin,adminController.getUserDetail)
  router.get('/getAdminData', isAdmin,adminController.getAdminData)
  router.get('/getReportData', isAdmin,adminController.getReportData)
  router.get('/getData', isAdmin,adminController.getData)
  router.post('/updateReplyStatus', isAdmin,adminController.updateReplyStatus)
  router.post('/updateMultiReplyStatus', isAdmin,adminController.updateMultiReplyStatus)
  router.post('/restoreMultiReplyStatus', isAdmin,adminController.restoreMultiReplyStatus)
  router.post('/updateReadStatus', isAdmin,adminController.updateReadStatus)
  router.post('/editMultiUsername', isAdmin,adminController.editMultiUsername)
  router.post('/updateMultiCoin', isAdmin,adminController.updateMultiCoin)
  router.post('/deleteMultiUser', isAdmin,adminController.deleteMultiUser)
  router.post('/deleteMultiAdmin', isAdmin,adminController.deleteMultiAdmin)
  router.post('/adminLogin', adminController.adminLogin)
  router.post('/updatePassword', isAdmin, adminController.updatePassword)
  router.post('/updateTheme', isAdmin, adminController.updateTheme)
  router.get('/getCurrentAdmin', isAdmin, adminController.getCurrentAdmin)
  router.post('/addAdmin', isAdmin, adminController.addAdmin)
  router.post('/updateAdmin', isAdmin, adminController.updateAdmin)
  router.post('/updateMultiAdmin', isAdmin, adminController.updateMultiAdmin)
  router.post('/blockUser', isAdmin, adminController.blockUser)
  router.post('/unblockUser', isAdmin, adminController.unblockUser)
  router.post('/removeProfilePic', isAdmin, adminController.removeProfilePic)
  router.post('/editUserDetail', isAdmin, adminController.editUserDetail)
  router.post('/deletePhoto', isAdmin, adminController.deletePhoto)
  router.get('/getUserTagAnalystData', isAdmin, adminController.getUserTagAnalystData)
  router.get('/getUserWishTagAnalystData', isAdmin, adminController.getUserWishTagAnalystData)
  router.post('/getSubData', isAdmin, adminController.getSubData)
  router.get('/getUserGenderAnalystData', isAdmin, adminController.getUserGenderAnalystData)
  router.get('/getUserAgeAnalystData', isAdmin, adminController.getUserAgeAnalystData)

  //for test  
  router.post('/suggestion', isLoggedIn, friendController.getSuggestedType)
  
  // report routes
  // router.post('/submitReport', isLoggedIn, reportController.submitReport)
  router.post('/submitReport', reportController.submitReport)
  
  return router
}
