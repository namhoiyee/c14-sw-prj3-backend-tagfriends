
import AWS from "aws-sdk";
import dotenv from 'dotenv'
dotenv.config();

export async function deleteFromS3(url:string){
    console.log('deleting file', url)
    var s3 = new AWS.S3()
    var params = {
        Bucket: process.env.S3_BUCKET!,
        Key: url
    };
    try{
        await s3.headObject(params).promise()
        console.log("File Found in S3")
        try {
            await s3.deleteObject(params).promise()
            console.log("File deleted from S3")
        } catch (e) {
            console.log(e)
        }
    }catch(e){
        console.log('file not in S3')
    }
    
}

