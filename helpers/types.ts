  export type ResponseData<T> =
    | {
      isSuccess: true
      data: T
    }
    | {
      isSuccess: false
      msg: string
    }
//   export type User = {
//     username?:string
//     id:number
//   }
  
//   export type UserServer = {
//     username:string
//     id:number
//     password:string
//   }
  
export type JWTPayload = {
    id:number
    username:string
    lang:'en'|'zh'
    profilePic?:string
    registration_status:number
} 

export type JWTAdminPayload = {
    id: number,
    // adminName: string,
    // email: string,
    // role: "Super" | "Ordinary" | "Intern",
    // theme: "lightBlue" | "crimson" | "light" | "dark" | "darkMode"
}

declare global{
    namespace Express{
        interface Request{
            user?: User
        }
    }
}

export interface User{
    id:number
    username:string
}

export type ResponseJson<T> = {
    isSuccess:true,
    data:T
}|{
    isSuccess:false,
    msg:string
}

export type TagClass = { id: number; class: string };
export type Tag = { id: number; tag: string; classId: number; class: string; dbPrimaryId: number; };