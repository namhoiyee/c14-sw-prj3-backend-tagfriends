import { MatchingService } from "../services/matching-service";
import { ProfileService } from "../services/profile-service";
import express from "express";
import fetch from "node-fetch";
import { io, socketMap } from "../socketio";

type FindBuddyResult = {
  "user_locations.user_id": number;
  "users.username": string;
};
export class MatchingController {
  constructor(
    private matchingService: MatchingService,
    private profileService: ProfileService
  ) {}

  insertGeolocation = async (req: express.Request, res: express.Response) => {
    const latitude = req.body?.latitude;
    const longitude = req.body?.longitude;
    const userId = req.user!.id;
    if (!latitude || !longitude) {
      res.status(400).json({
        isSuccess: false,
        msg: "require lat and long",
      });
      return;
    }
    try {
      const result = await this.matchingService.insertGeolocation({
        latitude,
        longitude,
        userId,
      });
      // req.session['userGrid'] = result
      // console.log({gridResultsession: req.session['userGrid']})
      // req.session['userInsertLocationLastTimeMatch'] = new Date(result['matched_at']).getTime()
      // console.log({afterInsertSession: req.session['userInsertLocationLastTimeMatch']})
      if (result["grid"] && result["location"]) {
        // console.log('grid and location',result);
        res.status(201).json({
          isSuccess: true,
          data: result,
        });
      } else {
        res.status(500).json({
          isSuccess: false,
          msg: result,
        });
      }
    } catch (error) {
      // console.log('failed to insert memo:', error)
      res.status(500).json({
        isSuccess: false,
        msg: "cannot insert location " + error,
      });
    }
  };

  callAI = (
    result: Object[],
    userEmail: string,
    userId: number
  ): Promise<{ match: [] }> => {
    let reqJson = { result: result, email: userEmail };
    let port = (userId % 5) + 8000;
    port = 8001;
    return new Promise((resolve, reject) => {
      fetch(`http://localhost:${port}/matching`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(reqJson),
      })
        .then((res) => res.json())
        .then((result) => {
          resolve(result);
        })
        .catch((e) => reject(e));
    });
  };
  //test only
  // callAIOnly = async (req: express.Request, res: express.Response) => {
  //   const userId = req.user!.id
  //   const userEmail = await this.profileService.getEmailById(userId)
  //   const userLastLocation = await this.matchingService.getUserLastGrid(userId)
  //   const buddies = await this.matchingService.findBuddies(userLastLocation, userId)
  //   const buddiesId = buddies.map(buddy => buddy.user_id)
  //   let result = await this.matchingService.getBuddiesTags(buddiesId)

  //   let reqJson = { result: result, email: userEmail }

  //   let port = userId % 5 + 8000
  //   port = 8002
  //   try{
  //   let prediction = await fetch(`http://localhost:${port}/matching`, {
  //     method: "POST",
  //     headers: {
  //       'Content-Type': 'application/json'
  //     },
  //     body: JSON.stringify(reqJson)

  //   })

  //   let result1 = await prediction.json()
  //   res.json(result1)
  // }catch(e){
  //   throw new Error (e)
  // }
  // return new Promise((resolve, reject) => {
  //   let res = fetch(`http://localhost:${port}/matching`, {
  //     method: "POST",
  //     headers: {
  //       'Content-Type': 'application/json'
  //     },
  //     body: JSON.stringify(reqJson)

  //   }).then((result) => {
  //     let prediction = res.json()
  //     console.log("prediction", prediction)
  //     resolve(result);
  //   }).catch(e => {
  //     reject(e)
  //   })
  // })
  // }

  callAIOrTimeOut = async (
    result: Object[],
    userEmail: string,
    userId: number
  ): Promise<{ match: Array<any> }> => {
    return new Promise((resolve, reject) => {
      let timeout = setTimeout(() => {
        console.log("timer");
        resolve({ match: [] });
      }, 10000);
      this.callAI(result, userEmail, userId)
        .then((result) => {
          clearTimeout(timeout);
          resolve(result);
        })
        .catch((e) => {
          clearTimeout(timeout);
          reject({ match: [] });
          console.log(e);
        });
    });
  };

  findBuddies = async (req: express.Request, res: express.Response) => {
    try {
      const userId = req.user!.id;
      const userLastLocation = await this.matchingService.getUserLastGridWithLocation(
        userId
      );
      if (!userLastLocation) {
        res.status(500).json({
          isSuccess: false,
          msg: "no known location",
        });
        return
      }
      let buddies: FindBuddyResult[] = await this.matchingService.findBuddies(
        userLastLocation[0]['grid'],
        userId
      );
      // if (buddies.length < 10) {
      //   const remainingBuddyNumber = 10 - buddies.length;
      //   const randomMatches: FindBuddyResult[] =
      //     await this.matchingService.randomGenUserIdsForMatch(
      //       remainingBuddyNumber
      //     );
      //   randomMatches.forEach((randomMatch) => buddies.push(randomMatch));
      // }
      const finalResult = await this.prioritizeResult(userId, buddies);
      const finalResultWithLocation = { ...finalResult, userLocation: userLastLocation[0]['location']}
      // console.log("matching result :", finalResult);
      let trained = this.getLikeList(userId);
      res.status(200).json({
        isSuccess: true,
        data: { finalResultWithLocation, trained },
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        isSucces: false,
        msg: "failed to find buddies" + error,
      });
    }
  };

  likeOrUnlikeOtherUser = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      const userId = req.user!.id;
      const likedUserId = req.body?.likedUserId;
      const likeRes = req.body?.likeRes;

      if (likeRes === "like") {
        const insertRelationshipResult =
          await this.matchingService.likeOtherUser(userId, likedUserId);
        // console.log("insertRelationshipResult",insertRelationshipResult)
        const updateMatchedResultForUI =
          await this.matchingService.updateMatchedResultForUI(
            userId,
            likedUserId
          );
        let room = insertRelationshipResult.likedRelationship.room_id;
        // const crushCheckResult = await this.matchingService.checkCrushHappen(
        //   userId,
        //   likedUserId,
        // );
        if (socketMap[userId] != null) {
          for (const socket of socketMap[userId]) {
            socket.join("room:" + room);
          }
        }

        if (insertRelationshipResult.crush) {
          res.status(201).json({
            isSuccess: true,
            data: {
              userId,
              likedUserId,
              room,
              updateMatchedResultForUI,
            },
          });
          let userInfo = this.profileService.getProfile(userId);
          let friendInfo = this.profileService.getProfile(likedUserId);
          io.to("room:" + room.room_id).emit("match", {
            user: userInfo,
            friend: friendInfo,
          });
        }
        res.status(201).json({
          isSuccess: true,
          data: { userId, likedUserId, room, updateMatchedResultForUI },
        });
      } else {
        await this.matchingService.unlikeOtherUser(userId, likedUserId);

        const updateMatchedResultForUI =
          await this.matchingService.updateMatchedResultForUI(
            userId,
            likedUserId
          );

        res.status(201).json({
          isSuccess: true,
          data: { userId, likedUserId, updateMatchedResultForUI },
        });
      }
    } catch (e) {
      console.log(e);
      res.status(500).json({
        isSuccess: false,
        msg: "failed to like/dislike" + e,
      });
    }
  };

  payToRematch = async (req: express.Request, res: express.Response) => {
    try {
      // const coinsUsed = req.body?.coins

      const userId = req.user!.id;

      const matchedResult = await this.matchingService.payToRematch(
        userId,
        this.prioritizeResult
      );
      // console.log("doRematchNow",matchedResult)
      res.status(200).json({
        isSuccess: true,
        data: matchedResult,
      });
    } catch (e) {
      console.log("Error: rematch : " + e);
      res.status(401).json({
        isSuccess: false,
        msg: "unexpected error" + e,
      });
    }
  };

  prioritizeResult = async (userId: number, buddies: FindBuddyResult[]) => {
    try {
      let removeSwipedPeople = await this.matchingService.removeSwipedPeople(
        buddies,
        userId
      );
      const buddiesRemained = removeSwipedPeople.buddiesRemained;

      let buddiesRemainedId = buddiesRemained.map((item) => item.user_id);
      if (buddiesRemainedId.length > 40)
        buddiesRemainedId = buddiesRemainedId.slice(0, 40);
      let aiPromise = this.aiResult(userId, buddiesRemainedId);
      let removedList = removeSwipedPeople.removedList;
      if (buddiesRemained.length < 10) {
        const remainingBuddyNumber = 10 - buddiesRemained.length;
        const randomMatches =
          await this.matchingService.randomGenUserIdsForMatch(
            remainingBuddyNumber,
            removedList
          );
        randomMatches.forEach((randomMatch) =>
          buddiesRemained.push(randomMatch)
        );
      }

      const algorithmMatchResult =
        await this.matchingService.matchUserWishTagsWithOtherUserTags(
          userId,
          buddiesRemained
        );
      // let buddiesRemained2 = buddiesRemained.map(item => item.user_id)
      let algoMatchingMap = new Map();
      algorithmMatchResult.otherUserDetail.map((item) => {
        algoMatchingMap.set(item.id, item);
      });
      // let usersFromAlgo = algorithmMatchResult.otherUserDetail.map(item => item.id)
      let aiResult = await aiPromise;
      console.log('aiResult', aiResult);
      aiResult.forEach((item) => {
        algoMatchingMap.delete(item.id);
      });
      let algoResult = Array.from(algoMatchingMap.values());

      // usersFromAlgo.forEach(id => {
      //   let index = aiResult.indexOf(id)
      //   if (index > -1) buddiesRemained2.slice(index, 1)
      // })
      let final = [];
      let a = 0,
        b = 0;
      for (let i = 0; i < 10; i++) {
        if (i % 2) {
          final.push(algoResult[b]);
          b++;
        } else {
          if (aiResult.length > a) {
            final.push(aiResult[a]);
            a++;
          } else {
            final.push(algoResult[b]);
            b++;
          }
        }
      }

      return {
        matchedTime: algorithmMatchResult.matchedTime,
        otherUserDetail: final,
      };
    } catch (e) {
      throw e;
    }
  };

  aiResult = async (userId: number, buddiesRemained2: number[]) => {
    try {
      // console.log("buddies:", buddies)
      let userEmail = await this.profileService.getEmailById(userId);
      let result = await this.matchingService.getBuddiesTags(buddiesRemained2);
      let aiResult = await this.callAIOrTimeOut(result, userEmail, userId);
      let idsbyAi: number[] = [];
      if (aiResult.match.length) {
        idsbyAi = aiResult.match.map((item) => item.id as number);
        let usersFromAi;
        usersFromAi = await this.matchingService.AIUsersInfo(idsbyAi, userId);
        return usersFromAi;
      } else {
        return [];
      }
    } catch (e) {
      throw e;
    }
  };

  // let buddyIds = buddiesAfterFilterTags.buddiesAfterFilterTags.map(buddy => {
  //   return parseInt(Object.keys(buddy)[0])
  // })

  // let userMatchGallery = await this.getMatchUserGallery(buddyIds)
  // req.session['userInsertLocationLastTimeMatch'] = new Date(buddyDetailsAfterTagFilter['matchedTime']).getTime()
  // console.log({ afterInsertSession: new Date(buddyDetailsAfterTagFilter['matchedTime']).getTime() })

  getLikeList = async (id: number) => {
    // let id = req.user!.id
    let result = await this.matchingService.getLikeList(id);
    console.log("result: ", result)
    let email = await this.profileService.getEmailById(id);
    let likeList = { result: result, userEmail: email };
    // let port = id % 5 + 8000
    let trainedRelationship = result.map((res) => res.friendId);

    if (result.length) {
      try {
        console.log("work on model...");
        let resp = await fetch(`http://localhost:8005/train`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(likeList),
        });
        let text = await resp.json();
        if (text.accuracy) {
          console.log("id: ", id, "accuracy: ", text.accuracy);
          this.matchingService.setFriendsTrained(trainedRelationship, id);
          return { trained: true };
        } else {
          console.log("error: ", text["error"]);
          return { trained: false, error: text["error"] };
        }

        // let resp2 = await fetch(`http://localhost:${port}/loadModel`, {
        //     method: "POST",
        //     headers: {
        //         'Content-Type': 'application/json'
        //     },
        //     body: JSON.stringify(email)
        // })
        // console.log(await resp2.text())

        // if (await resp.json()){
        //     console.log("trained")
        // }
      } catch (e) {
        console.log(e);
        return { trained: false, error: e };
      }
    } else return { trained: false };
  };

  checkAfter15Mins = async (req: express.Request, res: express.Response) => {
    // console.log({check15Minsession: req.session['userGrid']})
    try {
      const userId = req.user!.id;
      // console.log({check15Minsession: req.session['userGrid']})
      // console.log({ checkAfter15MinUserId: userId })
      const newInsertRes = await this.matchingService.checkAfter15Mins(userId);
      // console.log({ newInsertRes: newInsertRes })
      const lastMatchTime =
        new Date(newInsertRes[0]?.["matched_at"]).getTime() ||
        new Date("1995-12-17T03:24:00").getTime();
      let timeNow = Date.now();
      let timePassed = timeNow - lastMatchTime;
      // console.log({ minPassed: timePassed / 1000 / 60 })
      // req.session.save()

      // console.log("login check time", req.session['userInsertLocationLastTimeMatch'])
      // console.log((timeNow - req.session['userInsertLocationLastTimeMatch']) / 1000 / 60)
      let matchTimePeriod = 1;
      if (timePassed / 1000 / 60 > matchTimePeriod) {
        res.status(201).json({
          isSuccess: true,
          data: { timePassed, rematch: "need to do rematch" },
        });
        return;
      }

      let userLastTimeMatchResult = await this.showUpdatedResult(userId);

      let needToGetGeolocationAgain = true;
      if (timePassed < 180000) {
        needToGetGeolocationAgain = false;
      }
      res.status(200).json({
        isSuccess: true,
        data: {
          timePassed: timePassed,
          userLastTimeMatchResult,
          needToGetGeolocationAgain,
        },
      });
    } catch (e) {
      console.log(e);
      res.status(500).json({
        isSuccess: false,
        msg: "failed to check time" + e,
      });
    }
  };

  showUpdatedResult = async (userId: number) => {
    let userLastTimeMatchResult;
    let userLastTimeMatchResultFull =
      await this.matchingService.getUserLastTimeMatchResult(userId);

    // let buddyDetailsAfterTagFilter;
    // if (!userLastTimeMatchResultFull) {
    //   const userLastLocation = await this.matchingService.getUserLastGrid(
    //     userId
    //   );
    //   if (userLastLocation) {
    //     let buddies = await this.matchingService.findBuddies(
    //       userLastLocation,
    //       userId
    //     );
    //     if (buddies.length < 10) {
    //       const remainingBuddyNumber = 10 - buddies.length;
    //       const randomMatches: number[] =
    //         await this.matchingService.randomGenUserIdsForMatch(
    //           remainingBuddyNumber
    //         );
    //       randomMatches.forEach((randomMatch) => buddies.push(randomMatch));
    //     }
    //   } else {
    //     let randomMatch = await this.matchingService.randomGenUserIdsForMatch(
    //       10
    //     );
    //     buddyDetailsAfterTagFilter =
    //       await this.matchingService.matchUserWishTagsWithOtherUserTags(
    //         userId,
    //         randomMatch
    //       );
    //   }
    // }

    if (userLastTimeMatchResultFull.updated_result) {
      userLastTimeMatchResult = userLastTimeMatchResultFull.updated_result;
    } else {
      userLastTimeMatchResult = userLastTimeMatchResultFull.result;
    }
    return userLastTimeMatchResult;
  };

  getWhoLikeU = async (req: express.Request, res: express.Response) => {
    try {
      console.log("here");
      let results = await this.matchingService.getWhoLikeU(req.user!["id"]);
      console.log("here2");
      let finalResults = [];
      for (let result of results) {
        let finalResult = await this.matchingService.checkULikeWho(
          req.user!["id"],
          result.userId
        );
        if (finalResult[0] == null) {
          finalResults.push(result);
        }
      }
      if (finalResults.length) {
      }
      console.log('finalResults', finalResults);
      res.json({
        isSuccess: true,
        data: finalResults,
      });
    } catch (e) {
      res.status(401).json({
        isSuccess: false,
        data: 'unexpected error ' + e,
      });
    }
  };
}
