import { AdminService } from "../services/admin-service";
import { Request, Response } from 'express';
import { JWTAdminPayload } from "../helpers/types";
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';

export class AdminController {

  constructor(private adminService: AdminService) { this.adminService }

  getUserData = async (req: Request, res: Response) => {
    let result = await this.adminService.getUserData()
    res.json(result)
    return
  }

  getUserDetail = async (req: Request, res: Response) => {
    let result = await this.adminService.getUserDetail(req.body.userID)
    res.json(result)
    return
  }

  getAdminData = async (req: Request, res: Response) => {
    let result = await this.adminService.getAllAdminData()
    res.json(result)
    return
  }

  getReportData = async (req: Request, res: Response) => {
    let result = await this.adminService.getReportData()
    res.json(result)
    return
  }

  getData = async (req: Request, res: Response) => {
    // let getData = await this.adminService.getData()
    let dailyUser = await this.adminService.getDailyUsers()
    let dailyRegistration = await this.adminService.getDailyRegistration()
    let dailyMatch = await this.adminService.dailyMatch()
    let dailyConsumption = await this.adminService.getDailyConsumption()
    res.json({ dailyUser: dailyUser, dailyRegistration: dailyRegistration, dailyConsumption: dailyConsumption, dailyMatch: dailyMatch })
    return
  }

  updateReplyStatus = async (req: Request, res: Response) => {
    try{
      let adminID = req.user!.id
      let result
      if (req.body.reply === "no"){
        let checkNotIntern = await this.adminService.checkNotIntern(adminID)
        if (checkNotIntern.isSuccess === false){
          res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
          return
        }
        let adminDetail = await this.adminService.getCurrentAdmin(adminID)
        if (adminDetail.isSuccess === false){
          res.end("failed to update multi reply status")
          return
        }
        result = await this.adminService.updateReplyStatus(req.body.reply, req.body.report_id, adminID, adminDetail.data?.role)
      } else {
        let checkSuper = await this.adminService.checkSuper(adminID)
        if (checkSuper.isSuccess === false){
          res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
          return
        }
        result = await this.adminService.restoreReplyStatus(req.body.reply, req.body.report_id, adminID)
      }
      if (result.isSuccess === true){
        res.end("updated reply status")
        return
      } else {
        res.end("failed to update reply status")
        return
      }
      return
    } catch(e){
      console.error(e)
    }
  }

  updateMultiReplyStatus = async (req: Request, res: Response) => {
    let adminID = req.user!.id
    let checkNotIntern = await this.adminService.checkNotIntern(adminID)
    if (checkNotIntern.isSuccess === false){
      res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
      return
    }
    let adminDetail = await this.adminService.getCurrentAdmin(adminID)
    if (adminDetail.isSuccess === false){
      res.end("failed to update multi reply status")
      return
    }
    let result = await this.adminService.updateMultiReplyStatus(req.body.report_ids, adminID, adminDetail.data?.role)
    if (result.isSuccess === true){
      res.end("updated multi reply status")
      return
    } else {
      res.end("failed to update multi reply status")
    }
    return
  }

  restoreMultiReplyStatus = async (req: Request, res: Response) => {
    let adminID = req.user!.id
    let checkSuper = await this.adminService.checkSuper(adminID)
    if (checkSuper.isSuccess === false){
      res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
      return
    }
    let result = await this.adminService.restoreMultiReplyStatus(req.body.report_ids)
    if (result.isSuccess === true){
      res.end("restored multi reply status")
      return
    } else {
      res.end("failed to restore multi reply status")
    }
    return
  }

  updateReadStatus = async (req: Request, res: Response) => {
    await this.adminService.updateReadStatus(req.body.report_id)
    res.end("updated read status")
    return
  }

  editMultiUsername = async (req: Request, res: Response) => {
    await this.adminService.editMultiUsername(req.body.userIDs, req.body.newName)
    res.end("updated username")
    return
  }

  updateMultiCoin = async (req: Request, res: Response) => {
    try {
      let adminID = req.user!.id
      let checkNotIntern = await this.adminService.checkNotIntern(adminID)
      if (checkNotIntern.isSuccess === false){
        res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
        return
      }
      let result = await this.adminService.updateMultiCoin(req.body.userIDs, req.body.newCoin, req.body.description, adminID)
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
        })
        return
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  deleteMultiUser = async (req: Request, res: Response) => {
    let result = await this.adminService.deleteMultiUser(req.body.userIDs)
    if (result.isSuccess === true){
      res.status(200).json({isSuccess: true})
      return
    } else {
      res.status(400).json({isSuccess: false, msg: "Failed to delete user(s)"})
      return
    }
  }

  deleteMultiAdmin = async (req: Request, res: Response) => {
    let adminID = req.user!.id
    let checkSuper = await this.adminService.checkSuper(adminID)
    if (checkSuper.isSuccess === false){
      res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
      return
    }
    let result = await this.adminService.deleteMultiAdmin(req.body.adminIDs)
    if (result.isSuccess === true){
      res.status(200).json({isSuccess: true})
      return
    } else {
      res.status(400).json({isSuccess: false, msg: "Failed to delete administrator(s)"})
      return
    }
  }

  adminLogin = async (req: Request, res: Response) => {
    try {
      let result = await this.adminService.adminLogin(req.body.email, req.body.password)
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "wrong password or email."
        })
        return
      } else if (result.isSuccess === true) {
        let id = result.data!.id
        let payload: JWTAdminPayload = { id }
        const token = jwtSimple.encode(payload, jwt.jwtSecret);
        res.status(200).json({
          isSuccess: true,
          data: {
            admin_data: result.data,
            token: token
          }
        })
        return
      }
    } catch (error) {
      console.log("admin login: " + error)
      res.status(500).json({
        isSuccess: false,
        msg: "server error" + error
      })
    }
  }

  updatePassword = async (req: Request, res: Response) => {
    let adminID = req.user!.id
    let result = await this.adminService.updatePassword(adminID, req.body.newPassword, req.body.oldPassword)
    if (result.isSuccess === false) {
      res.status(401).end("failed")
      return
    } else if (result.isSuccess === true) {
      res.status(200).end("success")
      return
    }
  }

  updateTheme = async (req: Request, res: Response) => {
    let adminID = req.user!.id
    let result = await this.adminService.updateTheme(req.body.theme, adminID)
    if (result.isSuccess === false) {
      res.status(401).end("failed")
      return
    } else if (result.isSuccess === true) {

      res.status(200).end("success")
      return
    }
  }

  getCurrentAdmin = async (req: Request, res: Response) => {
    try {
      let adminID = req.user!.id
      let result = await this.adminService.getCurrentAdmin(adminID)
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "wrong password or email."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
          data: result.data
        })
        return
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  addAdmin = async (req: Request, res: Response) => {
    try {
      if (req.body.role !== "Super" && req.body.role !==  "Ordinary" && req.body.role !== "Intern"){
        res.status(401).json({
          isSuccess: false,
          msg: "Incorrect role input."
        })
        return
      }
      let check = await this.adminService.checkAdmin(req.body.email)
      if (check.isSuccess === false){
        res.status(401).json({
          isSuccess: false,
          msg: "Email is already registered."
        })
        return
      }
      let result = await this.adminService.addAdmin(req.body.email, req.body.adminName, req.body.password, req.body.role)
      console.log("result: ", result)
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
        })
        return
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
      return
    }
  }

  updateAdmin = async (req: Request, res: Response) => {
    try {
      let adminID = req.user!.id
      let checkSuper = await this.adminService.checkSuper(adminID)
      if (checkSuper.isSuccess === false){
        res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
        return
      }
      let result = await this.adminService.updateAdmin(req.body.adminName, req.body.email, req.body.role, adminID)
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
        })
        return
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  updateMultiAdmin = async (req: Request, res: Response) => {
    try {
      let adminID = req.user!.id
      let checkSuper = await this.adminService.checkSuper(adminID)
      if (checkSuper.isSuccess === false){
        res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
        return
      }
      let result = await this.adminService.updateMultiAdmin(req.body.content)
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please be informed that email cannot be duplicated. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
        })
        return
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  blockUser = async (req: Request, res: Response) => {
    try {
      let adminID = req.user!.id
      let checkNotIntern = await this.adminService.checkNotIntern(adminID)
      if (checkNotIntern.isSuccess === false){
        res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
        return
      }
      let result = await this.adminService.blockUser(req.body.userID)
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
        })
        return
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  unblockUser = async (req: Request, res: Response) => {
    try {
      let adminID = req.user!.id
      let checkNotIntern = await this.adminService.checkNotIntern(adminID)
      if (checkNotIntern.isSuccess === false){
        res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
        return
      }
      let result = await this.adminService.unblockUser(req.body.userID)
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
        })
        return
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  removeProfilePic = async (req: Request, res: Response) => {
    try {
      let adminID = req.user!.id
      let checkNotIntern = await this.adminService.checkNotIntern(adminID)
      if (checkNotIntern.isSuccess === false){
        res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
        return
      }
      let result = await this.adminService.removeProfilePic(req.body.userID, req.body.url)
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
        })
        return
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  editUserDetail = async (req: Request, res: Response) => {
    try {
      let adminID = req.user!.id
      let checkNotIntern = await this.adminService.checkNotIntern(adminID)
      if (checkNotIntern.isSuccess === false){
        res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
        return
      }
      let result = await this.adminService.editUserDetail(req.body.content, req.body.userID)
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
        })
        return
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  deletePhoto = async (req: Request, res: Response) => {
    try {
      let adminID = req.user!.id
      let checkNotIntern = await this.adminService.checkNotIntern(adminID)
      if (checkNotIntern.isSuccess === false){
        res.status(401).json({isSuccess: false, msg: "Unauthorized order"})
        return
      }
      let result = await this.adminService.deletePhoto(req.body.url, req.body.userID)
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
        })
        return
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  getUserTagAnalystData = async (req: Request, res: Response) => {
    try {
      let result = await this.adminService.getUserTagAnalystData()
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
          data: result.data
        })
        return 
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  getUserWishTagAnalystData = async (req: Request, res: Response) => {
    try {
      let result = await this.adminService.getUserWishTagAnalystData()
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
          data: result.data
        })
        return 
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  getSubData = async (req: Request, res: Response) => {
    try {
      let result
      if (req.body.title === "userTag"){
        result = await this.adminService.getSubData(req.body.type)
      } else if (req.body.title === "userWishTag"){
        result = await this.adminService.getWishTagSubData(req.body.type)
      } else if (req.body.title === "gender") {
        result = await this.adminService.getGenderSubData(req.body.type)
      } else {
        result = await this.adminService.getAgeSubData(req.body.type)
      }
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
          data: result.data
        })
        return 
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  getUserGenderAnalystData = async (req: Request, res: Response) => {
    try {
      let result = await this.adminService.getUserGenderAnalystData()
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
          data: result.data
        })
        return 
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }

  getUserAgeAnalystData = async (req: Request, res: Response) => {
    try {
      let result = await this.adminService.getUserAgeAnalystData()
      if (result.isSuccess === false) {
        res.status(401).json({
          isSuccess: false,
          msg: "Something went wrong. Please try again."
        })
        return
      } else if (result.isSuccess === true) {
        res.status(200).json({
          isSuccess: true,
          data: result.data
        })
        return 
      }
    } catch (e) {
      console.log("error: ", e)
      res.status(500).json({
        isSuccess: false,
        msg: "error"
      })
    }
  }
}