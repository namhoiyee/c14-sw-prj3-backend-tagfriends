import { Request, Response } from 'express';
// import { checkPassword } from '../hash';
import { UserService } from '../services/user-service';
import fetch from 'node-fetch';
import { JWTPayload } from '../helpers/types';
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';
export class UserController {

    constructor(private userService: UserService) { this.userService }

    registration = async (req: Request, res: Response) => {
        try {
            let check = await this.userService.checkEmail(req.body.email);
            // console.log(check)
            if (check) {
                // console.log('Email is already registered!')
                res.status(403).json({ isSuccess: false, msg: 'Email is already registered!' })
            } else {
                let result = await this.userService.registrationService(req.body);
                if (result.isSuccess) {
                    let { id, registration_status } = result.data!
                    let payload: JWTPayload = {
                        id,
                        username: req.body.username,
                        lang: req.body.loginLang,
                        registration_status
                    }
                    const token = jwtSimple.encode(payload, jwt.jwtSecret);
                    res.status(200).json({
                        isSuccess: true,
                        data: token
                    })
                } else {
                    let result = await this.userService.registrationService(req.body);
                    if (result.isSuccess) {
                        let { id, registration_status } = result.data!
                        let payload: JWTPayload = {
                            id,
                            username: req.body.username,
                            lang: req.body.loginLang,
                            registration_status
                        }
                        const token = jwtSimple.encode(payload, jwt.jwtSecret);
                        res.status(200).json({
                            isSuccess: true,
                            data: token
                        })
                    } else {
                        res.status(500).json({
                            isSuccess: false,
                            msg: result.msg
                        })
                    }
    
                }
                // console.log('Registration success! Session: ', req.session['user'])
                // res.status(200).end('Registration success!')
            }
        } catch (e) {
            res.status(500).json({
                isSuccess: false,
                msg: 'unable to register'
            })
        }

    }
    updateFetchTime = async (req: Request, res: Response) => {
        let updateFetchTime = await this.userService.updateFetchTime(req.user!.id)
        // console.log('user registration status: ', userRegStatus)
        if (updateFetchTime.isSuccess) {
            res.json({ isSuccess: true, data: {coin:updateFetchTime.added} })
        } else {
            res.status(400).json({ isSuccess: false, msg: updateFetchTime.msg })
        }
    }


    checkUserRegStatus = async (req: Request, res: Response) => {
        try{
            let userRegStatus = await this.userService.getRegistrationStatus(req.user!.id)
        console.log('user registration status: ', userRegStatus)
            res.status(200).json({ isSuccess: true, data: userRegStatus })
        } catch (e) {
            res.status(401).json({ isSuccess: false, msg: 'unable to check registration status' + e })
        }
    }

    login = async (req: Request, res: Response) => {
        try {
            console.log(req.body)
            let result = await this.userService.loginService(req.body);
            console.log(result)
            if (result.isSuccess) {
                let payload: JWTPayload
                if (result.data!.lang != req.body.lang) {
                    await this.userService.updateLang(req.body.lang, result.data!.id)
                }
                
                if (result.data?.registration_status>1){
                    payload = {
                        id: result.data!.id,
                        username: result.data!.username,
                        lang: req.body.lang,
                        registration_status:result.data!.registration_status,
                        profilePic:result.data!.profilePic
                    }
                }else{
                    payload = {
                        id: result.data!.id,
                        username: result.data!.username,
                        lang: req.body.lang,
                        registration_status:result.data!.registration_status,
                    }
                }
                console.log("payload",payload)
                const token = jwtSimple.encode(payload, jwt.jwtSecret);
                res.status(200).json({
                    isSuccess: true,
                    data: {token:token,coinAdded:result.data!.coinsAdded}
                })
            } else {
                res.status(403).json({ isSuccess: false, msg: result.msg })
            }
        } catch (error) {
            res.status(500).json({ isSuccess: false, msg: 'login failed' + error })
        }
    }

    // logout = (req: Request, res: Response) => {
    //     // console.log("logout...origin session: ", req.session['user'])
    //     if (req.session['user']) {
    //         delete req.session['user']
    //     }
    //     if (req.session['chatroom']) {
    //         delete req.session['chatroom']
    //     }
    //     // console.log("Logout success! Removed session: ", req.session['user'])
    //     res.end('/')
    // }
    //TODO
    loginGoogle = async (req: Request, res: Response) => {
        try {
            const accessToken: string = req.session?.['grant'].response.access_token;
            // console.log(accessToken)
            const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
                method: "get",
                headers: {
                    "Authorization": `Bearer ${accessToken}`
                }
            });
            const result = await fetchRes.json();
            // console.log(result)
            const users = (await this.userService.checkEmail(result));
            // const users = (await client.query(`SELECT * FROM users WHERE users.username = $1`, [result.email])).rows;
            const user = users[0];
            // console.log(user)
            if (result)
                if (!user) {
                    // console.log('go to registration')
                    let registerInfo = await this.userService.registrationServiceForGoogle(result);
                    req.session['user'] = registerInfo;
                    delete req.session['user'].password;
                    req.session['user']['lang'] = req.body.lang
                    // console.log('Registration success! Session: ', req.session['user'])
                    res.redirect('/main')
                } else {
                    // console.log(result.id)
                    const checkingToken = (await this.userService.checkToken(result.id));
                    // console.log("checkingToken", checkingToken)
                    if (!checkingToken[0].google_access_token) {
                        // console.log('Login google failed')
                        res.status(403).end('Login google failed')
                    } else {
                        req.session['user'] = checkingToken[0];
                        delete req.session['user'].password;
                        if (req.body.loginLang == "en") {
                            req.session['user']['lang'] = "en"
                        } else if (req.body.loginLang == "zh") {
                            req.session['user']['lang'] = "zh"
                        }
                        // console.log('Login success! Session: ', req.session['user'])
                        res.redirect('/main')
                    }
                }
        } catch (e) {
            res.status(500).json({
                isSuccess:false,
                msg:'unable to login google ' + e
            })
        }
    }

    loginRedirect = (req: Request, res: Response) => {
        if (req.session?.['user']) {
            res.redirect("/main/main.html")
        } else {
            res.redirect("/login.html")
        }
    }

    getLanguage = async (req: Request, res: Response) => {
        let result = await this.userService.getLang(req.session['user']['id'])
        res.json(result[0].lang)
    }

    updateLanguage = async (req: Request, res: Response) => {
        await this.userService.updateLang(req.body?.lang, req.session['user']['id'])
        res.status(200).end('updated language')
    }

    // useCoins = async (req: Request, res: Response) => {
    //     try {
    //         const coinUsedRes = await this.userService.canUseCoins(req.user!.id, req.body?.coins)
    //         // console.log("coinUsedRes",coinUsedRes)
    //         if (coinUsedRes) {
    //             res.status(200).json({ coinUsedRes })
    //         } else {
    //             res.status(401).json('insufficient balance')
    //         }
    //     } catch (e) {
    //         throw e
    //     }
    
    // }

    //when user_state is updated. username, id, lang
    updateToken = async (req: Request, res: Response) => {
        try {
            const userId = req.user!.id
            const userDetail = await this.userService.getAllInfoForToken(userId)
            if (userDetail[0].id === userId) {
                const user = userDetail[0]
                let payload: JWTPayload = {
                    id: user.id,
                    username:user.username,
                    lang: user.lang,
                    registration_status: user.registration_status,
                    profilePic: user.url,
                }
                const token = jwtSimple.encode(payload, jwt.jwtSecret);
                res.status(200).json({
                    isSuccess: true,
                    data: token
                })
            }else {
                res.status(500).json({
                    isSuccess:false,
                    msg:'unable to update Token'
                })
            }
        } catch (e) {
            res.status(500).json({
                isSuccess:false,
                msg:'unable to update Token'
            })
        }


    }
}