import { Request, Response } from 'express';
import { ProfileService } from '../services/profile-service';
// import fs from 'fs'
// import path from 'path'
import fetch from 'node-fetch'
import { deleteFromS3 } from '../helpers/deleteFromS3';
import { TagClass } from '../helpers/types';




export class ProfileController {

  constructor(private profileService: ProfileService) { this.profileService }

  nonDuplicableClassesForWishTags = ['習慣2', '習慣1', '尋求'];
  nonDuplicableClassesForUserTags = ['習慣2', '習慣1', '尋求', "性別", "年齡", "星座"]

  getProfile = async (req: Request, res: Response) => {
    try {
      let result = await this.profileService.getProfile(req.user!.id)
      delete result.profile['password']
      // console.log('profile from controller: ', profile)
      if (result.pictures.length) {
        result.pictures.forEach((pic, i) => {
          if (pic.description == 'profile')
            result.profile['profilePic'] = pic.url
          result.pictures.splice(i, 1)
        })
        // console.log("user photos:", profile.pictures)
        // console.log("profileGetProfile",profile.profile[0])
      }
      res.status(200).json({
        isSuccess: true,
        data: result
      })
    } catch (e) {
      res.json({
        isSuccess: false,
        msg: "unable to get profile" + e
      })
    }

  }

  // updateProfilePic = async (req: Request, res: Response) => {
  //   // console.log("req body: ",req.body)
  //   // console.log("original: ", req.file)
  //   let userId = req.session['user'].id
  //   try {
  //     if (req.body.picture) {
  //       let result, profilePic, originalProfilePic: any
  //       let circleFileName = userId + '-' + Date.now() + ".png"
  //       let filePath = path.join(process.cwd(), 'public', 'main', 'uploads', circleFileName)
  //       fs.writeFile(filePath, req.body.picture.split(',')[1], 'base64', (err) => { console.log(err) })
  //       // console.log("filePath: ",filePath)

  //       if (req.file) {
  //         result = await this.profileService.uploadProfilePic(userId, circleFileName, req.file.filename)
  //         originalProfilePic = result.originalProfilePic
  //         profilePic = result.profilePic
  //       } else {
  //         result = await this.profileService.uploadProfilePic(userId, circleFileName)
  //         originalProfilePic = result.originalProfilePic
  //         profilePic = result.profilePic

  //       }
  //       if (profilePic.length) {
  //         req.session['user'].profile_pic = profilePic[0].url
  //         console.log("original profile: ", originalProfilePic[0].url)
  //         // console.log("upload pic:", req.session['user'])
  //         res.status(202).end('Successfully uploaded profile picture.')
  //       } else {
  //         res.status(402).end('user not found.')
  //       }
  //     }
  //     else {
  //       res.status(401).end("no file uploaded.")
  //     }
  //   } catch (e) {
  //     console.log(e)
  //   }
  // }

  // addProfilePic = async (req: Request, res: Response) => {
  //   // console.log("req body: ",req.body)
  //   // console.log("original: ", req.file)
  //   let userId = req.session['user'].id
  //   try {
  //     let originalProfilePic: any
  //     if (req.file) {
  //       originalProfilePic = (await this.profileService.uploadProfilePic(userId, req.file.filename)).originalProfilePic

  //       if (originalProfilePic.length) {
  //         // console.log("original profile: ", originalProfilePic[0].url)
  //         // console.log("upload pic:", req.session['user'])
  //         res.status(202).end('Successfully uploaded profile picture.')
  //       } else {
  //         res.status(402).end('user not found.')
  //       }
  //     }
  //     else {
  //       res.status(401).end("no file uploaded.")
  //     }
  //   } catch (e) {
  //     console.log(e)
  //   }
  // }

  uploadProfilePic = async (req: Request, res: Response) => {
    try {
      if (req.files['profile'] && req.files['originalProfile']) {
        let profilePath = req.files['profile'][0].key
        let originalProfilePath = req.files['originalProfile'][0].key
        let userId = req.user!.id
        let port = 8000 + (req.user!['id'] % 4);
        port = 8001
        // console.log('port: ', port)
        try {
          console.log("checking image")
          let resp = await fetch(`http://localhost:${port}/image`, {
            method: "POST",
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(profilePath)
          })
          let pass = await resp.json()
          let faces = pass.face
          // console.log("coordinates:", pass)
          // if (faces > 0) {
          //   let profiles = await this.profileService.uploadProfilePic(userId, profilePath, originalProfilePath)
          //   res.status(201).json({
          //     isSuccess: true,
          //     data: { profiles, msg: "Please update token" }
          //   })
  
          if (faces === 0) {
            res.status(406).json({
              isSuccess: false,
              msg: 'No face detected. Pick another photo.'
            })
            Promise.all([deleteFromS3(req.files['profile'][0].key), deleteFromS3(req.files['originalProfile'][0].key)])
          } else {
            let profiles = await this.profileService.uploadProfilePic(userId, profilePath, originalProfilePath)
            res.status(201).json({
              isSuccess: true,
              data: { profiles, msg: "Please update token" }
            })
            if (pass.msg) {
              console.log("python server error" + pass.msg)
            }
          }
  
  
        } catch (err) {
          Promise.all([deleteFromS3(req.files['profile'][0].key), deleteFromS3(req.files['originalProfile'][0].key)])
          throw new Error("update Profile pic" + err)
        }
  
      }
      else {
        res.status(406).json({
          isSuccess: false,
          msg: 'no file chosen.'
        })
      }
    } catch (e) {
      res.status(401).json({
        isSuccess: false,
        msg: 'unexpected error in upload profile pic ' + e
      })
    }
  }


  updateProfile = async (req: Request, res: Response) => {
    // console.log("controller:",req.body)
    try {
      let userId = req.user!.id
    // console.log("update profile", req.body)
      if (!req.body || !req.body.gender || !req.body.birthday || !req.body.username || !req.body.text) {
        res.status(401).json({
          isSuccess: false,
          msg: "not enough information."
        })
        return
      }

      let profile = await this.profileService.updateProfile(req.body, userId)

      if (profile.length > 0) {
        // console.log('profile',profile)
        res.status(202).json({
          isSuccess: true,
          data: {
            profile: { ...profile[0], gender: req.body.gender },
            msg: 'please update token',
          }
        })
      } else {
        res.status(401).json({
          isSuccess: false,
          msg: "user not found."
        })
      }
    } catch (error) {
      console.log('update profile', error)
      res.status(401).json({
        isSuccess: false,
        msg: "update profile failed."
      })
    }
    
  }

  uploadVoice = async (req: Request, res: Response) => {
    // console.log("controller:",req.body)
    let userId = req.user!.id
    console.log("update voice", req.file)
    if (req.file) {
      let url = (req.file as any).key
      console.log(url)
      let voice = await this.profileService.uploadVoice(url, userId)
      // console.log(profile)
      if (voice.length) {
        res.status(200).json({
          isSuccess: true,
          data: {
            voice,
          }
        })
      } else {
        res.status(401).json({
          isSuccess: false,
          msg: 'cannot update on database'
        })
      }
    }
    else {
      res.status(401).json({
        isSuccess: false,
        msg: "no file received."
      })
    }
  }



  getOwnUserTags = async (req: Request, res: Response) => {
    try {
      // console.log('getting user tag')
      let result = await this.profileService.getOwnUserTags(req.user!.id);
      // let count = await this.profileService.getTagCount(req.user!.id);
      // console.log('tags: ', result)
      res.json({
        isSuccess: true,
        data: {
          userTags: result,
          tagCount: result.length
        }
      })
    } catch (e) {
      res.status(401).json({
        isSuccess: false,
        msg: "get User Tags failed:" + e
      })
    }

  }

  getWishTags = async (req: Request, res: Response) => {
    try {
      let result = await this.profileService.getWishTags(req.user!['id']);
      // let tagCount = await this.profileService.getWishTagCount(req.user!['id']);
      res.json({
        isSuccess: true,
        data: {
          wishTags: result,
          tagCount: result.length
        }
      })
    } catch (e) {
      res.status(401).json({
        isSuccess: false,
        msg: 'get wish tags failed: ' + e
      })
    }
    // console.log('getting user wish tag')
  }

  getMatchUserTags = async (req: Request, res: Response) => {
    // console.log({getMatchUserTags: req.session['userGrid']})
    let matchIds: number[] = req.body.id
    try {
      if (matchIds.length > 0) {
        // let result = []
        // for (let i = 0; i < matchIds.length; i++) {
        const result = await this.profileService.getTagsForUsers(matchIds);
          // result.push(res)
        // }
        // console.log({getMatchUserTags: req.session['userGrid']})
        res.status(200).json({
          isSuccess: true,
          data: result
        })
      } else {
        res.status(401).json({
          isSuccess: false,
          msg: 'no matched user'
        })
      }
    } catch (e) {
      console.log(e)
      res.json(500).json({
        isSuccess: false,
        msg: 'unable to find tags' + e
      })
    }
  }

  getClassAndTags = async (req: Request, res: Response) => {
    try {
      let tagClasses: TagClass[] = await this.profileService.getClass();
      // console.log('class: ', TagClass)
      // console.log('getting all tags')
      let tags = await this.profileService.getAllTags();

      const tagClassesStructured = tagClasses.map(tagClass => {
        const tagsForSameClass = tags.filter(tags => tags.classId === tagClass.id)
        return {...tagClass, tags: tagsForSameClass}
      })

      res.json({
        isSuccess: true,
        data: tagClassesStructured
      })
    } catch (e) {
      res.status(401).json({
        isSuccess: false,
        msg: 'Error: get all classes and tags :' + e
      })
    }

  }

  addUserTag = async (req: Request, res: Response) => {
    try {

      let userId = req.user!.id
        // console.log('edit', req.body?.oldTagId, req.body?.newTagTagId, req.body?.oldTagTagId)
      if (!req.body?.newTag) {
        throw new Error('missing newTag')
      }

      const newTagTagID = req.body.newTag.id
      const newTagClass = req.body.newTag.class
      
      await this.checkNonDuplicableClassesForUserTags(newTagClass, 'XXX', userId, 'Add')

      let tag = await this.profileService.addUserTag(newTagTagID, userId);
      res.status(201).json({
        isSuccess: true,
        data: { tag }
      });
    } catch (e) {
      res.status(401).json({
        isSuccess: false,
        msg: 'Error add wish tag' + e

      })
    }
  }

  addWishTag = async (req: Request, res: Response) => {
    // console.log('adding a new wish tag...tag: ', req.body)
    try {

      let userId = req.user!.id
        // console.log('edit', req.body?.oldTagId, req.body?.newTagTagId, req.body?.oldTagTagId)
      if (!req.body?.newTag) {
        throw new Error('missing newTag')
      }

      const newTagTagID = req.body.newTag.id
      const newTagClass = req.body.newTag.class
      
      await this.checkNonDuplicableClassesForWishTags(newTagClass, 'XXX', userId, 'Add')

      let tag = await this.profileService.addWishTag(newTagTagID, userId);
      res.status(201).json({
        isSuccess: true,
        data: { tag }
      });
    } catch (e) {
      res.status(401).json({
        isSuccess: false,
        msg: 'Error add wish tag' + e

      })
    }
  }

  deleteUserTag = async (req: Request, res: Response) => {
    try {
      let userId = req.user!.id
      // console.log('edit', req.body?.oldTagId, req.body?.newTagTagId, req.body?.oldTagTagId)
      if (!req.body?.originalTag) {
        throw new Error('missing originalTag')
      }

      const originalTagTagID = req.body.originalTag.id
      const originalTagClass = req.body.originalTag.class
      
      if (this.nonDuplicableClassesForWishTags.includes(originalTagClass)) {
        throw new Error('deleting non-deletable class')
      }

      let queryMsg = await this.profileService.deleteUserTag(originalTagTagID, userId);
      res.status(202).json({
        isSuccess: true,
        data: {
          msg: queryMsg
        }
      });
    } catch (e) {
      res.status(401).json({
        isSuccess: false,
        msg: 'Error delete user tag ' + e

      })
    }

  }

  deleteWishTag = async (req: Request, res: Response) => {
    try {
      let userId = req.user!.id
      // console.log('edit', req.body?.oldTagId, req.body?.newTagTagId, req.body?.oldTagTagId)
      if (!req.body?.originalTag) {
        throw new Error('missing originalTag')
      }

      const originalTagTagID = req.body.originalTag.id
      const originalTagClass = req.body.originalTag.class
      
      if (this.nonDuplicableClassesForWishTags.includes(originalTagClass)) {
        throw new Error('deleting non-deletable class')
      }

      let queryMsg = await this.profileService.deleteWishTag(originalTagTagID, userId);
      res.status(202).json({
        isSuccess: true,
        data: {
          msg: queryMsg
        }
      });
    } catch (e) {
      res.status(401).json({
        isSuccess: false,
        msg: 'Error delete wish tag' + e

      })
    }
  }

  newRegistration = async (req: Request, res: Response) => {
    // console.log('handling new registration info')
    // console.log('required questionnaire: ', req.body)
    try {
      if (req.body.username !== "") {
        await this.profileService.updateUsername(req.body.username, req['user']!['id']);
      }
      if (req.body.gender !== 'null') {
        await this.profileService.updateGender(req.body.gender, req['user']!['id'])
      }

      let info = await this.profileService.insertNewRegistrationInfo(req.body.birthday, req.body.habitOne, req.body.habitTwo, req['user']!['id'])
      console.log("info", info)
      res.status(200).json({
        isSuccess: true,
        data: info
      })
    } catch (e) {
      console.log(e)
      res.status(401).json({
        isSuccess: false,
        msg: "failed to complete update for questionnaire." + e
      })
    }
  }

  editUserWishTag = async (req: Request, res: Response) => {
    // console.log('handling edit tag...: ', req.body)
    // let smokingTag, drinkingTag
    let userId = req.user!.id
    try {
      // console.log('edit', req.body?.oldTagId, req.body?.newTagTagId, req.body?.oldTagTagId)
      if (!req.body?.oldTag || !req.body?.newTag) {
        throw new Error('missing oldTag / newTag')
      }
      // if (req.body.tagCigar) {
      //   // console.log('handling cigar')
      //   let oldTagID = await this.profileService.getTagID(req.body.tagCigar)
      //   let newTagID = await this.profileService.getTagID(req.body.newTag)
      //   // console.log('old: ', oldTagID, 'and new: ', newTagID)
      //   smokingTag = (await this.profileService.updateUserTag(oldTagID[0]['id'], newTagID[0]['id'], req['user']!['id']))[0]
      // } else {
        // console.log('handling alcohol')
        let oldTagID = req.body.oldTag.dbPrimaryId
        let oldTagTagID = req.body.oldTag.id
        let newTagTagID = req.body.newTag.id
        let oldTagClass = req.body.oldTag.class
        let newTagClass = req.body.newTag.class
        if (oldTagTagID === newTagTagID) {
          res.status(202).json({
            isSuccess: true,
            data: {
              msg: 'no changed'
            }
          })
          return
        }
        await this.checkNonDuplicableClassesForWishTags(newTagClass, oldTagClass, userId, 'Edit')
        console.log('old: ', oldTagID, 'and new: ', newTagTagID)
        await this.profileService.updateUserWishTag(oldTagID, newTagTagID, userId)
      // }
      // if (smokingTag || drinkingTag) {

        res.status(202).json({
          isSuccess: true,
          data: {
            msg: 'success'
          }
        })
      // } else {
      //   res.json({
      //     isSuccess: false,
      //     msg: "user does not have the old tag"
      //   })
      // }
    } catch (error) {
      res.status(401).json({
        isSuccess: false,
        msg: "unable to update wish tag. " + error
      })
    }
  }

  checkNonDuplicableClassesForUserTags = async (newTagClass: string, oldTagClass: string, userId: number, mode: 'Edit' | 'Delete' | 'Add') => {
    const editModeCheck = mode === 'Edit' && this.nonDuplicableClassesForUserTags.includes(newTagClass) && !this.nonDuplicableClassesForUserTags.includes(oldTagClass)
    const addModeCheck = mode === 'Add' && this.nonDuplicableClassesForUserTags.includes(newTagClass)
    try {
      if (editModeCheck || addModeCheck) {
        const checkUnique = await this.profileService.checkEditUnique(userId, this.nonDuplicableClassesForUserTags, false)
        if (checkUnique.length > 0 && checkUnique.filter(item => item.class === newTagClass).length > 0) {
          throw new Error('duplicated class')
        } 
      }
    } catch (e) {
      throw e
    }
  }

  checkNonDuplicableClassesForWishTags = async (newTagClass: string, oldTagClass: string, userId: number, mode: 'Edit' | 'Delete' | 'Add') => {
    const editModeCheck = mode === 'Edit' && this.nonDuplicableClassesForWishTags.includes(newTagClass) && !this.nonDuplicableClassesForWishTags.includes(oldTagClass)
    const addModeCheck = mode === 'Add' && this.nonDuplicableClassesForWishTags.includes(newTagClass)
    try {
      if (editModeCheck || addModeCheck) {
        const checkUnique = await this.profileService.checkEditUnique(userId, this.nonDuplicableClassesForWishTags, true)
        if (checkUnique.length > 0 && checkUnique.filter(item => item.class === newTagClass).length > 0) {
          throw new Error('duplicated class')
        } 
      }
    } catch (e) {
      throw e
    }
  }

  
  editUserTag = async (req: Request, res: Response) => {
    let userId = req.user!.id
    try {
      // console.log('edit', req.body?.oldTagId, req.body?.newTagTagId, req.body?.oldTagTagId)
      if (!req.body?.oldTag || !req.body?.newTag) {
        throw new Error('missing oldTag / newTag')
      }
      // if (req.body.tagCigar) {
      //   // console.log('handling cigar')
      //   let oldTagID = await this.profileService.getTagID(req.body.tagCigar)
      //   let newTagID = await this.profileService.getTagID(req.body.newTag)
      //   // console.log('old: ', oldTagID, 'and new: ', newTagID)
      //   smokingTag = (await this.profileService.updateUserTag(oldTagID[0]['id'], newTagID[0]['id'], req['user']!['id']))[0]
      // } else {
        // console.log('handling alcohol')
        let oldTagID = req.body.oldTag.dbPrimaryId
        let oldTagTagID = req.body.oldTag.id
        let newTagTagID = req.body.newTag.id
        let oldTagClass = req.body.oldTag.class
        let newTagClass = req.body.newTag.class
        if (oldTagTagID === newTagTagID) {
          res.status(202).json({
            isSuccess: true,
            data: {
              msg: 'no changed'
            }
          })
          return
        }
        await this.checkNonDuplicableClassesForUserTags(newTagClass, oldTagClass, userId, 'Edit')
        console.log('old: ', oldTagID, 'and new: ', newTagTagID)
        await this.profileService.updateUserTag(oldTagID, newTagTagID, userId)
      // }
      // if (smokingTag || drinkingTag) {

        res.status(202).json({
          isSuccess: true,
          data: {
            msg: 'success'
          }
        })
      // } else {
      //   res.json({
      //     isSuccess: false,
      //     msg: "user does not have the old tag"
      //   })
      // }
    } catch (error) {
      res.status(401).json({
        isSuccess: false,
        msg: "unable to update wish tag. " + error
      })
    }
  }


  getGallery = async (req: Request, res: Response) => {
    // console.log('handling get gallery...user id: ', req.session['user'].id)
    try {
      let gallery = await this.profileService.getGallery(req['user']!.id)
      res.json({
        isSuccess: true,
        data: gallery
      })
    } catch (error) {
      res.status(401).json({
        isSuccess: false,
        msg: "Error: failed to get gallery :" + error
      })
    }

    // console.log('got gallery: ', gallery)

  }

  addPhotos = async (req: Request, res: Response) => {
    try {
      console.log('addPhotos', req.files)
      let urlArr = []

      for (let i = 0; i < req.files.length; i++) {
        urlArr.push(req.files[i].key)
      }
      let photos = await this.profileService.addGallery(urlArr, req.user!.id)
      console.log('photo query', photos)
      if (photos.length > 0) {
        res.status(201).json({
          isSuccess: true,
          data: photos
        })
      } else {
        console.log({
          isSuccess: false,
          msg: "exceeded photos number"
        })
        res.status(400).json({
          isSuccess: false,
          msg: "exceeded photos number"
        })
      }
    } catch (e) {
      console.log(e)
      res.status(500).json({
        isSuccess: false,
        msg: "unable to add photos " + e
      })
    }
  }

  delPhotos = async (req: Request, res: Response) => {
    try {
      if (!req.body || !req.body.photos || !req.body.photos[0].id || !req.body.photos[0].url) {
        console.log(req.body)
        res.status(500).json({
          isSuccess: false,
          msg: "not enough information"
        })
        return
      }
      let urls = req.body.photos.map((photo: any) => photo.url)
      await this.profileService.deleteGallery(urls, req.user!.id)
      res.status(201).json({
        isSuccess: true,
        data: req.body.photos.map((photo: any) => photo.id)
      })
    } catch (e) {
      console.log(e)
      res.status(500).json({
        isSuccess: false,
        msg: "unable to del photos " + e
      })
    }
  }

  loadUserProfile = async (req: Request, res: Response) => {
    try {
      const userId = req.user!.id
      const userDetails = await this.profileService.loadUserProfile(userId)
      if (userDetails && userDetails.length > 0) {
        res.json({
          isSuccess: true,
          data: { info: userDetails[0] }
        })
        return
      }
      res.status(401).json({
        isSuccess: false,
        msg: 'failed to load user profile'
      })
    } catch (e) {
      console.log(e)
      res.status(401).json({
        isSuccess: false,
        msg: 'failed to load user profile'
      })
    }
  }

  loadUserProfilePic = async (req: Request, res: Response) => {
    try {
      const userId = req.user!.id
      const userProfilePic = await this.profileService.getProfilePic(userId)
      if (userProfilePic && userProfilePic.pictures.length > 1) {
        res.json({
          isSuccess: true,
          data: { userProfilePic: userProfilePic.pictures }
        })
        return
      }
      res.status(401).json({
        isSuccess: false,
        msg: 'failed to load user profile pic'
      })
    } catch (e) {
      console.log(e)
      res.status(401).json({
        isSuccess: false,
        msg: 'failed to load user profile pic'
      })
    }
  }



  updateStageThree = async (req: Request, res: Response) => {
    await this.profileService.updateStageThree(req.session['user']['id'])
    res.status(200).end('updated initial profile')
  }
}


