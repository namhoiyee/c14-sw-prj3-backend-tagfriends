import { ReportService } from "../services/report-service";
import { Request, Response } from 'express';


export class ReportController {

  constructor(private reportService: ReportService) { this.reportService }

  submitReport = async (req: Request, res: Response) => {
    try {
      // let userID = req.user!.id
      let userID = 1;
      let targetID;
      if (!req.body.targetID){
        targetID = 1
      } else {
        targetID = req.body.targetID
      }
      console.log(req.body)
      console.log("targetID: ", targetID)
      let result = await this.reportService.submitReport(
        userID,
        targetID,
        req.body.title,
        req.body.content,
      )
      if (result.isSuccess){
        res.status(200).json({isSuccess: true})
        return 
      }
    } catch (e) {
      console.error(e)
      res.status(500).json({
        isSuccess: false,
        msg: "failed to submit report"
      })
    }
  }

}