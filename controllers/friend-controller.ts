import { Request, Response } from "express";
import { FriendService } from "../services/friend-service";
import { io } from "../socketio";
// import fetch from 'node-fetch'
// import { ProfileService } from '../services/profile-service';

export class FriendController {
  constructor(private friendService: FriendService) {}

  getFriendList = async (req: Request, res: Response) => {
    try {
      let userRelationship = await this.friendService.friendList(req.user!.id);
      res.json({
        isSuccess: true,
        data: userRelationship,
      });
    } catch (e) {
      res.status(401).json({
        isSuccess: false,
        msg: "unable to get friendslist" + e,
      });
    }
  };

  getSuggestedType = async (req: Request, res: Response) => {
    try {
      // let userId = req.user!.id
      let selfRelId = req.body.selfRelId;
      let suggestion = await this.friendService.userSuggestions(selfRelId);
      res.status(201).json({
        isSuccess: true,
        data: suggestion,
      });
    } catch (error) {
      res.status(500).json({
        isSuccess: false,
        msg: "unable to find suggestions: " + error,
      });
    }
  };

  getFriendNameAndPhotoAndLastMessage = async (req: Request, res: Response) => {
    try {
      const userId = req.user!.id;
      const relationships = await this.friendService.checkFriends(userId);
      const friendIds = relationships?.map((relation) => relation.friend_id)
      const roomIds = relationships?.map((relation) => relation.room_id);
      if (friendIds && roomIds) {
        const friendNameAndPhoto =
          await this.friendService.getFriendNameAndPhoto(friendIds);
        const lastMessage: any[] = await this.friendService.getLastMessage(roomIds, userId);
        
        const combinedResult: {}[] = [];
        for (let id of friendIds) {
          const photoIndex = friendNameAndPhoto.findIndex(
            (item) => item.true_friend_id === id
          );
          const msgIndex = lastMessage.findIndex((item) =>
            userId === item?.user_id
              ? item?.friend_id === id
              : item?.user_id === id
          );
          if (msgIndex !== -1) {
            combinedResult.push({
              ...friendNameAndPhoto[photoIndex],
              ...lastMessage[msgIndex],
            });
          } else {
            combinedResult.push({
              ...friendNameAndPhoto[photoIndex]
            });
          }
          
        }

        // console.log("FriendNameAndPhoto", friendNameAndPhoto);
        // console.log("LastMessage", lastMessage);
        console.log("combinedResult", combinedResult);
        res.json({
          isSuccess: true,
          data: combinedResult,
        });
        return;
      }
      res.json({
        isSuccess: true,
        data: [],
      });
    } catch (e) {
      res.status(401).json({
        isSuccess: false,
        msg: 'unexpected error ' + e,
      });
    }
  };
  // //TODO
  // recordChatroomSession = async (req: Request, res: Response) => {
  //   try {
  //     // console.log('chatroom_id: ', req.body)
  //     await this.friendService.readMessage(req.body.friendChatroom);
  //     let userRelationship = await this.friendService.friendLikeList(
  //       req.user!.id
  //     );
  //     // console.log('got self relationship_id and friend_id', userRelationship)
  //     if (!userRelationship) return;
  //     for (let i = 0; i < userRelationship.length; i++) {
  //       if (userRelationship[i].id == req.body.selfChatroom) {
  //         let fdName = await this.friendService.getFriendNameAndPhoto(
  //           userRelationship[i]["friend_id"]
  //         );
  //         req.session["chatroom"] = req.body;
  //         req.session["chatroom"]["friendName"] = fdName[0]["username"];
  //         req.session["chatroom"]["friendID"] =
  //           userRelationship[i]["friend_id"];
  //         // console.log('chatroom session: ', req.session['chatroom'])
  //         res.status(201).send("chatroom created");
  //         return;
  //       }
  //     }
  //   } catch (e) {
  //     throw e;
  //   }
  // };
  //TODO
  getTotalUnreadMessageCount = async (req: Request, res: Response) => {
    try {
      let userId = req.user!.id
      let unreadCount = await this.friendService.getTotalUnreadMsg(userId)
      res.json({
        isSuccess:true,
        data: unreadCount[0]
      })
    } catch (error) {
      res.status(401).json({
        isSuccess:false,
        msg:"unable to get unread message count"
      })
    }
  }  

  getChatroomMessageInsideRoom = async (req: Request, res: Response) => {
    try {
      // let blockTime = new Date().toISOString();
      // console.log('getting chatroom message, session: ', req.session['chatroom'])
      // let checkBlockTime = await this.friendService.checkBlockTime(
      //   req.session["chatroom"]["selfChatroom"]
      // );
      // let checkBlockTime2 = await this.friendService.checkBlockTime(
      //   req.session["chatroom"]["friendChatroom"]
      // );
      // if (checkBlockTime[0]["blocked_time"] != null) {
      //   blockTime = checkBlockTime[0]["blocked_time"];
      // } else if (checkBlockTime2[0]["blocked_time"] != null) {
      //   blockTime = checkBlockTime2[0]["blocked_time"];
      // }
      if (!req.body?.roomId) {
        res.status(401).json({
          isSuccess:false,
          msg: "missing information"
        })
      }

      let userId = req.user!.id
      let roomId = req.body.roomId
      let message = await this.friendService.messageList(userId,roomId);
      
      console.log('500 messages',message)
      res.status(201).json({
        isSuccess:true,
        data:message
      });
      // console.log('chatroom message: ', message)
      // res.json([message, req.session['chatroom'], friendImg[0], checkBlockTime[0]['blocked_time'],friendInfo])
    } catch (e) {
      res.status(401).json({
        isSuccess:false,
        msg: "unable get msg" + e
      })
    }
  };

  sendMessage = async (req: Request, res: Response) => {
    try {
      if (!req.body?.roomId || !req.body?.message) {
        res.status(500).json({
          isSuccess: false,
          msg: "not enough information"
        });
      }
      let roomId = req.body.roomId;
      let userId = req.user!.id;
      let result = await this.friendService.insertMessage(
        req.body.message,
        roomId,
        userId
      );

      // console.log("insertMessage",result)
      // console.log('to: ',req.session['chatroom']['friendID'] )
      // let checkBlockTime = await this.friendService.checkBlockTime(
      //   req.session["chatroom"]["selfChatroom"]
      // );
      // let checkBlockTime2 = await this.friendService.checkBlockTime(
      //   req.session["chatroom"]["friendChatroom"]
      // );
      // console.log('checking block time: ', checkBlockTime[0]['blocked_time'])
      // if (
      //   checkBlockTime[0]["blocked_time"] == null &&
      //   checkBlockTime2[0]["blocked_time"] == null
      // ) {
      io.to("room:" + roomId).emit("message-update", { ...result[0], roomId });
      io.to("room:" + roomId).emit("unread-update", { count: 1, sender_id: userId });

      // if (req.session['chatroom']['selfChatroom'] > req.session['chatroom']['friendChatroom']){
      //     let room = (req.session['chatroom']['friendChatroom'] + '-' + req.session['chatroom']['selfChatroom'])
      //     console.log('inform room: ', room)
      //     io.to(room).emit('message-update', result)
      // } else {
      //     let room = (req.session['chatroom']['selfChatroom'] + '-' + req.session['chatroom']['friendChatroom'])
      //     console.log('inform room: ', room)
      //     io.to(room).emit('message-update', result)
      // }
      // io.emit('message-update', result)
      res.status(201).json({
        isSuccess: true,
        data: 'success',
      });
    } catch (error) {
      console.log("Error in sending chats: ", error)
      res.status(500).json({
        isSuccess: false,
        msg: "fail to send msg:" + error,
      });
    }
  };

  
  setMsgToRead = async (req: Request, res: Response) => {
    try {
      if (!req.body?.roomId) {
        res.status(401).json({
          isSuccess:false,
          data: "not enough information"
        });
      }
      let userId = req.user!.id
      let roomId = req.body.roomId;
      await this.friendService.setMsgToRead(userId, roomId);
      
      const totalUnread = await this.friendService.getTotalUnreadMsg(userId)
      res.status(201).json({
        isSuccess:true,
        data: totalUnread[0]
      });
    } catch (e) {
      res.status(401).json({
        isSuccess:false,
        data: "unable to update read status"
      });
    }
  };

  // deleteChatRecord = async (req: Request, res: Response) => {
  //   try {
  //     // console.log(req.session['chatroom'])
  //     if (
  //       req.session["chatroom"]["selfChatroom"] >
  //       req.session["chatroom"]["friendChatroom"]
  //     ) {
  //       await this.friendService.deleteMessagesFromLarger(
  //         req.session["chatroom"]["selfChatroom"],
  //         req.session["chatroom"]["friendChatroom"]
  //       );
  //     } else if (
  //       req.session["chatroom"]["selfChatroom"] <
  //       req.session["chatroom"]["friendChatroom"]
  //     ) {
  //       await this.friendService.deleteMessagesFromSmaller(
  //         req.session["chatroom"]["selfChatroom"],
  //         req.session["chatroom"]["friendChatroom"]
  //       );
  //     }
  //     // let result = await this.friendService.deleteMessages(req.session['chatroom']['selfChatroom'], req.session['chatroom']['friendChatroom'])
  //     // console.log('result of delete record: ', result)
  //     return;
  //   } catch (e) {
  //     throw e;
  //   }
  // };

  // blockFd = async (req: Request, res: Response) => {
  //   try {
  //     // console.log('handling block service: ', req.session['chatroom'])
  //     await this.friendService.blockFd(req.session["chatroom"]["selfChatroom"]);
  //     res
  //       .status(200)
  //       .end(
  //         "blocked relationship id: ",
  //         req.session["chatroom"]["friendChatroom"]
  //       );
  //   } catch (e) {
  //     throw e;
  //   }
  // };

  // unblockFd = async (req: Request, res: Response) => {
  //   try {
  //     // console.log('handling unblock service: ', req.session['chatroom'])
  //     await this.friendService.unblockFd(
  //       req.session["chatroom"]["selfChatroom"]
  //     );
  //     res
  //       .status(200)
  //       .end(
  //         "unblocked relationship id: ",
  //         req.session["chatroom"]["friendChatroom"]
  //       );
  //   } catch (e) {
  //     throw e;
  //   }
  // };
}
