import { Request, Response, NextFunction } from 'express'
import { config } from 'dotenv'
import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import jwt from './jwt';
import { JWTAdminPayload, JWTPayload } from './helpers/types';
import { UserService } from './services/user-service';
import { AdminService } from './services/admin-service';
config()

const permit = new Bearer({
  query: "access_token"
})

let userService: UserService;
let adminService:AdminService;

export function setService(user: UserService,admin:AdminService) {
  userService = user
  adminService = admin
}

export async function isAdmin(req: Request, res: Response, next: NextFunction) {
  try {
    //check header for token
    console.log("admin logged in")
    const token = permit.check(req);
    if (!token) {
      return res.status(401).json({ msg: "no admin Token: Permission Denied" });
    }
    const payload: JWTAdminPayload = jwtSimple.decode(token, jwt.jwtSecret);
    console.log(payload.id)
    let user = await adminService.getAdminDataById(payload.id);
    if (user) {
      req.user = user;
      return next();
    } else {
      return res.status(401).json({ msg: "Permission Denied" });
    }
  } catch (e) {
    console.log(e)
    return res.status(401).json({ msg: "Permission Denied" });
  }

}

export async function isLoggedIn(req: Request, res: Response, next: NextFunction) {
  try {
    //check header for token
    console.log("is logged in")
    const token = permit.check(req);
    if (!token) {

      return res.status(401).json({ msg: "no Token: Permission Denied" });
    }
    const user = await checkUserExist(token)
    if (user) {
      req.user = user;
      return next();
    } else {
      return res.status(401).json({ msg: "Permission Denied" });
    }
  } catch (e) {
    console.log(e)
    return res.status(401).json({ msg: "Permission Denied" });
  }

}

export async function checkUserExist(token: string) {
  try {
    const payload: JWTPayload = jwtSimple.decode(token, jwt.jwtSecret);
    // console.log('userTokenDecoded', payload)
    let user = await userService.getUserById(payload.id);
    console.log("user", user)
    return user
  } catch (e) {
    throw e
  }

}
