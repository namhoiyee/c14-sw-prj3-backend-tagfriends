import { Knex } from "knex";
import fs from 'fs'
import path from 'path'
import AWS from "aws-sdk";
import dotenv from 'dotenv'
dotenv.config();


export async function seed(knex: Knex): Promise<void> {
    var s3 = new AWS.S3()
    let p = path.join(__dirname, '..', 'fakePhotos')
    let fAsian = 'asian_female'
    let fWest = 'western_female'
    let mAsian = 'asian_male'
    let mWest = 'western_male'
    let pathArr = [fAsian, fWest, mAsian, mWest]
    let images = await knex.select('url').from('user_photos').where('description','profile')
    if (images.length) {
        try {
            console.log("before delete")
            for (let i = 0; i < images.length; i++) {

                var params = {
                    Bucket: process.env.S3_BUCKET!,
                    Key: images[i].url
                };
                await s3.headObject(params).promise()
                console.log("File Found in S3")
                try {
                    await s3.deleteObject(params).promise()
                    console.log("deleted")
                } catch (e) {
                    console.log(e)
                }
            }
        } catch (e) {
            console.log("cannot find file",e)
        }
    }
    await knex('user_photos').del();
    const [female_id] = await knex.select('id').from('genders').where('gender', 'female');
    const [male_id] = await knex.select('id').from('genders').where('gender', 'male')

    // let count = [0, 0, 0, 0]
    let insertArr: { user_id: number, url: string, description: string }[] = []
    let users = await knex.select('id', 'gender_id').from('users')

    for (let user of users) {
        let folder: string
        if (user.gender_id == female_id.id) {
            //asian female
            folder = Math.random() < 0.5 ? pathArr[0] : pathArr[1]
        } else if (user.gender_id == male_id.id) {
            folder = Math.random() < 0.5 ? pathArr[2] : pathArr[3]
        } else {
            folder = pathArr[Math.floor(Math.random() * 4)]
        }
        // console.log('path', path.join(p, folder))
        try {
            let files = await (fs.promises.readdir(path.join(p, folder)))
            let randomPhotoIndex = Math.floor(Math.random() * files.length)
            let fileKey = 'photo/' + folder + '-' + Date.now() + files[randomPhotoIndex].substring(files[randomPhotoIndex].lastIndexOf('.'))
            let fileContent = await fs.promises.readFile(path.join(p, folder, files[randomPhotoIndex]))
            var params2 = {
                Bucket: process.env.S3_BUCKET!,
                Key: fileKey,
                Body: fileContent,

            };
            await s3.putObject(params2).promise()
            
            insertArr.push({ user_id: user.id, url: fileKey, description: 'profile' })
        } catch (e) {
            console.log(e)
        }

        

    }
    await knex.insert(insertArr).into('user_photos').returning('id')
    insertArr.map(e => {
        e.description = 'original profile'
    })
    await knex.insert(insertArr).into('user_photos').returning('id')

    // console.log("ran", photos, originalPhotos)
}
