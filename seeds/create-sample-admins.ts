import { Knex } from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("admins").del();

    // Inserts seed entries
    const hashPw = await hashPassword("123");
    await knex("admins").insert([
        { 
            id: 1, 
            admin_name: "Super Admin",
            email: "superadmin@taggie.xyz",
            password: hashPw,
            role: "Super",
            theme: "crimson",
        },
        { 
            id: 2,
            admin_name: "Ordinary Admin 1",
            email: "ordinaryadmin1@taggie.xyz",
            password: hashPw,
            role: "Ordinary",
            theme: "lightBlue",
        },{ 
            id: 3,
            admin_name: "Ordinary Admin 2",
            email: "ordinaryadmin2@taggie.xyz",
            password: hashPw,
            role: "Ordinary",
            theme: "darkMode",
        },{ 
            id: 4,
            admin_name: "Intern Admin",
            email: "internadmin@taggie.xyz",
            password: hashPw,
            role: "Intern",
            theme: "dark",
        },
    ]);
};
