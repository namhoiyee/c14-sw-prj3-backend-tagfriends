import { Knex } from "knex";
import dotenv from 'dotenv'
dotenv.config();


export async function seed(knex: Knex): Promise<void> {
    let usersId = await knex.select('id')
        .from('users')
    let userLocations = usersId.map((id) => ({
        grid: 331153,
        user_id: id.id,
        location: knex.raw(`point(${22.2860375}, ${114.1486955})`),
        created_at: new Date((new Date() as any) - (Math.floor(Math.random()*10))*86400000),
    }));

    await knex
        .insert(userLocations)
        .into("user_locations")
        .returning(["grid", "location", "created_at"]);
}

