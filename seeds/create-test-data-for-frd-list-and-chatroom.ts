import { Knex } from "knex";
import { v4 as uuidv4 } from 'uuid'

export async function seed(knex: Knex): Promise<void> {
    await knex("relationship").del();
    await knex("chatroom_records").del();

    const [{ id: kevin2_id }] = await knex
        .select('id')
        .from('users')
        .where('email', '0@0')

    const [{ id: kevin3_id }] = await knex
        .select('id')
        .from('users')
        .where('email', '2@2')
    
    const [{ id: kevin4_id }] = await knex
        .select('id')
        .from('users')
        .where('email', '1@1')
    
    let room_23 = uuidv4()
    let room_24 = uuidv4()
    await knex("relationship").insert([
        {
            friend_id: kevin3_id,
            user_id: kevin2_id,
            status:"like",
            match_coordinates: knex.raw(`point(114.14816, 22.2876)`),
            room_id:room_23,
            created_at: new Date((new Date() as any) - (Math.floor(Math.random()*10))*86400000)
        },
        {
            friend_id: kevin2_id,
            user_id: kevin3_id,
            status:"like",
            match_coordinates: knex.raw(`point(114.14816, 22.2876)`),
            room_id:room_23,
            created_at: new Date((new Date() as any) - (Math.floor(Math.random()*10))*86400000)
        },
        {
            friend_id: kevin4_id,
            user_id: kevin2_id,
            status:"like",
            match_coordinates: knex.raw(`point(114.14816, 22.2876)`),
            room_id:room_24,
            created_at: new Date((new Date() as any) - (Math.floor(Math.random()*10))*86400000)
        },
        {
            friend_id: kevin2_id,
            user_id: kevin4_id,
            status:"like",
            match_coordinates: knex.raw(`point(114.14816, 22.2876)`),
            room_id:room_24,
            created_at: new Date((new Date() as any) - (Math.floor(Math.random()*10))*86400000)
        },
        {
            friend_id: 4,
            user_id: kevin2_id,
            status:"like",
            match_coordinates: knex.raw(`point(114.14816, 22.2876)`),
            room_id:uuidv4(),
            created_at: new Date((new Date() as any) - (Math.floor(Math.random()*10))*86400000)
        }
    ]).returning('id');
    // once relationship is built up, a blank message will be recorded to both parties' chatroom record
    // await knex("chatroom_records").insert([
    //     {
    //         relationship_id: relationship_id[0],
    //         message: ""
    //     },
    //     {
    //         relationship_id: relationship_id[1],
    //         message: ""
    //     },
    //     {
    //         relationship_id: relationship_id[2],
    //         message: ""
    //     },
    //     {
    //         relationship_id: relationship_id[3],
    //         message: ""
    //     }
    // ]);
};
