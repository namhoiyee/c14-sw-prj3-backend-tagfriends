import { Knex } from "knex";

let users: {
    user_id: number,
    transaction: number,
    description: string,
    created_by: string,
    created_at: Date
}[] = []
for (let i = 0; i < 50; i++){
    users.push({
        user_id: Math.ceil(Math.random()*20), 
        transaction: -1,
        description: "rematch",
        created_by: "system",
        created_at: new Date((new Date() as any) - (Math.floor(Math.random()*10))*86400000)
    })
}

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("coin_records").del();

    // Inserts seed entries
    await knex("coin_records").insert(users);
};
