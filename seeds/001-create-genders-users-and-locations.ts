import { Knex } from "knex";
import { hashPassword } from "../hash";
import Chance from "chance";
// import { STATUS_CODES } from "http";
// import { st } from "../db";
// import { getUserGrid } from "../geohash";

let chance = new Chance(Math.random);
type User = {
  username: string;
  email: string;
  birthday: Date;
  gender_id: number;
  password: string;
  registration_status: number;
  isFake: boolean;
  created_at: Date;
};

export async function seed(knex: Knex): Promise<void> {
  await knex("users").del();
  await knex("genders").del();

  const female_id = (
    await knex
      .insert({
        gender: "female",
      })
      .into("genders")
      .returning("id")
  )[0] as number;

  const male_id = (
    await knex
      .insert({
        gender: "male",
      })
      .into("genders")
      .returning("id")
  )[0] as number;

  const other_id = (
    await knex
      .insert({
        gender: "其他",
      })
      .into("genders")
      .returning("id")
  )[0] as number;

  const hashPw = await hashPassword("1");
  let users: User[] = [];
  let registration_status = 3;
  for (let i = 0; i < 20; i++) {
    let bdayYear =
      Math.random() < 0.2
        ? chance.year({ min: 1960, max: 2000 })
        : chance.year({ min: 1980, max: 2000 });
    let name = chance.name();
    let date = chance.birthday({ year: bdayYear }) as Date;
    let beginDate = new Date((new Date() as any) - (Math.floor(Math.random()*10))*86400000)
    let email = `${i}@${i}`;
    let gender_id =
      Math.random() < 0.1
        ? other_id
        : Math.random() < 0.5
        ? female_id
        : male_id;
    let user: User = {
      username: name.length > 50 ? name.slice(0, 49) : name,
      email,
      birthday: date,
      gender_id,
      password: hashPw,
      registration_status,
      isFake: true,
      created_at: beginDate,
    };
    users.push(user);
  }

//   let usersId: number[] = 
  await knex
    .insert(users)
    .into("users")
    .returning("id");

//   let userLocations = usersId.map((id) => ({
//     grid: 331153,
//     user_id: id,
//     location: knex.raw(`point(${22.2860375}, ${114.1486955})`),
//   }));

//   await knex
//     .insert(userLocations)
//     .into("user_locations")
//     .returning(["grid", "location", "created_at"]);
}
