import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("reports").del();

    // Inserts seed entries
    await knex("reports").insert([
        { 
            id: 1, 
            user_id: 1,
            target_id: 2,
            category: "gdsda Harassment",
            content: "1 sent some inappropriate text and photo to me.",
            read: true,
            replied: true,
            replied_by: "Super Admin ID: 1"
        },
        { 
            id: 2, 
            user_id: 2,
            target_id: 3,
            category: "asdas Harassment",
            content: "2 sent some inappropriate text and photo to me.",
            read: true,
            replied: true,
            replied_by: "Super Admin ID: 1"
        },
        { 
            id: 3, 
            user_id: 2,
            target_id: 1,
            category: "asdas Harassment",
            content: "3 sent some inappropriate text and photo to me.",
            read: false,
            replied: true,
        },
        { 
            id: 4, 
            user_id: 3,
            target_id: 1,
            category: "ewr Harassment",
            content: "photo to me.",
            read: false,
            replied: false
        }
    ]);
};
