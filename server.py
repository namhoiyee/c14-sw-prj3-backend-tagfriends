# %%

from sanic import Sanic
from sanic.response import json
import tensorflow as tf
import numpy as np
# import pathlib
import os
# import sys
# import math
import cv2
#from tensorflow.keras import layers
# from tensorflow.keras.models import load_model
# import tflite_runtime.interpreter as tflite
import urllib.request
from train_model_server import loadImage,loadModel 
from dotenv import load_dotenv
load_dotenv()

access_secret = os.environ.get("AWS_SECRET_ACCESS_KEY")
access_key = os.environ.get("AWS_ACCESS_KEY_ID")
bucket_name = os.environ.get("S3_BUCKET")

app = Sanic("Python Hosted Prediction")
models = {}


def loadImageFromURL(URL):
    req = urllib.request.urlopen(URL)
    arr = np.asarray(bytearray(req.read()), dtype=np.uint8)
    img = cv2.imdecode(arr, -1)
    return img




@app.post("/matching")
async def matching_predict(request):
    # try:
        dataJson = request.json
        data = dataJson['result']
        print(len(dataJson['result']))
        dataInput = np.empty((len(data),131),dtype=np.float32)
        for i in range(len(data)):
            for j in range(131):
                dataInput[i][j] = -1
        imgdataInput = np.empty((len(data),160,160,3),dtype=np.float32)
        data_dir = os.getcwd()
        model_path = os.path.join(data_dir,'models',str(dataJson['email'])+'-matching.tflite')
        urlS3 = os.environ.get("CDN_URL")
        result = []
        if not(os.path.exists(model_path)):
            model = str(dataJson['email'])+'-matching.tflite'
            loadModel(model)
        if (os.path.exists(model_path)):
            interpreter = tf.lite.Interpreter(model_path=model_path)
            interpreter.allocate_tensors()
            input_details = interpreter.get_input_details()
            output_details = interpreter.get_output_details()
            for i in range(len(data)):
                target_id = data[i]['buddyId']
                for tag in data[i]['buddyTagsId']:
                    dataInput[i][tag-1] = 1.0
                url = data[i]['url']
                img = loadImage(urlS3+url)
                img =  (img/127.5) - 1
                interpreter.set_tensor(input_details[1]['index'], [img])
                interpreter.set_tensor(input_details[0]['index'], [dataInput[i]])
                interpreter.invoke()
                output_data = interpreter.get_tensor(output_details[0]['index'])
                
                data1 = output_data[0][0].item()
                data1 = "{:.2f}".format(data1)
                if output_data > 0.5:    
                    result.append({'id':target_id,'accuracy':data1})
                if len(result) >=5:
                    return json({"match":result})
             
        return json({"match":result})
        
    # except Exception as e:
        print("from matching")
        print(e)
        return json({"match":[]})


@app.post("/image")
async def image_predict(request):
    try:
        url = request.json
        print(request.json)
        casaPath = ["haarcascade_frontalface_default.xml","haarcascade_profileface.xml"]
        urlS3 = os.environ.get("CDN_URL")
        # Create the haar cascade
        faceCascades = []
        for path in casaPath:
            faceCascades.append(cv2.CascadeClassifier(path))
        
        # Read the image
        image = loadImageFromURL(urlS3+url)
        # cv2.imshow("profile",image)
        # cv2.waitKey()
        # cv2.destroyAllWindows() 
        margin = (int(image.shape[1]),int(image.shape[0]))
        w = image.shape[1] if image.shape[1] < 1000 else 1000
        h = int(image.shape[0]/image.shape[1]*w)
        ratio = w/image.shape[1]
        image = cv2.resize(image,(w,h))
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        flipped = cv2.flip(gray, 1)
        
        # Detect faces in the image
        frontFace = faceCascades[0].detectMultiScale(
            gray,
            scaleFactor=1.2,
            minNeighbors=4,
            minSize=(50, 50)
        )

        profileFace1 = faceCascades[1].detectMultiScale(
            gray,
            scaleFactor=1.2,
            minNeighbors=5,
            minSize=(50, 50)
        )

        profileFace2 = faceCascades[1].detectMultiScale(
            flipped,
            scaleFactor=1.2,
            minNeighbors=5,
            minSize=(50, 50)
        )
        # eyes = faceCascades[2]..detectMultiScale(
        #     gray,
        #     scaleFactor=1.3,
        #     minNeighbors=5,
        #     minSize=(30, 30)
        # )

        
        print("Found {0} faces!".format(len(frontFace)))
        print("Found {0} profile!".format(len(profileFace1)+len(profileFace2)))
        # print("Found {0} eyes!".format(len(eyes)))



        # Draw a rectangle around the faces
        # maxW = 0
        # maxH = 0
        # maxX = 0
        # maxY = 0
        # for (x, y, w, h) in frontFace:
        #     if w < 0.1*image.shape[1] or h < 0.1*image.shape[0]:
        #         print('get a picture with bigger face', w)
        #     cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 4)
        #     if w*h > maxW*maxH:
        #         maxW = w
        #         maxH = h
        #         maxX = x
        #         maxY = y

        # for (x, y, w, h) in profileFace1:
        #     if w < 0.1*image.shape[1] or h < 0.1*image.shape[0]:
        #         print('get a picture with bigger face', w)
        #     cv2.rectangle(image, (x, y), (x+w, y+h), (255, 0, 0), 4)
        #     if w*h > maxW*maxH:
        #         maxW = w
        #         maxH = h
        #         maxX = x
        #         maxY = y
        # for (x, y, w, h) in profileFace2:
        #     if w < 0.1*image.shape[1] or h < 0.1*image.shape[0]:
        #         print('get a picture with bigger face', w)
        #     cv2.rectangle(image, (x, y), (x+w, y+h), (0, 0, 255), 4)
        #     if w*h > maxW*maxH:
        #         maxW = w
        #         maxH = h
        #         maxX = x
        #         maxY = y
        # for (x, y, w, h) in eyes:
        #     cv2.rectangle(image, (x, y), (x+w, y+h), (255, 255, 0), 2)
        # if image.shape[0] < 100 or image.shape[1]<100:
        #     print('get a bigger picture', image.shape)
        
        # cv2.imshow("Faces found", image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows() 
        # print(maxX,maxY,maxX+maxW,maxY+maxH)
        # coordinates = (int(maxX/ratio),int(maxY/ratio),int((maxX + maxW)/ratio),int((maxY + maxH)/ratio))
        # print("coordinates:", coordinates)
        # print('')
        # if maxW > 100 and maxH >100:
        if len(frontFace) or len(profileFace1) or len(profileFace2):
            return json({"face":len(frontFace)+len(profileFace1)+len(profileFace2)})
        else:
            return json({"face":0})
    except Exception as e:
        print(e)
        return json({"face":-1, "msg": e})

 

if __name__ == "__main__":
    # port = int(os.environ['PORT'])
    port = 8001
    app.run(host="localhost", port=port)
# %%
