import express from 'express'
import expressExpress from 'express-session'
import { format } from 'fecha'
import dotenv from 'dotenv'
// import path from 'path'
import http from 'http'
import socketIO from 'socket.io'
import { env } from './env'
import { setSocketIO } from './socketio'
import { knex } from './db'
import { isLoggedIn, setService, isAdmin } from './guards'
import { createRouter } from './routes'
import multer from 'multer'
import { MatchingController } from './controllers/matching-controller'
import { MatchingService } from './services/matching-service'
import { UserService } from './services/user-service'
import { UserController } from './controllers/user-controller'
import { FriendService } from './services/friend-service'
import { FriendController } from './controllers/friend-controller'
import { ProfileService } from './services/profile-service'
import { ProfileController } from './controllers/profile-controller'
import { grantExpress } from './oauth';
import aws from 'aws-sdk'
import multerS3 from 'multer-s3'
import cors from 'cors'
import { AdminService } from './services/admin-service'
import { AdminController } from './controllers/admin-controller'
import { ReportService } from './services/report-service'
import { ReportController } from './controllers/report-controller'

dotenv.config()

let app = express()
let server = new http.Server(app)
let io = new socketIO.Server(server)
setSocketIO(io)

app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())
export let sessionMiddleware = expressExpress({
  secret: 'Memo Wall will not down',
  saveUninitialized: true,
  resave: true,
})
app.use(sessionMiddleware)

app.use(grantExpress as express.RequestHandler)

app.use((req, res, next) => {
  let date = new Date()
  let text = format(date, 'YYYY-MM-DD hh:mm:ss')
  console.log(`[${text}]`, req.method, req.url)
  next()
})

const s3 = new aws.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: 'ap-southeast-1'
});


export const uploadS3 = multer({
  storage: multerS3({
    s3: s3,
    bucket: process.env.S3_BUCKET!,
    metadata: (req, file, cb) => {
      cb(null, { fieldName: file.fieldname });
    },
    key: (req, file, cb) => {
      cb(null, `${file.fieldname}/${Date.now()}-${file.originalname.replace(/ /g, "-")}.${file.mimetype.split('/')[1]}`);
    }
  }),
  limits: { fileSize: 1048576 },
  fileFilter: function (req, file, callback) {
    const ext = file.mimetype;
    console.log('ext', ext)
    if (file.fieldname == "voice") {
      if (ext !== 'audio/mpeg' && ext !== 'audio/ogg') {
        return callback(new Error('Only mp3s are allowed'))
      }
    } else {
      if (ext !== 'image/jpeg' && ext !== 'image/png') {
        return callback(new Error('Only images are allowed'))
      }
    }
    callback(null, true)
  }
})

// app.get('/', (req, res) => {
//   if (req.session?.['user']) {
//     res.redirect('/main/main.html')
//   } else {
//     res.redirect('/login.html')
//   }
// })
// app.use(express.static('public'))
// app.use(express.static('uploads'))
// app.use(express.static('login'))
// app.use('/main', isLoggedIn, express.static('protected'))

// let storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     cb(null, './public/main/uploads')
//   },
//   filename: (req, file, cb) => {
//     let filename = file.fieldname + '-' + Date.now() + '-' + file.originalname
//     cb(null, filename)
//   },
// })
// let upload = multer({ storage })

let matchingService = new MatchingService(knex)
let userService = new UserService(knex)
export let friendService = new FriendService(knex)
let profileService = new ProfileService(knex)
let adminService = new AdminService(knex)
let reportService = new ReportService(knex)
setService(userService, adminService)

let matchingController = new MatchingController(matchingService, profileService)
let userController = new UserController(userService)
let friendController = new FriendController(friendService)
let profileController = new ProfileController(profileService)
let adminController = new AdminController(adminService)
let reportController = new ReportController(reportService)

let router = createRouter({
  isLoggedIn,
  isAdmin,
  matchingController,
  userController,
  friendController,
  profileController,
  adminController,
  reportController,
  uploadS3
})
app.use(router)

app.use((req, res) => {
  res.json({ msg: '404' })
})

async function main() {
  // if (!fs.existsSync('./public/main/uploads')) {
  //   fs.mkdirSync('./public/main/uploads')
  // }
  server.listen(env.PORT, () => {
    console.log(`listening on http://localhost:${env.PORT}`)
  })
}
main()
