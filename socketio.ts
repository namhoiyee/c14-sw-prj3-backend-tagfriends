import express from "express";
import socketIO from "socket.io";
import {friendService, sessionMiddleware } from "./server";
import { checkUserExist } from "./guards";

export let io: socketIO.Server;
export let socketMap: {
  [userId: number]: socketIO.Socket[];
} = {};

export function setSocketIO(value: socketIO.Server) {
  io = value;
  io.on("connection", async (socket) => {
    let userId;
    try {
      const token = socket.handshake.auth.token;
      const user = await checkUserExist(token);
      
      if (!user) {
        socket.disconnect();
        return;
      }

      userId = user.id;
      if (socketMap[userId] != null) {
        socketMap[userId].push(socket);
      } else {
        socketMap[userId] = [socket];
      }

      const relationships = await friendService.checkRelationship(userId)
      console.log("relationship", relationships);

      for (let room of relationships) {
        socket.join("room:" + room.room_id);
        console.log("inside room: ", room.room_id, "user:", userId);
        io.to("room:" + room.room_id).emit("inside", room.room_id);
      }
    } catch (error) {
      if (userId) {
        socketMap[userId].pop();
      }
      socket.disconnect();
      throw new Error("io:" + error);
    }
  });

  // io.use(async (socket, next) => {
  //   try {
  //     const token = socket.handshake.auth.token;
  //     const isUserExist = await checkUserExist(token);
  //     console.log("isUserExist", isUserExist.id);
  //     const relationships = await friendService.checkRelationshipId(
  //       isUserExist.id
  //     );
  //     console.log("relationships", relationships);

  //     if (!isUserExist) {
  //       socket.disconnect();
  //       return;
  //     }
  //     socket.emit(
  //       "room-to-join",
  //       relationships.map((relation) => relation.room_id)
  //     );
  //     socket.on("hey", (e) => console.log(e));
  //     socket.on("heys", () => {
  //       console.log("being clicked");
  //     });
  //     socket.on("join", (room) => {
  //       console.log(room);
  //       socket.join(room);
  //     });

  //     // console.log('token',payload)
  //     socket.emit("sh", isUserExist);
  //   } catch (e) {
  //     console.log("error", e);
  //     socket.disconnect();
  //   }

  //   next();
  // });

  io.use((socket, next) => {
    let req = socket.request as express.Request;
    let res = req.res as express.Response;
    sessionMiddleware(req, res, next as express.NextFunction);
  });
}
